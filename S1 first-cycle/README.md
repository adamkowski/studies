# Subjects

* [PwJC#](#programming-in-c)
* [PwJS](#scripting-languages)
* [MK](#methods-of-compilation)
* [PwXML](#programming-in-xml)
* [PwJJava](#programming-in-java)
* [PKW](#multicore-computers-programming)
* [PK](#component-oriented-programming)

## Programming in C#

The aim of this course was to learn the basis of C# language. We were using the .NET Framework 4.7.2.

* **#1:** Console application for generating HTML tables using the methods specified in the task,
* **#2:** Simple HTTP server using a TcpListener class with basic GUI for administrate. It served tables from the
  previous task according to client's requests,
* **#3:** Application to making source file copies and .zip archives based on analyzing structure of project
  directories,
* **#4:** Task to use polymorphism: a Windows Forms application to drawing different types of shapes with real-time
  preview,
* **#5:** The biggest app - extension of the previous exercise: GUI using more advanced techniques, creating multiple
  separate tabs for different drawings, operations save and load from files,
* **#6:** Graphical application to help entering names in combobox - comparison and use of appropriate data structures
  to analyze the most popular names,
* **#7:** Console tool for converting photo dimensions and creating structure of copied files,
* **#8:** Additional task (no grade): introduction to unit tests - implementation of a binary tree in the Test Driven
  Development approach.

## Scripting Languages

The classes was focused on explanation, use and presentation of possibilities two languages: Perl 5 and Python 3.

* **Perl #1:** Creating our own version of the ls command in accordance to the provided functionality requirements,
* **Perl #2:** First regex task: generating statistics from parsed .ics file and creating calendar in .csv file format
  from the extracted data,
* **Perl #3:** Second regex task: extracting data from huge HTML files and generating a summary to a .csv file,
* **Python #1:** Two tasks: make a histogram of the frequency of characters and breaking the substitution cipher (
  practical use of the histogram),
* **Python #2:** Comparing files byte by byte and searching for duplicates in specified directories,
* **Python #3:** Implementation of lab2 from Perl in Python language.

## Methods of Compilation

On this subject I created a project of my own programming language and built a compiler for it generating assembler code
for MIPS processors (tested on
[MARS](http://courses.missouristate.edu/KenVollmar/MARS/) emulator).

Technologies and tools used:

* Flex - Lexical analyzer,
* Bison - Syntax analyzer,
* C++11 - Processing and generating the result code,
* Perl - automating tests,
* Make - build automation.

<table>
<thead>
    <tr>
        <th>Action</th>
        <th>Syntax</th>
    <tr>
</thead>
<tbody>
    <tr>
        <td>Declaration</td>
        <td>int x; <br> flo[7, x] y;</td>
    </tr>
    <tr>
        <td>Assignment</td>
        <td>arr[row, col] { 8 }; <br> pi { 3.14 };</td>
    </tr>
    <tr>
        <td>Expressions</td>
        <td>x { 2 * (x + 3) }; <br> res { pi * 4 * 4};</td>
    </tr>
    <tr>
        <td>Printing data</td>
        <td>output(4); <br> output(res); <br> output("Hello\n");</td>
    </tr>
    <tr>
        <td>Insert input</td>
        <td>var { input() };</td>
    </tr>
    <tr>
        <td>Conditional expression</td>
        <td>
            if (x < 5) { do_something(); }
            <br>
            if (age < 18) { ... } else { ... }
        </td>
    </tr>
    <tr>
        <td>Loops</td>
        <td>while (counter >= 0) { do_calculations(); }</td>
    </tr>
    <tr>
        <td>User's functions</td>
        <td>fun name() { ... }</td>
    </tr>
</tbody>
</table>

## Programming in XML

Because XML is not a programming language, the subject was rather focused on file processing and technologies related to
XML.

* **Lab #1:** Build XML document containing a list of nested person nodes,
* **Lab #2:** Validation file from lab1 using the XSD schema,
* **Lab #3:** XML file conversion and data visualization in the form of an HTML table,
* **Lab #4:** Using XSL-FO and the Apache FOP tool to convert the same XML file to pdf,
* **Lab #5:** Generating HTML table using the XML DOM parser in JavaScript,
* **Lab #6:** Using Eclipse IDE to generate WSDL with service creation.

## Programming in Java

One project - simple console application "Employee Records" in MVC pattern with object serialization, saving and loading
from compressed archives.

## Multicore Computers Programming

Basics of using the OpenMP library in C++. Writing multithreaded programs and parallelization of existing sequential
algorithms. Optimizing the use of CPU cache memory, multi-nested parallel regions, tasks, locks.

* **Lab #1:** Acceleration of matrix multiplication with optimization of cache line usage and parallel computation,
* **Lab #2:** Generating Mandelbrot fractal with parallel computations. Scheduling loop iterations and optimisation
  workload of threads,
* **Lab #3:** Ulam spiral: using nested parallel regions to optimise threads workload in calculating prime numbers,
* **Lab #4:** Generating Sierpinski triangle. Concurrent recursion using tasks in OpenMP,
* **Lab #5:** Using OpenMP tasks and locks to explore all the maze paths in a recursive way.

## Component-Oriented Programming

Basics of working with the Spring 4 framework. An initial code was provided for each task and it had to be completed
according to the instructions.

* **Lab #2:** Different types of configurations, creating beans, dependency injection, resolving conflicts,
* **Lab #3:** Aspect-oriented programming in Spring. Configuration and different types of aspects,
* **Lab #4:** Spring MVC: handling http requests, use of JSP and Thymeleaf technologies,
* **Lab #5:** Creating a controller, saving information in the database using JDBC, form validation,
* **Lab #6:** Implementing own repositories with Hibernate. Using JpaRepository with auto-generated implementations.
