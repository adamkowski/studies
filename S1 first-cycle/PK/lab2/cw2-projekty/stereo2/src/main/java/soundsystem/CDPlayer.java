package soundsystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

public class CDPlayer implements MediaPlayer {
  private CompactDisc cd;

  public CDPlayer() {
  }

  public void play() {
    cd.play();
  }

  public void setCd(CompactDisc cd) {
    this.cd = cd;
  }

  public CompactDisc getCd() {
    return cd;
  }
}
