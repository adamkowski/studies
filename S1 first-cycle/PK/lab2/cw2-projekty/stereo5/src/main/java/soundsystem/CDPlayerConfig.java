package soundsystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import java.util.List;

@Configuration
@PropertySource("classpath:/soundsystem/soundsystem.properties")
public class CDPlayerConfig {
	@Autowired
	Environment env;

	@Bean
	public CompactDisc cd() {
		return new SgtPeppers();
	}

	@Bean
	@Profile("dev")
	public MediaPlayer player() {
		return new CDPlayer();
	}

	@Bean
	@Profile("test")
	public MediaPlayer sevenNationArmyPlayer() {
		return new MediaPlayer() {
			@Autowired
			@Qualifier("SevenNationArmy")
			private CompactDisc cd;

			@Override
			public void play() {
				cd.play();
			}
		};
	}

	@Bean
	@Profile("prod")
	public MediaPlayer goingToHellPlayer() {
		return new MediaPlayer() {
			@Autowired
			@Rock
			private CompactDisc cd;

			@Override
			public void play() {
				cd.play();
			}
		};
	}

	@Bean
	@Profile("dev")
	@Primary
	public CompactDisc brothersInArms() {
		return new BlankDisc("Brothers in Arms", "Dire Straits", List.of());
	}

	@Bean
	@Profile("test")
	@Qualifier("SevenNationArmy")
	public CompactDisc elephant() {
		return new BlankDisc("Elephant", "The White Stripes", List.of());
	}

	@Bean
	@Profile("prod")
	@Rock
	public CompactDisc goingToHell() {
		return new BlankDisc(env.getProperty("cd.album"), env.getProperty("cd.artist"), List.of());
	}
}
