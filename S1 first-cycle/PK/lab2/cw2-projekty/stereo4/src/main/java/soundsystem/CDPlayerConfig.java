package soundsystem;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({"classpath:META-INF/spring/cdconfig.xml", "classpath:META-INF/spring/playerconfig.xml"})
public class CDPlayerConfig {
}
