package soundsystem;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=CDPlayerConfig.class)
public class CDPlayerTest {

  @Rule
  public final StandardOutputStreamLog log = new StandardOutputStreamLog();

  @Autowired
  private MediaPlayer player;

  @Autowired
  private CompactDisc cd;

  @Test
  public void play() {
    BlankDisc blank = (BlankDisc) cd;

    StringBuilder message = new StringBuilder();
    message.append("Playing ").append(blank.getTitle());
    message.append(" by ").append(blank.getArtist()).append(System.lineSeparator());
    blank.getTracks().forEach(track -> message.append("-Track: ").append(track).append(System.lineSeparator()));

    player.play();
    assertEquals(message.toString(), log.getLog());
  }

}
