package soundsystem;

import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class SgtPeppers implements CompactDisc {

  public final String title = "Sgt. Pepper's Lonely Hearts Club Band";
  public final String artist = "The Beatles";
  public final List<String> tracks;

  public SgtPeppers() {
    ExpressionParser parser = new SpelExpressionParser();
    this.tracks = Stream.of(parser.parseExpression("{'Sgt. Pepper''s Lonely Hearts Club Band'}").getValue(String.class),
            parser.parseExpression("{'With a Little Help from My Friends'}").getValue(String.class),
            parser.parseExpression("{'Lucy in the Sky with Diamonds'}").getValue(String.class),
            parser.parseExpression("{'Getting Better'}").getValue(String.class))
            .collect(Collectors.toList());
  }

  public void play() {
    System.out.println("Playing " + title + " by " + artist);
  }

}
