package soundsystem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.List;

@Configuration
public class CDPlayerConfig {
	@Bean
	public CompactDisc sgtPeppers() {
		return new SgtPeppers();
	}

	@Bean
	@Primary
	public CompactDisc cd() {
		ExpressionParser expressionParser = new SpelExpressionParser();
		Expression title = expressionParser.parseExpression("title");
		Expression artist = expressionParser.parseExpression("artist");
		Expression tracks = expressionParser.parseExpression("tracks");

		EvaluationContext context = new StandardEvaluationContext(sgtPeppers());
		return new BlankDisc((String) title.getValue(context),
				(String) artist.getValue(context),
				(List<String>) tracks.getValue(context));
	}

	@Bean
	public MediaPlayer player() {
		return new CDPlayer();
	}
}
