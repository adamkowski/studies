package sia.knights;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class Minstrel {

    public Minstrel() {
    }

    @Pointcut("execution(* sia.knights.Quest.embark(Horse)) && args(horse)")
    private void minstrel(Horse horse) {
    }

    @Before(value = "minstrel(horse)", argNames = "horse")
    public void singBeforeQuest(Horse horse) {
        System.out.println("Tra la la; Jakiż rycerz jest dzielny!" + " No i " + horse.getClass().getSimpleName() + " też.");
    }

    public void singAfterQuest() {
        System.out.println("Hip hip hura; Dzielny rycerz wypełnił misję!");
    }

}
