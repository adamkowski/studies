package sia.knights;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = KnightConfig.class, loader = AnnotationConfigContextLoader.class)
public class BraveKnightTest {

    @Autowired
    private Knight knight;

    @Autowired
    private Horse horse;

    @Rule
    public final StandardOutputStreamLog log = new StandardOutputStreamLog();

    @Test
    public void knightShouldEmbarkOnQuest() {
        String minstrelMessage = "Tra la la; Jakiż rycerz jest dzielny!" + " No i " + horse.getClass().getSimpleName() + " też.";
        String knightMessage = "Wsiadam na konia i podejmuję misję pokonania smoka!";
        knight.embarkOnQuest();
        assertEquals(log.getLog().trim(), minstrelMessage + System.lineSeparator() + knightMessage);
    }

}
