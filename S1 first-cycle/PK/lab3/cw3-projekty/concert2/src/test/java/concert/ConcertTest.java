package concert;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConcertConfig.class)
public class ConcertTest {
    private static final String MESSAGE = "Wyłączenie telefonów" + System.lineSeparator() +
            "Zajęcie miejsc" + System.lineSeparator();

    @Autowired
    @Qualifier("woodstock")
    Performance perf = new Woodstock();

    @Autowired
    Performance perf2 = new Woodstock2();


    @Rule
    public final StandardOutputStreamLog log = new StandardOutputStreamLog();

    @Test
    public void performanceIsNotNullTest() {
        assertNotNull("koncert powinen być przygotowany", perf);
    }

    @Test
    public void performanceTest() {
        perf.perform();
        assertEquals(MESSAGE + "Koncert Woodstock..." + System.lineSeparator() + "Brawa i oklaski!!!" + System.lineSeparator(),
                log.getLog());
    }

    @Test
    public void perf2Test() {
        try {
            perf2.perform();
            assertTrue("Powinien polecieć wyjątek", false);
        } catch (Throwable t) {
            assertTrue(t instanceof RuntimeException);
            assertEquals(MESSAGE + "Koncert Woodstock 2 ..." + System.lineSeparator() + "Domaganie się zwrotu za bilety" + System.lineSeparator(),
                    log.getLog());
        }

    }

}
