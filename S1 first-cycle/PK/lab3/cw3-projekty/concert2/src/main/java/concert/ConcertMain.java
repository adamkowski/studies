package concert;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ConcertMain {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ConcertConfig.class);

        Performance p = context.getBean(Performance.class);
        try {
            p.perform();
        } catch (RuntimeException re) {
            System.out.println("Wyjątek: " + re.getMessage());
        }

        context.close();
    }
}
