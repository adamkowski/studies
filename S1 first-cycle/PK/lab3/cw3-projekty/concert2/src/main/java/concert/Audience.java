package concert;

import org.aspectj.lang.annotation.*;


@Aspect
public class Audience {
    @Pointcut("execution(* concert.Performance.perform(..))")
    public void audience() {
    }

    @Before("audience()")
    public void silenceCellPhones() {
        System.out.println("Wyłączenie telefonów");
    }

    @Before("audience()")
    public void takeSeats() {
        System.out.println("Zajęcie miejsc");
    }

    @AfterReturning("audience()")
    public void applause() {
        System.out.println("Brawa i oklaski!!!");
    }

    @AfterThrowing("audience()")
    public void demandRefund() {
        System.out.println("Domaganie się zwrotu za bilety");
    }
}
