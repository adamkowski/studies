package concert;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConcertConfig.class)
public class ConcertTest {
    private static final String MESSAGE = "Wyłączenie telefonów" + System.lineSeparator() +
            "Zajęcie miejsc" + System.lineSeparator() +
            "Koncert Woodstock..." + System.lineSeparator();

    @Autowired
    private Performance performanceSuccess;

    @Autowired
    @Qualifier("failure")
    private Performance performanceFailure;

    @Rule
    public final StandardOutputStreamLog log = new StandardOutputStreamLog();

    @Test
    public void performanceIsNotNullTest() {
        assertNotNull("koncert powinen być przygotowany", performanceSuccess);
    }

    @Test
    public void performanceWasASuccess() {
        performanceSuccess.perform();
        assertEquals(MESSAGE + "Brawa i oklaski!!!" + System.lineSeparator(), log.getLog());
    }

    @Test(expected = ArithmeticException.class)
    public void performanceWasAFailure() {
        performanceFailure.perform();
        assertEquals(MESSAGE + "Domaganie się zwrotu za bilety" + System.lineSeparator(), log.getLog());
    }
}
