package concert;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;

@Configuration
@EnableAspectJAutoProxy
public class ConcertConfig {
    @Bean
    @Primary
    public Performance woodstock() {
        return new Woodstock();
    }

    @Bean
    @Qualifier("failure")
    public Performance woodstock2() {
        return new Woodstock2();
    }

    @Bean
    public Audience audience() {
        return new Audience();
    }
}
