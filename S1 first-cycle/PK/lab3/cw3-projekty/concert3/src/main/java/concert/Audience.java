package concert;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class Audience {
    @Pointcut("execution(* concert.Performance.perform(..))")
    public void audience() {
    }

    @Around("audience()")
    public void watchPerformance(ProceedingJoinPoint proceedingJoinPoint) {
        silenceCellPhones();
        takeSeats();
        try {
            proceedingJoinPoint.proceed();
            applause();
        } catch (Throwable throwable) {
            demandRefund();
        }
    }

    private void silenceCellPhones() {
        System.out.println("Wyłączenie telefonów");
    }

    private void takeSeats() {
        System.out.println("Zajęcie miejsc");
    }

    private void applause() {
        System.out.println("Brawa i oklaski!!!");
    }

    private void demandRefund() {
        System.out.println("Domaganie się zwrotu za bilety");
    }
}