from src.file.file import File


class Deduplicator:
    def __init__(self, files):
        self.__duplicates = {}
        self.__search_duplicates(files)

    @property
    def duplicates(self):
        return self.__duplicates

    def __search_duplicates(self, files):
        while files:
            compare_what = File(files.pop(0))

            index = 0
            while files and index < len(files):
                compare_with = File(files[index])

                if compare_what.length == compare_with.length:
                    different = self.__compare(compare_what, compare_with)
                else:
                    different = True

                if not different:
                    self.__add_duplicated(compare_what, compare_with)
                    files.pop(index)
                else:
                    index += 1

    @staticmethod
    def __compare(comp_what, comp_with):
        index = 0
        for byte in comp_with.content:
            if index < comp_what.length:
                if byte != comp_what.content[index]:
                    return True
                index += 1

        return False

    def __add_duplicated(self, comp_what, comp_with):
        if comp_what.name not in self.__duplicates:
            self.__duplicates[comp_what.name] = []
        self.__duplicates[comp_what.name].append(comp_with.name)

    @staticmethod
    def print(what):
        duplicates = what.duplicates
        counter = 0
        for family in duplicates:
            counter += 1
            print("{}.".format(counter), family)
            for member in duplicates[family]:
                print("\t>", member)
