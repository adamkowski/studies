import os


class File:
    def __init__(self, file_name):
        self.name = file_name
        with open(file_name, "rb") as f:
            self.content = f.read()
        self.length = len(self.content)

    @staticmethod
    def load_files(dirs):
        files = []
        for dir_path in dirs:
            files += File.__read_dir(dir_path)

        return files

    @staticmethod
    def __read_dir(dir_path):
        dir_content = []
        for path, dirs, files in os.walk(dir_path):
            for filename in files:
                dir_content.append(os.path.join(path, filename))

        return dir_content
