import sys

from src.deduplicator.deduplicator import Deduplicator
from src.file.file import File


def main():
    if len(sys.argv) < 2:
        print("At least one directory has to be specified")
        sys.exit(1)

    all_files = File.load_files(sys.argv[1:])

    duplicates = Deduplicator(all_files)
    Deduplicator.print(duplicates)


if __name__ == '__main__':
    main()
