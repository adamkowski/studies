from unittest import TestCase

from src.file.file import File


class TestFile(TestCase):
    def test_current_dir_contains_current_file(self):
        files = File.load_files(".")
        self.assertEqual('.\\test_file.py' in files, True)
