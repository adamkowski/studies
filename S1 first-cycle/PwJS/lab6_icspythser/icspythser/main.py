import sys

from icspythser.parser import Parser
from icspythser.summary import Summary


def read_content(file_name):
    try:
        with open(file_name, mode="r", encoding="UTF-8") as file:
            return file.read().splitlines()
    except FileNotFoundError:
        print("File '" + file_name + "' not found")
        sys.exit(1)


def main():
    if len(sys.argv) != 2:
        print("The .isc file has to be specified")
        sys.exit(1)

    content = read_content(sys.argv[1])
    uni_classes = Parser.get_uni_classes(content)

    Summary.print(uni_classes)

    table = Summary.generate_table(uni_classes)
    with open('calendar.csv', 'w') as file:
        file.write(table)


if __name__ == '__main__':
    main()
