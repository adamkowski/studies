from unittest import TestCase

from icspythser.parser import Parser


class TestParser(TestCase):
    def test_get_uni_classes_should_return_one_element(self):
        content = [
            'DTSTART;xyz:12345678T123456',
            'DTEND;asdf:12345678T123456',
            'SUMMARY:Asdf - Grupa: X_L_Y, Sala: 123']
        result = Parser.get_uni_classes(content)
        self.assertEqual(len(result), 1)

    def test_get_uni_classes_with_bad_data_should_return_empty_list(self):
        content = [
            'DTSTART;xyz:12345678T123456',
            'DTEND;asdf:12345678T123456',
            'SUMMARY:Asdf']
        result = Parser.get_uni_classes(content)
        self.assertEqual(len(result), 0)
