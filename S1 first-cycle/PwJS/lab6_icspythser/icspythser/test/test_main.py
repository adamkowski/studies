import os
from unittest import TestCase

from icspythser.main import read_content


class Test(TestCase):
    def setUp(self):
        file = open("test_tmp.txt", "w")
        file.write("First line\nSecond line")
        file.close()

    def tearDown(self):
        os.remove("test_tmp.txt")

    def test_read_content(self):
        content = read_content("test_tmp.txt")
        self.assertEqual(content[0], 'First line')
        self.assertEqual(content[1], 'Second line')
