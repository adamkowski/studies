from datetime import datetime


class Summary:
    @staticmethod
    def print(classes):
        summary = Summary.__create(classes)
        for study_form in summary:
            print(study_form)
            for subject in summary[study_form]:
                print('\t>', subject, end=" [ ")
                for class_form in summary[study_form][subject]:
                    print(class_form, end=":")
                    print(summary[study_form][subject][class_form], end=" ")
                print(']')

    @staticmethod
    def __create(classes):
        summary = {}
        for meeting in classes:
            if 0 <= datetime.strptime(meeting.date, '%Y-%m-%d').weekday() <= 4:
                study_form = 'Stacjonarne'
            else:
                study_form = 'Niestacjonarne'

            if study_form not in summary:
                summary[study_form] = {}

            if meeting.subject not in summary[study_form]:
                summary[study_form][meeting.subject] = {}

            if meeting.form not in summary[study_form][meeting.subject]:
                summary[study_form][meeting.subject][meeting.form] = 0

            duration = Summary.__get_classes_duration(
                meeting.date, meeting.hour_begin, meeting.hour_end)
            summary[study_form][meeting.subject][meeting.form] += duration

        return summary

    @staticmethod
    def __get_classes_duration(date, class_begin, class_end):
        lesson_unit = 45
        datetime_format = '%Y-%m-%dT%H:%M:%S'
        begin = datetime.strptime(date + "T" + class_begin,
                                  datetime_format).timestamp()
        end = datetime.strptime(date + "T" + class_end,
                                datetime_format).timestamp()

        return int(((end - begin) / 60) / lesson_unit)

    @staticmethod
    def generate_table(classes):
        table = '"DATA", "GODZ_OD", "GODZ_DO", "ZAJĘCIA", "GRUPA", "SALA"\n'
        for meeting in classes:
            line = (meeting.date, meeting.hour_begin, meeting.hour_end,
                    meeting.subject, meeting.group, meeting.classroom)
            table += '"' + '", "'.join(line) + '"\n'

        return table
