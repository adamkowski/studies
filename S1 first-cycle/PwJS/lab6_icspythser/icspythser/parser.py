import re

from icspythser.UniClass import UniClass


class Parser:
    PATTERN_START = re.compile(
        r"^DTSTART;.*(\d{4})(\d{2})(\d{2})T(\d{2})(\d{2})(\d{2})")
    PATTERN_END = re.compile(
        r"^DTEND.*\d{8}T(\d{2})(\d{2})(\d{2})")
    PATTERN_SUMMARY = re.compile(
        r"^SUMMARY:(.*) -.*Grupa: (.*_([LWA]).*), Sala: (.*)")

    @staticmethod
    def get_uni_classes(file):
        classes = []

        class_start = class_end = class_summary = None
        for line in file:
            match = re.match(Parser.PATTERN_START, line)
            if match:
                class_start = match.groups()

            match = re.match(Parser.PATTERN_END, line)
            if match:
                class_end = match.groups()

            match = re.match(Parser.PATTERN_SUMMARY, line)
            if match:
                class_summary = match.groups()

            if class_start and class_end and class_summary:
                lesson = UniClass(class_start, class_end, class_summary)
                classes.append(lesson)
                class_start = class_end = class_summary = None

        return classes
