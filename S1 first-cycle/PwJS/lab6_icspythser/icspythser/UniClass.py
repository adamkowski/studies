class UniClass:
    def __init__(self, begin, end, summary):
        self.date = '-'.join(begin[:3])
        self.hour_begin = ':'.join(begin[3:])
        self.hour_end = ':'.join(end[:])
        (self.subject, self.group, self.form, self.classroom) = summary
