from decrypter.decrypter import Decrypter


def load_data(file_name):
    """
    file_name:
    first line - 26 letters reflecting the frequency of characters in open text
    next lines - text encrypted with a substitution cipher
    """

    try:
        with open(file_name) as file:
            frequency = file.readline()
            encrypted = file.readlines()
    except FileNotFoundError:
        raise FileNotFoundError
    return frequency, encrypted


try:
    plain_text_freq, encrypted_text = load_data('cipher.txt')
    decrypted_text = Decrypter.decrypt(plain_text_freq, encrypted_text)
    print(decrypted_text)
except FileNotFoundError:
    print("File 'cipher.txt' not found.")
