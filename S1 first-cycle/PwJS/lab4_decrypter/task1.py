from histogram.hist_printer import print_histogram
from histogram.histogram import Histogram


def load_data(file_name):
    """file_name: file containing text"""

    try:
        with open(file_name) as file:
            return file.readlines()
    except FileNotFoundError:
        raise FileNotFoundError
        print('Error')


try:
    hist = Histogram()
    hist.addText(load_data('input.txt'))
    print_histogram(hist)
except FileNotFoundError:
    print("Input file not found.")