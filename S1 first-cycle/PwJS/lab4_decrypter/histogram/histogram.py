class Histogram(object):
    __LETTERS = ("A", "B", "C", "D", "E", "F", "G", "H", "I",
                 "J", "K", "L", "M", "N", "O", "P", "Q", "R",
                 "S", "T", "U", "V", "W", "X", "Y", "Z")

    def __init__(self):
        self.__histogram = {}
        self.__letters = 0

    @property
    def histogram(self):
        return self.__histogram

    @property
    def letters(self):
        return self.__letters

    def addText(self, text):
        for line in text:
            self.addLine(line)

    def addLine(self, line):
        for letter in line:
            self.addLetter(letter)

    def addLetter(self, letter):
        to_add = letter.upper()
        if to_add in self.__LETTERS:
            self.__letters += 1
            self.__put_letter_to_histogram(to_add)

    def __put_letter_to_histogram(self, letter):
        if letter not in self.__histogram:
            self.__histogram[letter] = 0
        self.__histogram[letter] += 1

    def sort_histogram(self):
        return {key: val for key, val in sorted(self.__histogram.items(),
                                                key=lambda item: item[1],
                                                reverse=True)}
