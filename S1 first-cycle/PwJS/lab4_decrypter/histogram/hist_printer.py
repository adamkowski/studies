def print_histogram(hist):
    histogram = hist.sort_histogram()
    letters = hist.letters
    for element in histogram:
        amount = histogram[element]
        percent = amount / letters * 100
        print(element, "\t", amount, "\t", "{:.2f}%".format(percent))
