import unittest

from histogram.histogram import Histogram


class MyTestCase(unittest.TestCase):
    def test_without_add_nothing_histogram_is_empty(self):
        hist = Histogram()
        self.assertEqual(bool(hist.histogram), False)

    def test_empty_histogram_contains_zero_letters(self):
        hist = Histogram()
        self.assertEqual(hist.letters == 0, True)

    def test_histogram_with_one_letter_contains_one_letter(self):
        hist = Histogram()
        hist.addLetter('A')
        self.assertEqual(hist.letters == 1, True)

    def test_after_add_letter_histogram_contains_this_letter(self):
        hist = Histogram()
        hist.addLetter('A')
        self.assertEqual('A' in hist.histogram, True)

    def test_after_add_non_letter_histogram_doesnt_contains_it(self):
        hist = Histogram()
        hist.addLetter('7')
        self.assertEqual('7' in hist.histogram, False)

    def test_after_string_histogram_contains_its_letters(self):
        hist = Histogram()
        hist.addLine('AB')
        self.assertEqual('A' in hist.histogram, True)
        self.assertEqual('B' in hist.histogram, True)

    def test_histogram_contains_proper_amount_of_inserted_letters(self):
        hist = Histogram()
        hist.addLine('A BAB AC')
        self.assertEqual(hist.histogram['A'] == 3, True)
        self.assertEqual(hist.histogram['B'] == 2, True)
        self.assertEqual(hist.histogram['C'] == 1, True)

    def test_after_sort_histogram_is_sorted(self):
        hist = Histogram()
        hist.addLine('abb')
        sorted_hist = str(hist.sort_histogram())
        self.assertEqual(sorted_hist == "{'B': 2, 'A': 1}", True)


if __name__ == '__main__':
    unittest.main()
