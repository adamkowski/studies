from histogram.histogram import Histogram


class Decrypter(object):
    @staticmethod
    def decrypt(plain_text_freq, encrypted_text):
        histogram = Histogram()
        histogram.addText(encrypted_text)
        encrypted_text_hist = histogram.sort_histogram()

        substitution_map = Decrypter.__substitute_letters(encrypted_text_hist,
                                                          plain_text_freq)

        return Decrypter.__decrypt_text(encrypted_text, substitution_map)

    @staticmethod
    def __substitute_letters(encrypted_text_hist, plain_text_freq):
        index = 0
        substitutions = {}
        for element in encrypted_text_hist:
            substitutions[plain_text_freq[index]] = element
            index += 1
        return substitutions

    @staticmethod
    def __decrypt_text(encrypted_text, substitution_map):
        decrypted_text = ""
        for line in encrypted_text:
            for letter in line:
                if letter in substitution_map:
                    decrypted_text += substitution_map[letter]
                else:
                    decrypted_text += letter
        return decrypted_text
