#! /usr/bin/perl

use File::Compare;
use Term::ANSIColor;
use Array::Diff;

die "Needed args:\n1) compiler\n2) path to test\n3) path to proper files" if @ARGV != 3;

my $compiler = $ARGV[0];
my @to_test = get_files($ARGV[1]);
my @proper_files = get_files($ARGV[2]);

compile_tests();
compare_files();


sub get_files($) {
	opendir my $dir, $_[0] or die "Cannot open directory: $!";
	my @files = readdir $dir;
	closedir $dir;
	
	@files = remove_dots(@files);
	
	return sort @files;
}

sub remove_dots($) {
	my @f = @_;
	for ($index = @f; $index >= 0; $index--) {
		if (@f[$index] eq '.' or @f[$index] eq '..') {
			splice(@f, $index, 1);
		}
	}
	return @f;
}

sub compile_tests() {
    print "╒═════════════════════════ COMPILE ═════════════════════════╕\n";
    foreach $test_file(@to_test) {
        $test_file =~ /(.*).txt/;
        $name_without_ext = $1;
        `./$compiler $ARGV[1]$test_file ./tmp/$name_without_ext`;
        my $result = `./$compiler $ARGV[1]$test_file ./tmp/$name_without_ext`;
        print "│ ";
        chomp($result);
        if ($result eq "") {
            printf ("%28s %-38s", $test_file, colored(['green'], '[OKAY]'));
        }
        else {
            printf ("%28s %-38s", $test_file, colored(['red'], $result));
        }
        print "│\n";
    }
}

sub compare_files() {
    print "╞═════════════════════════ COMPARE ═════════════════════════╡\n";
    foreach $test(@proper_files) {
        $compare = compare("$ARGV[2]/$test", "./tmp/$test");
        print "│ ";
        if ($compare == 0) {
            printf ("%28s %-38s", $test, colored(['green'], '[OKAY]'));
        }
        elsif ($compare == 1) {
            printf ("%28s %-38s", $test, colored(['yellow'], '[DIFF]'));
        }
        else {
            printf ("%28s %-38s", $test, colored(['red'], '[CHECK]'));
        }
        printf ("│\n");
    }
    
    my @all_to_test = get_files("./tmp");
    my $diff = Array::Diff->diff( \@all_to_test, \@proper_files );
    my @not_tested = @{$diff->deleted};
    
    if (scalar(@not_tested) > 0) {
        print "╞═══════════════════════ NOT VERIFIED ══════════════════════╡\n";
        foreach my $file(@not_tested) {
            printf ("│ %-57s │\n", $file);
        }
    }
    
    print "╘═══════════════════════════════════════════════════════════╛\n";
}

