%{
#include <stdlib.h>
#include <string.h>
#include "def.tab.hh"

extern int yylineno;
int yyerror(const char *,...);
%}

%%

"int"                           { return INT; }
"flo"                           { return FLOAT; }
"str"                           { return STRING; }

"if"                            { return IF; }
"else"                          { return ELSE; }
"while"                         { return WHILE; }

"output"                        { return OUTPUT; }
"input"                         { return INPUT; }

"fun"                           { return FUNCTION; }

\+                              { return '+'; }
\-                              { return '-'; }
\*                              { return '*'; }
\/                              { return '/'; }

\>                              { return '>'; }
\>\=                            { return GEQ; }
\<                              { return '<'; }
\<\=                            { return LEQ; }
\=                              { return '='; }
\!\=                            { return NEQ; }

\(                              { return '('; }
\)                              { return ')'; }
\{                              { return '{'; }
\}                              { return '}'; }
\[                              { return '['; }
\]                              { return ']'; }

\"                              { return '"'; }
\,                              { return ','; }

0|[1-9][0-9]*                   { yylval.ival = atoi(yytext); return NUM_INT; }
(0|[1-9][0-9]*)[.][0-9]+        { yylval.fval = atof(yytext); return NUM_REAL; }
[a-zA-Z_][a-zA-Z0-9_]*          { yylval.text = strdup(yytext); return ID; }
\".*\"                          { yylval.text = strdup(yytext); return TEXT; }

\ |\t                           { ; }
\;                              { return ';'; }
\n                              { yylineno++; }

.                               { yyerror("Error: unrecognized symbol\n"); }

%%

int yyerror(const char *msg,...)
{
    printf("%d: %s\n", yylineno, msg);
    exit(1);
}

