#include <string>
#include <vector>
#include <map>
#include <stack>

class SingleElement {
    public:
        SingleElement(int type, std::string value): type(type), value(value) { ; }
        
        int type;
        std::string value;
};

class Symbol {
    public:
        Symbol(int type, std::string size): type(type), size(size) { ; }
        Symbol(int type, std::string size, std::vector<SingleElement> sizes, std::vector<int> dims): 
            type(type), size(size), sizes(sizes), dims(dims) { ; }
            
        int type;
        std::string size;
        std::vector<SingleElement> sizes;
        std::vector<int> dims;
};

class Compiler {
    public:
        const std::string LABEL_IF = "LABEL";
        const std::string LABEL_WHILE = "WHILE";
        const std::string INTERNAL_VAR_PRINT = "__print";
        const std::string INTERNAL_VAR_CALC = "__calc_result";
        const std::string INTERNAL_VAR_FLOAT = "__float_tmp";
        const std::string FUN_DECL_BEGIN = "<<<beg";
        const std::string FUN_DECL_END = "<<<end";
        const char ARRAY_INDEX_ASSIGNMENT = 'a';
        const char ARRAY_GET_INDEX_VALUE = 'b';
        const std::string THREES_FILE = "threes.txt";
        const std::string SYMBOLS_FILE = "symbols.txt";

        enum types {
            NONE_TYPE = 0, INT_TYPE = 1, FLOAT_TYPE = 2, STRING_TYPE = 4, 
            ARRAY_INT = 5, ARRAY_FLOAT = 6 
        };
        enum variable_symbols { VAR_OK = 0, VAR_REDEFINED = 1 };

        std::stack<SingleElement> token_stack;
        std::map<std::string, Symbol*> symbols;
        std::vector<std::string> assembler_code;
        std::stack<std::string> labels;
        std::stack<std::string> while_labels;
        std::vector<SingleElement> sizes_tmp;

        int insert_symbol(std::string, int);
        int insert_symbol(std::string, int, std::string);
        void gen_print(std::string, types);
        void load_input(std::string);
        void gen_label();
        void insert_label();
        void gen_else_jump();
        void insert_end_while();
        void gen_while_labels();
        std::string process_functions();
        void call_function(std::string);
        std::string check_function_calls();
        void make_op(char, std::string);
        void gen_jump(std::string);
        void gen_loop(std::string);
        void create_float_tmp(float);

    private:
        std::vector<std::string> fun_called;
        std::vector<std::string> defined_functions;
        
        std::vector<int> get_dims();
        std::string gen_print_line1(types);
        std::string gen_print_line2(types, std::string);
        bool is_definition_correct(std::string);
        std::string gen_assignment_line(SingleElement, SingleElement, std::string);
        std::string gen_arith_op_line(std::string, SingleElement, SingleElement);
        std::string gen_load_line(SingleElement, int);
        void append_single_three_to_file(std::string);
        std::string gen_jump_line(int, std::string, int);
        void gen_get_array_index_value(std::string, SingleElement);
        void gen_assign_array_index(std::string, SingleElement);
        void gen_assign_variable(std::string, SingleElement);
        void gen_arithmetic_operation(std::string, SingleElement, char, std::string);
        std::string gen_conversion(int, SingleElement, SingleElement);
        int get_type(SingleElement);
};

