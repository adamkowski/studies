#include "compiler.hh"
#include <algorithm>
#include <sstream>
#include <fstream>

int Compiler::insert_symbol(std::string sym, int type) {
    if (symbols.find(sym) == symbols.end()) {
        int size_total = 1;
        for (auto size : sizes_tmp) {
            size_total *= std::stoi(size.value);
        }

        std::vector<int> dims = get_dims();
        symbols[sym] = new Symbol(type, std::to_string(size_total), sizes_tmp, dims);   

        sizes_tmp.clear();
        return VAR_OK;
    }
    return VAR_REDEFINED;
}

std::vector<int> Compiler::get_dims() {
    std::vector<int> dims;
    std::vector<SingleElement> reversed_sizes(sizes_tmp.rbegin(), sizes_tmp.rend());
    int dim_size = 1;
    for (auto size : reversed_sizes) {
        dims.push_back(dim_size);
        dim_size *= std::stoi(size.value);
    }
    std::reverse(dims.begin(), dims.end());
    return dims;
}

int Compiler::insert_symbol(std::string sym, int type, std::string size) {
    if (symbols.find(sym) == symbols.end()) {
        symbols[sym] = new Symbol(type, size);
        return VAR_OK;
    }
    return VAR_REDEFINED;
}

void Compiler::gen_print(std::string value, types print_register) {
    types pr_reg = print_register;
    std::string val = value;

    static int tmp_number = 0;
    std::string print_tmp = INTERNAL_VAR_PRINT + std::to_string(++tmp_number);

    if (print_register == NONE_TYPE) {
        pr_reg = static_cast<types>(symbols[value]->type);
        print_tmp = value;
    }
    else { 
        insert_symbol(print_tmp, pr_reg, value);
    }

    std::string line1 = "\t" + gen_print_line1(pr_reg);
    std::string line2 = "\t" + gen_print_line2(pr_reg, print_tmp);
    std::string line3 = "\tsyscall\n";
    assembler_code.push_back(line1);
    assembler_code.push_back(line2);
    assembler_code.push_back(line3);
}

std::string Compiler::gen_print_line1(types pr_reg) {
    return "li $v0 , " + std::to_string(pr_reg);
}

std::string Compiler::gen_print_line2(types pr_reg, std::string variable_name) {
    std::stringstream line;
    line << "l";
    switch (pr_reg) {
        case INT_TYPE:
            line << "w $a0 , " << variable_name;
            break;
        case FLOAT_TYPE:
            line << ".s $f12 , " << variable_name;
            break;
        case STRING_TYPE:
            line << "a $a0 , " << variable_name;
            break;
    }
    return line.str();
}

void Compiler::load_input(std::string var_name) {
    int var_type = symbols[var_name]->type;

    std::string line1 = "\tli $v0, ";
    std::string line2 = "\tsyscall";
    std::string line3 = "\ts";
    if (var_type == INT_TYPE) {
        line1 += "5";
        line3 += "w $v0, ";
    }
    else if (var_type == FLOAT_TYPE) {
        line1 += "6";
        line3 += ".s $f0, ";
    }
    line3 += var_name + "\n";

    assembler_code.push_back(line1);
    assembler_code.push_back(line2);
    assembler_code.push_back(line3);
}

void Compiler::gen_label() {
    static int counter = 0;
    std::string label = LABEL_IF + std::to_string(++counter);
    labels.push(label);
}

void Compiler::insert_label() {
    std::string lab = labels.top() + ":";
    labels.pop();
    assembler_code.push_back(lab);
}

void Compiler::gen_else_jump() {
    std::string label1 = labels.top() + ":";
    labels.pop();
    gen_label();
    std::string label2 = labels.top();
    
    assembler_code.push_back("\tj " + label2);
    assembler_code.push_back(label1);
}

void Compiler::insert_end_while() {
    std::string label1 = while_labels.top() + ":";
    while_labels.pop();
    std::string label2 = while_labels.top();
    while_labels.pop();
    assembler_code.push_back("\tj " + label2);
    assembler_code.push_back(label1);
}

void Compiler::gen_while_labels() {
    static int counter = 0;
    for (int i = 0; i < 2; i++) {
        std::string label = LABEL_WHILE + std::to_string(++counter);
        while_labels.push(label);
    }
}

std::string Compiler::process_functions() {
    std::string line;
    std::vector<std::string> code = assembler_code;
    assembler_code.clear();
    std::vector<std::string> functions_tmp;
    std::vector<std::string> fun;
    
    unsigned int line_no = 0;
    while (line_no < code.size()) {
        line = code.at(line_no);
        if (line.rfind(FUN_DECL_BEGIN, 0) == 0) {
            line = code.at(++line_no);
            while (line.rfind(FUN_DECL_END, 0) != 0) {
                functions_tmp.push_back(line);
                line = code.at(++line_no);
            }
            
            line = code.at(++line_no);
            
            if (is_definition_correct(line) == false) {
                return line;
            }
            defined_functions.push_back(line);
            
            fun.push_back(line + ":");
            fun.insert(std::end(fun), std::begin(functions_tmp), std::end(functions_tmp));
            fun.push_back("\tjr $ra\n");
            
            functions_tmp.clear();
            line = code.at(++line_no);
        }
        
        assembler_code.push_back(line);
        line_no++;
    }
    
    assembler_code.push_back("\tli $v0 , 10\n\tsyscall\n");
    assembler_code.insert(std::end(assembler_code), std::begin(fun), std::end(fun));
    
    return "";
}

bool Compiler::is_definition_correct(std::string fun_name) {
    std::vector<std::string>::iterator search;
    search = std::find(defined_functions.begin(), defined_functions.end(), fun_name);
    if (search != defined_functions.end()) {
        return false;
    }
    return true;
}

void Compiler::call_function(std::string name) {
    std::string line = "\tjal " + name + "\n";
    fun_called.push_back(name);
    assembler_code.push_back(line);
}

std::string Compiler::check_function_calls() {
    for (auto called : fun_called) {
        std::vector<std::string>::iterator search;
        search = std::find(defined_functions.begin(), defined_functions.end(), called);
        
        if (search == defined_functions.end()) {
            return called;
        }
    }
    return "";
}

void Compiler::make_op(char operation_sign, std::string operation_assembler) {
    static int temp_var_counter = 0;
    std::string result_name = INTERNAL_VAR_CALC + std::to_string(++temp_var_counter);
    SingleElement element(NONE_TYPE, result_name);

    if (operation_sign == ARRAY_GET_INDEX_VALUE)
        gen_get_array_index_value(result_name, element);
    else if (operation_sign == ARRAY_INDEX_ASSIGNMENT)
        gen_assign_array_index(result_name, element);
    else if (operation_sign == '=')
        gen_assign_variable(result_name, element);
    else
        gen_arithmetic_operation(result_name, element, operation_sign, operation_assembler);
}

void Compiler::gen_get_array_index_value(std::string result_name, SingleElement element) {
    SingleElement se1 = token_stack.top();
    token_stack.pop();

    std::stringstream array_index_value;

    std::string array_name = se1.value;
    std::vector<int> dims = symbols[array_name]->dims;
    int array_index = 0;
    
    array_index_value << "\tli $t4 , 0" << std::endl;
    
    for (int i = 0; i < sizes_tmp.size(); i++) {
        if (sizes_tmp[i].type == NONE_TYPE)
            array_index_value << "\tlw $t6 , " << sizes_tmp[i].value << std::endl;
        else if (sizes_tmp[i].type == INT_TYPE)
            array_index_value << "\tli $t6 , " << sizes_tmp[i].value << std::endl;

        array_index_value << "\tli $t7 , " << dims[i] << std::endl
                          << "\tmul $t6 , $t6 , $t7" << std::endl
                          << "\tadd $t4 , $t4 , $t6" << std::endl;
    }
    sizes_tmp.clear();

    array_index_value << "\tmul $t4 , $t4 , 4" << std::endl
                      << "\tla $t5 , " << array_name << std::endl
                      << "\tadd $t4 , $t4 , $t5" << std::endl;

    if (symbols[array_name]->type == ARRAY_INT) {
        array_index_value << "\tlw $t0 , ($t4)" << std::endl
                          << "\tsw $t0 , " << result_name << std::endl;
        insert_symbol(result_name, INT_TYPE, "0");
    }
    else if (symbols[array_name]->type == ARRAY_FLOAT) {
        array_index_value << "\tl.s $f0 , ($t4)" << std::endl
                          << "\ts.s $f0 , " << result_name << std::endl;
        insert_symbol(result_name, FLOAT_TYPE, "0.0");
    }
    array_index_value << "\tli $t4 , 0" << std::endl;

    assembler_code.push_back(array_index_value.str());
    token_stack.push(element);
}

void Compiler::gen_assign_array_index(std::string result_name, SingleElement element) {
    SingleElement se2 = token_stack.top();
    token_stack.pop();
    SingleElement se1 = token_stack.top();
    token_stack.pop();
    
    std::string array_name = se2.value;
    std::vector<int> dims = symbols[array_name]->dims;
    
    std::stringstream assignment_to_array;
    assignment_to_array << gen_load_line(se1, 0) << std::endl
                        << "\tla $t4 , " << array_name << std::endl;

    assignment_to_array << "\tli $t5 , 0" << std::endl;

    for (int i = 0; i < sizes_tmp.size(); i++) {
        if (sizes_tmp[i].type == NONE_TYPE)
            assignment_to_array << "\tlw $t6 , " << sizes_tmp[i].value << std::endl;
        else if (sizes_tmp[i].type == INT_TYPE)
            assignment_to_array << "\tli $t6 , " << sizes_tmp[i].value << std::endl;

        assignment_to_array << "\tli $t7 , " << dims[i] << std::endl
                            << "\tmul $t6 , $t6 , $t7" << std::endl
                            << "\tadd $t5 , $t5 , $t6" << std::endl;
    }
    sizes_tmp.clear();

    assignment_to_array << "\tmul $t5 , $t5 , 4" << std::endl
                        << "\tadd $t4 , $t4 , $t5" << std::endl
                        << "\tli $t5 , 0" << std::endl;

    if (se1.type == INT_TYPE)
        assignment_to_array << "\tsw $t0 , ($t4)" << std::endl;
    else
        assignment_to_array << gen_assignment_line(se1, se2, "($t4)") << std::endl;

    assembler_code.push_back(assignment_to_array.str());
}

void Compiler::gen_assign_variable(std::string result_name, SingleElement element) {
    SingleElement se2 = token_stack.top();
    token_stack.pop();
    SingleElement se1 = token_stack.top();
    token_stack.pop();

    std::stringstream assignment;
    assignment << gen_load_line(se1, 0) << std::endl;
    
    int left_type = get_type(se2);
    int right_type = get_type(se1);
    if (left_type == INT_TYPE && right_type == FLOAT_TYPE) {
        assignment << "\tcvt.w.s $f1, $f0" << std::endl
                   << "\tmfc1 $t0, $f1" << std::endl
                   << "\tsw $t0 , " << se2.value << std::endl;
    }
    else {
        if (se1.type == INT_TYPE)
            assignment << "\tsw $t0 , " << se2.value << std::endl;
        else
            assignment << gen_assignment_line(se1, se2, se2.value) << std::endl;
    }

    assembler_code.push_back(assignment.str());
}

void Compiler::gen_arithmetic_operation(std::string result_name, SingleElement element, 
        char operation_sign, std::string operation_assembler) {

    SingleElement se2 = token_stack.top();
    token_stack.pop();
    SingleElement se1 = token_stack.top();
    token_stack.pop();

    std::stringstream three_stream;
    three_stream << element.value << " <= " << se1.value << " " 
                 << se2.value << " " << operation_sign;
    append_single_three_to_file(three_stream.str());

    std::stringstream code;
    code << gen_load_line(se1, 0) << std::endl
         << gen_conversion(0, se2, se1)
         << gen_load_line(se2, 1) << std::endl
         << gen_conversion(1, se1, se2)
         << gen_arith_op_line(operation_assembler, se1, se2) << std::endl
         << gen_assignment_line(se1, se2, result_name) << std::endl;

    assembler_code.push_back(code.str());
    token_stack.push(element);

    int type_1 = get_type(se1);
    int type_2 = get_type(se2);
    if (type_1 == INT_TYPE && type_2 == INT_TYPE) {
        insert_symbol(result_name, INT_TYPE, "0");
    }
    else if (type_1 == FLOAT_TYPE || type_2 == FLOAT_TYPE) {
        insert_symbol(result_name, FLOAT_TYPE, "0.0");
    }
    else {
        if (type_1 == NONE_TYPE) {
            auto symbol1 = symbols.find(se1.value);
            if (symbol1->second->type == INT_TYPE)
                insert_symbol(result_name, INT_TYPE, "0");
            else
                insert_symbol(result_name, FLOAT_TYPE, "0.0");
        }
    }
}

std::string Compiler::gen_assignment_line(SingleElement e1, SingleElement e2, std::string result) {
    std::string line = "\ts";
    if (get_type(e1) == FLOAT_TYPE || get_type(e2) == FLOAT_TYPE)
        line += ".s $f0";
    else
        line += "w $t0";
    
    line += " , " + result;
    return line;
}

std::string Compiler::gen_arith_op_line(std::string oper, SingleElement e1, SingleElement e2) {
    std::string line = "\t" + oper;
    if (get_type(e1) == FLOAT_TYPE || get_type(e2) == FLOAT_TYPE) {
        line += ".s $f0 , $f0 , $f1";
    }
    else {
        line += " $t0 , $t0 , $t1";
    }
    return line;
}

std::string Compiler::gen_load_line(SingleElement op, int regno) {
    std::stringstream line;
    line << "\tl";
    
    if (op.type == INT_TYPE) {
        line << "i $t";
    }
    else {
        auto symbol = symbols.find(op.value);
        if (symbol->second->type == INT_TYPE)
            line << "w $t";
        else
            line << ".s $f";
    }
        
    line << regno << " , " << op.value;
    return line.str();
}

std::string Compiler::gen_conversion(int reg_no, SingleElement first, SingleElement second) {
    if (!(get_type(first) == FLOAT_TYPE && get_type(second) == INT_TYPE))
        return "";

    std::stringstream conv;
    conv << "\tmtc1 $t" << reg_no << " , " << "$f" << reg_no << std::endl
         << "\tcvt.s.w $f" << reg_no << " , " << "$f" << reg_no << std::endl;
    return conv.str();
}

int Compiler::get_type(SingleElement element) {
    int type;
    if (element.type == NONE_TYPE) {
        auto symbol = symbols.find(element.value);
        if (symbol->second->type == INT_TYPE)
            type = INT_TYPE;
        else if (symbol->second->type == FLOAT_TYPE)
            type = FLOAT_TYPE;
        else if (symbol->second->type == ARRAY_INT)
            type = ARRAY_INT;
        else
            type = ARRAY_FLOAT;
    }
    else {
        type = INT_TYPE;
    }
    return type;
}

void Compiler::append_single_three_to_file(std::string single_three) {
    std::ofstream file(THREES_FILE, file.app);
    file << single_three << std::endl;
    file.close();
}

void Compiler::gen_jump(std::string jump) {
    SingleElement e2 = token_stack.top();
    token_stack.pop();
    SingleElement e1 = token_stack.top();
    token_stack.push(e2);

    std::stringstream comparison;
    comparison << gen_jump_line(e1.type, e1.value, 0) << std::endl;
    comparison << gen_jump_line(e2.type, e2.value, 1) << std::endl;
    comparison << "\t" << jump << " $t0 , $t1 , " << labels.top() << std::endl;
    
    assembler_code.push_back(comparison.str());
}

void Compiler::gen_loop(std::string jump) {
    SingleElement e2 = token_stack.top();
    token_stack.pop();
    SingleElement e1 = token_stack.top();
    token_stack.push(e2);

    std::string tmp = while_labels.top();
    while_labels.pop();
    std::string label = while_labels.top() + ":";
    while_labels.push(tmp);

    std::stringstream comparison;
    comparison << gen_jump_line(e1.type, e1.value, 0) << std::endl
               << gen_jump_line(e2.type, e2.value, 1) << std::endl
               << "\t" << jump << " $t0 , $t1 , " << while_labels.top() << std::endl;
    
    assembler_code.push_back(label);
    assembler_code.push_back(comparison.str());
}

std::string Compiler::gen_jump_line(int t, std::string value, int regno) {
    std::stringstream code;
    code << "\tl";
    int type = t;
    if (t == NONE_TYPE) {
        type = symbols[value]->type;
        code << "w $t";
    } 
    else if (t == INT_TYPE) {
        type = INT_TYPE;
        code << "i $t";
    }
    code << regno << " , " << value;
    return code.str();
}

void Compiler::create_float_tmp(float value) {
    static int float_tmp_counter = 0;
    std::string float_var = INTERNAL_VAR_FLOAT + std::to_string(++float_tmp_counter);
    SingleElement e(NONE_TYPE, float_var); 
	token_stack.push(e);
	insert_symbol(float_var, FLOAT_TYPE, std::to_string(value));
}

