%{

#include "compiler.hh"
#include <fstream>
#include <sstream>

#define INFILE_ERROR 1
#define OUTFILE_ERROR 2

extern "C" int yylex();
extern "C" int yyerror(const char* msg, ...);
extern FILE* yyin;
extern FILE* yyout;

Compiler compiler;

%}

%union {
    char *text;
    int ival;
    double fval;
};
%token<text> ID
%token<ival> NUM_INT
%token<fval> NUM_REAL
%token<text> TEXT
%token LEQ GEQ NEQ 
%token INT FLOAT STRING 
%token IF ELSE 
%token WHILE
%token OUTPUT INPUT
%token FUNCTION

%%

PROGRAM
    :LINE                           { ; }
    |PROGRAM LINE                   { ; }
    ;
LINE
    :FUNCTION_DECL                  { ; }
    |EXPR_SEMICOLON                 { ; }
    |CONDITIONAL_STATEMENT          { compiler.insert_label(); }
    |STATEMENT_WHILE                { compiler.insert_end_while(); }
    ;
EXPR_SEMICOLON
    :FUN_CALL ';'                   { ; }
    |EXPR_DECLARATION ';'           { ; }
    |EXPR_ASSIGNMENT ';'            { ; }
    |EXPR_PRINT ';'                 { ; }
    ;
FUN_CALL
    :ID '(' ')'                     { compiler.call_function($1); }
    ;
EXPR_DECLARATION
    :INT ID                         {
                                        if (compiler.insert_symbol($2, compiler.INT_TYPE, 
                                                "0") == compiler.VAR_REDEFINED) {
                                            std::string msg($2);
                                            msg += ": variable is already defined.";
                                            yyerror(msg.c_str());
                                        }
                                    }
    |FLOAT ID                       { 
                                        if (compiler.insert_symbol($2, compiler.FLOAT_TYPE, 
                                                "0.0") == compiler.VAR_REDEFINED) {
                                            std::string msg($2);
                                            msg += ": variable is already defined.";
                                            yyerror(msg.c_str());
                                        }
                                    }
    |INT DIM_DECL ID                { 
                                        if (compiler.insert_symbol($3, compiler.ARRAY_INT) == 
                                                compiler.VAR_REDEFINED) {
                                            std::string msg($3);
                                            msg += ": array is already defined.";
                                            yyerror(msg.c_str());
                                        }
                                    }
    |FLOAT DIM_DECL ID              { 
                                        if (compiler.insert_symbol($3, compiler.ARRAY_FLOAT) == 
                                                compiler.VAR_REDEFINED) {
                                            std::string msg($3);
                                            msg += ": array is already defined.";
                                            yyerror(msg.c_str());
                                        } 
                                    }
    ;
DIM_DECL
    :'[' SIZE_CONST ']'             { ; }
    ;
SIZE_CONST
    :SIZE_CONST ',' SIZE_VALUE      { ; }
    |SIZE_VALUE                     { ; }
    ;
SIZE_VALUE
    :NUM_INT                        { 
                                        SingleElement e(compiler.INT_TYPE, std::to_string($1));
                                        compiler.sizes_tmp.push_back(e);
                                    }
    |ID                             {
	                                    if (compiler.symbols[$1]->type != compiler.INT_TYPE) {
	                                        std::string msg($1);
                                            msg += ": variable have to be an int";
                                            yyerror(msg.c_str());
	                                    }
	                                    SingleElement e(compiler.NONE_TYPE, $1);
                                        compiler.sizes_tmp.push_back(e);
                                    }
    ;
EXPR_ASSIGNMENT
    :ID '{' EXPR '}'                {
                                        if (compiler.symbols.find($1) == compiler.symbols.end()) {
                                            std::string msg($1);
                                            msg += ": variable does not exist.";
                                            yyerror(msg.c_str());
                                        }
                                        SingleElement e(compiler.NONE_TYPE, $1);
                                        compiler.token_stack.push(e);
                                        compiler.make_op('=', "sw");
                                    }
    |ID '{' SCAN '}'                {
                                        if (compiler.symbols.find($1) == compiler.symbols.end()) {
                                            std::string msg($1);
                                            msg += ": variable does not exist.";
                                            yyerror(msg.c_str());
                                        }
                                        compiler.load_input($1); 
                                    }
    |ID DIM_DECL '{' EXPR '}'       {
                                        if (compiler.symbols.find($1) == compiler.symbols.end()) {
                                            std::string msg($1);
                                            msg += ": array does not exist.";
                                            yyerror(msg.c_str());
                                        }
                                        SingleElement e(compiler.NONE_TYPE, $1);
                                        compiler.token_stack.push(e);
                                        compiler.make_op(compiler.ARRAY_INDEX_ASSIGNMENT, "sw");
                                    }
    ;
EXPR
    :ID DIM_DECL                    { 
                                        SingleElement e(compiler.NONE_TYPE, $1);
                                        compiler.token_stack.push(e);
                                        compiler.make_op(compiler.ARRAY_GET_INDEX_VALUE, ""); 
                                    }
    |EXPR '+' COMPONENT             { compiler.make_op('+', "add"); }
    |EXPR '-' COMPONENT             { compiler.make_op('-', "sub"); }
    |COMPONENT                      { ; }
    ;
COMPONENT
    :COMPONENT '*' FACTOR           { compiler.make_op('*', "mul"); }
    |COMPONENT '/' FACTOR           { compiler.make_op('/', "div"); }
    |FACTOR                         { ; }
    ;
FACTOR
    :ID                             {
	                                    SingleElement e(compiler.NONE_TYPE, $1);
	                                    compiler.token_stack.push(e);
	                                }
    |NUM_INT                        { 
	                                    SingleElement e(compiler.INT_TYPE, std::to_string($1)); 
	                                    compiler.token_stack.push(e);
	                                }
    |NUM_REAL                       { compiler.create_float_tmp($1); }
    |'(' EXPR ')'                   { ; }
    ;
SCAN
    :INPUT '(' ')'                  { ; }
    ;
EXPR_PRINT
    :OUTPUT '(' NUM_INT ')'         { compiler.gen_print(std::to_string($3), compiler.INT_TYPE); }
    |OUTPUT '(' NUM_REAL ')'        { compiler.gen_print(std::to_string($3), compiler.FLOAT_TYPE); }
    |OUTPUT '(' TEXT ')'            { compiler.gen_print($3, compiler.STRING_TYPE); }
    |OUTPUT '(' ID ')'              { compiler.gen_print($3, compiler.NONE_TYPE); }
    ;
CONDITIONAL_STATEMENT
    :PART_IF PART_ELSE              { ; }
    |PART_IF                        { ; }
    ;
PART_IF
    :IF_BEGIN '{' CODE_BLOCK '}'    { ; }
    ;
PART_ELSE
    :ELSE_BEGIN '{' CODE_BLOCK '}'  { ; }
    ;
IF_BEGIN
    :IF '(' COND_EXPR ')'           { ; }
    ;
ELSE_BEGIN
    :ELSE                           { compiler.gen_else_jump(); }
    ;
COND_EXPR
    :EXPR '=' EXPR                  { compiler.gen_label(); compiler.gen_jump("bne"); }
    |EXPR NEQ EXPR                  { compiler.gen_label(); compiler.gen_jump("beq"); }
    |EXPR '<' EXPR                  { compiler.gen_label(); compiler.gen_jump("bge"); }
    |EXPR LEQ EXPR                  { compiler.gen_label(); compiler.gen_jump("bgt"); }
    |EXPR '>' EXPR                  { compiler.gen_label(); compiler.gen_jump("ble"); }
    |EXPR GEQ EXPR                  { compiler.gen_label(); compiler.gen_jump("blt"); }
    ;
STATEMENT_WHILE
    :WHILE_BEGIN '{' CODE_BLOCK '}' { ; }
    ;
WHILE_BEGIN
    :WHILE '(' COND_WHILE ')'       { ; }
    ;
COND_WHILE
    :EXPR '=' EXPR                  { compiler.gen_while_labels(); compiler.gen_loop("bne"); }
    |EXPR NEQ EXPR                  { compiler.gen_while_labels(); compiler.gen_loop("beq"); }
    |EXPR '<' EXPR                  { compiler.gen_while_labels(); compiler.gen_loop("bge"); }
    |EXPR LEQ EXPR                  { compiler.gen_while_labels(); compiler.gen_loop("bgt"); }
    |EXPR '>' EXPR                  { compiler.gen_while_labels(); compiler.gen_loop("ble"); }
    |EXPR GEQ EXPR                  { compiler.gen_while_labels(); compiler.gen_loop("blt"); }
    ;
CODE_BLOCK
    :LINE                           { ; }
    |CODE_BLOCK LINE                { ; }   
    ;
FUNCTION_DECL
    :FUNCTION ID '('')' BEG MID END {
                                        compiler.assembler_code.push_back($2);
                                        compiler.assembler_code.push_back("\n");
                                    }
    ;
BEG
    :'{'                            { compiler.assembler_code.push_back(compiler.FUN_DECL_BEGIN); }
    ;
END
    :'}'                            { compiler.assembler_code.push_back(compiler.FUN_DECL_END); }
    ;
MID
    :CODE_BLOCK                     { ; }
    ;

%%

void save_symbols_to_file() {
    std::ofstream symbols_file(compiler.SYMBOLS_FILE);
        
    for (auto symbol : compiler.symbols) {
        int array_size = 0;
        symbols_file << symbol.first << ":";
        switch (symbol.second->type) {
            case compiler.INT_TYPE:
                symbols_file << " .word " << symbol.second->size;
                break;
            case compiler.FLOAT_TYPE:
                symbols_file << " .float " << symbol.second->size;
                break;
            case compiler.STRING_TYPE:
                symbols_file << " .asciiz " << symbol.second->size;
                break;
            case compiler.ARRAY_INT:
            case compiler.ARRAY_FLOAT:
                array_size = std::stoi(symbol.second->size) * 4;
                symbols_file << " .space " << array_size;
                break;
            default:
                symbols_file << " .error";
        }
        symbols_file << std::endl;
    }
    
    symbols_file.close();
}

void save_code_to_yyout() {
    std::stringstream strings_to_end;
    fprintf(yyout, ".data\n");
    for (auto symbol : compiler.symbols) {
        std::string size = symbol.second->size;
        int array_size = 0;

        switch (symbol.second->type) {
            case compiler.INT_TYPE:
                fprintf(yyout, "\t%s:\t.word\t%s\n", symbol.first.c_str(), size.c_str());
                break;
            case compiler.FLOAT_TYPE:
                fprintf(yyout, "\t%s:\t.float\t%s\n", symbol.first.c_str(), size.c_str());
                break;
            case compiler.STRING_TYPE:
                strings_to_end << "\t" << symbol.first << ":\t.asciiz\t" << size << std::endl;
                break;
            case compiler.ARRAY_INT:
            case compiler.ARRAY_FLOAT:
                array_size = std::stoi(symbol.second->size) * 4;
                fprintf(yyout, "\t%s:\t.space\t%s\n", 
                    symbol.first.c_str(), std::to_string(array_size).c_str());
                break;
        }
    }
    
    fprintf(yyout, "%s\n", strings_to_end.str().c_str());
    
    fprintf(yyout, ".text\n");
    for (auto line : compiler.assembler_code) {
        fprintf(yyout, "%s\n", line.c_str());
    }
}

int main(int argc, char *argv[]) {
    if (argc == 3) {
        yyin = fopen(argv[1], "r");
        if (yyin == NULL) {
            printf("Error\n");
            return INFILE_ERROR;
        }

        yyout=fopen(argv[2], "w");
        if (yyout == NULL) {
            printf("Error\n");
            return OUTFILE_ERROR;
        }
        
        std::ofstream threes(compiler.THREES_FILE);
        threes.close();
    }
    else {
        printf("Error: no input and output files specified in args.\n");
        return -1;
    }

    yyparse();
    
    std::string redefined = compiler.process_functions();
    if (!redefined.empty()) {
        std::string message = "Redefined function '" + redefined + "'";
        yyerror(message.c_str());
    }
    
    std::string call_undef = compiler.check_function_calls();
    if (!call_undef.empty()) {
        std::string message = "Calling undefined function '" + call_undef + "'";
        yyerror(message.c_str());
    }
    
    save_symbols_to_file();
    save_code_to_yyout();
    
    return 0;
}

