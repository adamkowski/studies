<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="document">
	<html>
	<head>
		<title>XML Lab #3</title>
		<style>
			table, th, td { border: 1px solid; border-collapse: collapse; padding: 8px; }
			th { background: lightgrey; }
		</style>
	</head>
	<body>
		<h1>Lista osób</h1>
		<xsl:apply-templates select="person_list"/>
	</body>
	</html>
</xsl:template>

<xsl:template match="person_list">
	<table>
		<tr>
			<th>Imię</th>
			<th>Nazwisko</th>
			<th>Pesel</th>
			<th>Data urodzenia</th>
			<th>Płeć</th>
			<th>Ulica</th>
			<th>Nr domu</th>
			<th>Nr mieszkania</th>
			<th>Miasto</th>
			<th>Kod pocztowy</th>
			<th>Nr telefonu</th>
			<th>Email</th>
			<th>Zawód</th>
		</tr>
		<xsl:apply-templates select="person"/>
	</table>
</xsl:template>

<xsl:template match="person">
	<tr>
		<xsl:apply-templates select="first_name | last_name | pesel | birth_date | sex | address | phone_number | email | profession"/>
	</tr>
</xsl:template>

<xsl:template match="address">
	<xsl:apply-templates select="street | house_number | apartment_number | city | post_code"/>
</xsl:template>

<xsl:template match="first_name | last_name | pesel | birth_date | sex | phone_number | email | profession | street | house_number | apartment_number | city | post_code">
	<td><xsl:value-of select="."/></td>
</xsl:template>

</xsl:stylesheet>
