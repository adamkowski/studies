<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:template match="document">

    <fo:root>
	
		<fo:layout-master-set>
			<fo:simple-page-master master-name="page" reference-orientation="90">
				<fo:region-body margin="1cm"/>
				<fo:region-before extent="1cm"/>
				<fo:region-after extent="1cm"/>
			</fo:simple-page-master>
		</fo:layout-master-set>
		
		<fo:page-sequence master-reference="page" id="sequence" font-size="7pt">
		
			<fo:static-content flow-name="xsl-region-before">
				<fo:block text-align="left" border="1px solid black">
					<xsl:text>Lista osób</xsl:text>
				</fo:block>
			</fo:static-content>
			
			<fo:static-content flow-name="xsl-region-after">
				<fo:block text-align="right" border="1px solid black">
					<xsl:text>Strona </xsl:text>
					<fo:page-number/>
					<xsl:text> / </xsl:text>
					<fo:page-number-citation-last ref-id="sequence"/>
				</fo:block>
			</fo:static-content>
			
			<fo:flow flow-name="xsl-region-body">
				<fo:block>
					<xsl:apply-templates select="person_list"/>
				</fo:block>
			</fo:flow>
			
		</fo:page-sequence>
		
	</fo:root>
	
</xsl:template>

<xsl:template match="person_list">
	<fo:table border="1px solid black">
		<fo:table-column border="1px solid black"/>
		<fo:table-column border="1px solid black"/>
		<fo:table-column border="1px solid black"/>
		<fo:table-column border="1px solid black"/>
		<fo:table-column border="1px solid black"/>
		<fo:table-column border="1px solid black"/>
		<fo:table-column border="1px solid black"/>
		<fo:table-column border="1px solid black"/>
		<fo:table-column border="1px solid black"/>
		<fo:table-column border="1px solid black"/>
		<fo:table-column border="1px solid black"/>
		<fo:table-column border="1px solid black"/>
		<fo:table-column border="1px solid black"/>
		
		<fo:table-header>
			<fo:table-row border="1px solid black">
				<fo:table-cell>
					<fo:block font-weight="bold">First name</fo:block>
				</fo:table-cell>
				<fo:table-cell>
					<fo:block font-weight="bold">Last name</fo:block>
				</fo:table-cell>
				<fo:table-cell width="2cm">
					<fo:block font-weight="bold">Pesel</fo:block>
				</fo:table-cell>
				<fo:table-cell width="20mm">
					<fo:block font-weight="bold">Birth date</fo:block>
				</fo:table-cell>
				<fo:table-cell width="5mm">
					<fo:block font-weight="bold">Sex</fo:block>
				</fo:table-cell>
				<fo:table-cell>
					<fo:block font-weight="bold">Street</fo:block>
				</fo:table-cell>
				<fo:table-cell width="1cm">
					<fo:block font-weight="bold">House</fo:block>
				</fo:table-cell>
				<fo:table-cell width="15mm">
					<fo:block font-weight="bold">Apartment</fo:block>
				</fo:table-cell>
				<fo:table-cell>
					<fo:block font-weight="bold">City</fo:block>
				</fo:table-cell>
				<fo:table-cell width="1cm">
					<fo:block font-weight="bold">Post code</fo:block>
				</fo:table-cell>
				<fo:table-cell>
					<fo:block font-weight="bold">Phone</fo:block>
				</fo:table-cell>
				<fo:table-cell>
					<fo:block font-weight="bold">Email</fo:block>
				</fo:table-cell>
				<fo:table-cell>
					<fo:block font-weight="bold">Proffesion</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-header>
		
		<fo:table-body>
			<xsl:apply-templates select="person"/>
		</fo:table-body>
		
	</fo:table>
</xsl:template>

<xsl:template match="person">
	<fo:table-row border="1px solid black">
		<xsl:apply-templates select="first_name | last_name | pesel | birth_date | sex | address | phone_number | email | profession"/>
	</fo:table-row>
</xsl:template>

<xsl:template match="address">
	<xsl:apply-templates select="street | house_number | apartment_number | city | post_code"/>
</xsl:template>

<xsl:template match="first_name | last_name | pesel | birth_date | sex | phone_number | email | profession | street | house_number | apartment_number | city | post_code">
	<fo:table-cell>
		<fo:block>
			<xsl:value-of select="."/>
		</fo:block>
	</fo:table-cell>
</xsl:template>

</xsl:stylesheet>
