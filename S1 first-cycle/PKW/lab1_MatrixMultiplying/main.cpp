#include <iostream>
#include <cstdlib>
#include <omp.h>

void multiplySequential(int);

void multiplySequentialWithTransposition(int);

void multiplyParallel(int, int);

void multiplyParallelWithTransposition(int, int);

int main(int argc, char *argv[]) {
    const int matrixSize = atoi(argv[1]);

    printf("%d,", matrixSize);

    multiplySequential(matrixSize);
    printf(",");
    multiplySequentialWithTransposition(matrixSize);

    printf(",");
    multiplyParallel(matrixSize, 1);
    printf(",");
    multiplyParallelWithTransposition(matrixSize, 1);

    printf(",");
    multiplyParallel(matrixSize, 2);
    printf(",");
    multiplyParallelWithTransposition(matrixSize, 2);

    printf(",");
    multiplyParallel(matrixSize, 4);
    printf(",");
    multiplyParallelWithTransposition(matrixSize, 4);
    printf(";\n");
}

void multiplySequential(int size) {
    int **matrixA = new int *[size];
    int **matrixB = new int *[size];
    int **result = new int *[size];
    for (int i = 0; i < size; i++) {
        matrixA[i] = new int[size];
        matrixB[i] = new int[size];
        result[i] = new int[size];
    }

    double start = omp_get_wtime();

    for (int rowA = 0; rowA < size; rowA++) {
        for (int colB = 0; colB < size; colB++) {
            result[rowA][colB] = 0;
            for (int element = 0; element < size; element++) {
                result[rowA][colB] += matrixA[rowA][element] * matrixB[element][colB];
            }
        }
    }

    double duration = omp_get_wtime() - start;
    printf("%f", duration);

    for (int i = 0; i < size; i++) {
        delete[] result[i];
        delete[] matrixA[i];
        delete[] matrixB[i];
    }
    delete[] result;
    delete[] matrixA;
    delete[] matrixB;
}

void multiplySequentialWithTransposition(int size) {
    int **matrixA = new int *[size];
    int **matrixB = new int *[size];
    int **result = new int *[size];
    int **matrixBTransposed = new int *[size];
    for (int i = 0; i < size; i++) {
        matrixA[i] = new int[size];
        matrixB[i] = new int[size];
        result[i] = new int[size];
        matrixBTransposed[i] = new int[size];
    }
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            matrixBTransposed[i][j] = matrixB[j][i];
        }
    }

    double start = omp_get_wtime();

    for (int rowA = 0; rowA < size; rowA++) {
        for (int colB = 0; colB < size; colB++) {
            int cell = 0;
            for (int element = 0; element < size; element++) {
                cell += matrixA[rowA][element] * matrixBTransposed[colB][element];
            }
            result[rowA][colB] = cell;
        }
    }

    double duration = omp_get_wtime() - start;
    printf("%f", duration);

    for (int i = 0; i < size; i++) {
        delete[] result[i];
        delete[] matrixBTransposed[i];
        delete[] matrixA[i];
        delete[] matrixB[i];
    }
    delete[] result;
    delete[] matrixBTransposed;
    delete[] matrixA;
    delete[] matrixB;
}

void multiplyParallel(int size, int threads) {
    int **matrixA = new int *[size];
    int **matrixB = new int *[size];
    int **result = new int *[size];
    for (int i = 0; i < size; i++) {
        matrixA[i] = new int[size];
        matrixB[i] = new int[size];
        result[i] = new int[size];
    }

    double start = omp_get_wtime();

    omp_set_dynamic(0);
    omp_set_num_threads(threads);
#pragma omp parallel for
    for (int rowA = 0; rowA < size; rowA++) {
        for (int colB = 0; colB < size; colB++) {
            int cell = 0;
            for (int element = 0; element < size; element++) {
                cell += matrixA[rowA][element] * matrixB[element][colB];
            }
            result[rowA][colB] = cell;
        }
    }

    double duration = omp_get_wtime() - start;
    printf("%f", duration);

    for (int i = 0; i < size; i++) {
        delete[] result[i];
        delete[] matrixA[i];
        delete[] matrixB[i];
    }
    delete[] result;
    delete[] matrixA;
    delete[] matrixB;
}

void multiplyParallelWithTransposition(int size, int threads) {
    int **matrixA = new int *[size];
    int **matrixB = new int *[size];
    int **result = new int *[size];
    int **matrixBTransposed = new int *[size];
    for (int i = 0; i < size; i++) {
        matrixA[i] = new int[size];
        matrixB[i] = new int[size];
        result[i] = new int[size];
        matrixBTransposed[i] = new int[size];
    }
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            matrixBTransposed[i][j] = matrixB[j][i];
        }
    }

    double start = omp_get_wtime();

    omp_set_dynamic(0);
    omp_set_num_threads(threads);
#pragma omp parallel for
    for (int rowA = 0; rowA < size; rowA++) {
        for (int colB = 0; colB < size; colB++) {
            int cell = 0;
            for (int element = 0; element < size; element++) {
                cell += matrixA[rowA][element] * matrixBTransposed[colB][element];
            }
            result[rowA][colB] = cell;
        }
    }

    double duration = omp_get_wtime() - start;
    printf("%f", duration);

    for (int i = 0; i < size; i++) {
        delete[] result[i];
        delete[] matrixBTransposed[i];
        delete[] matrixA[i];
        delete[] matrixB[i];
    }
    delete[] result;
    delete[] matrixBTransposed;
    delete[] matrixA;
    delete[] matrixB;
}

