#include <iostream>
#include <cmath>
#include <omp.h>

const int pictureWidth = 1600;
const int pictureHeight = 1600;

struct Color {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} colorBlack = {0, 0, 0}, colorWhite = {255, 255, 255};

Color picture[pictureHeight][pictureWidth];

/*
 * Based on https://rosettacode.org/wiki/Mandelbrot_set#PPM_non_interactive
 */
void mandelbrot(const char *filename) {
    /* screen ( integer) coordinate */
    int iX,iY;
    /* world ( double) coordinate = parameter plane*/
    double Cx,Cy;
    const double CxMin=-2.5;
    const double CxMax=1.5;
    const double CyMin=-2.0;
    const double CyMax=2.0;
    /* */
    double PixelWidth=(CxMax-CxMin)/pictureWidth;
    double PixelHeight=(CyMax-CyMin)/pictureHeight;
    /* color component ( R or G or B) is coded from 0 to 255 */
    /* it is 24 bit color RGB file */
    const int MaxColorComponentValue=255;
    FILE * fp;
    const char *comment="# ";/* comment should start with # */
    static unsigned char color[3];
    /* Z=Zx+Zy*i  ;   Z0 = 0 */
    double Zx, Zy;
    double Zx2, Zy2; /* Zx2=Zx*Zx;  Zy2=Zy*Zy  */
    /*  */
    int Iteration;
    const int IterationMax=200;
    /* bail-out value , radius of circle ;  */
    const double EscapeRadius=2;
    double ER2=EscapeRadius*EscapeRadius;
    /*create new file,give it a name and open it in binary mode  */
    fp= fopen(filename,"wb"); /* b -  binary mode */
    /*write ASCII header to the file*/
    fprintf(fp,"P6\n %s\n %d\n %d\n %d\n",comment,pictureWidth,pictureHeight,MaxColorComponentValue);
    /* compute and write image data bytes to the file*/
    for(iY=0;iY<pictureHeight;iY++)
    {
        Cy=CyMin + iY*PixelHeight;
        if (fabs(Cy)< PixelHeight/2) Cy=0.0; /* Main antenna */
        for(iX=0;iX<pictureWidth;iX++)
        {
            Cx=CxMin + iX*PixelWidth;
            /* initial value of orbit = critical point Z= 0 */
            Zx=0.0;
            Zy=0.0;
            Zx2=Zx*Zx;
            Zy2=Zy*Zy;
            /* */
            for (Iteration=0;Iteration<IterationMax && ((Zx2+Zy2)<ER2);Iteration++)
            {
                Zy=2*Zx*Zy + Cy;
                Zx=Zx2-Zy2 +Cx;
                Zx2=Zx*Zx;
                Zy2=Zy*Zy;
            }
            if (Iteration == IterationMax) {
                picture[iY][iX] = colorBlack;
            } else {
                picture[iY][iX] = colorWhite;
            }
        }
    }

    fwrite(picture, sizeof(Color), pictureHeight * pictureWidth, fp);
    fclose(fp);
}

void mandelbrotParallel(const char *filename) {
    const double CxMin = -2.5;
    const double CxMax = 1.5;
    const double CyMin = -2.0;
    const double CyMax = 2.0;

    const double PixelWidth = (CxMax - CxMin) / pictureWidth;
    const double PixelHeight = (CyMax - CyMin) / pictureHeight;

    const int IterationMax = 200;

    const double EscapeRadius = 2;
    const double ER2 = EscapeRadius * EscapeRadius;

    double Cx, Cy;
    int iteration;
    double Zx;
    double Zy;
    double Zx2;
    double Zy2;

    int itAmount[4] = {0, 0, 0, 0};

#pragma omp parallel num_threads(4) default(none) private(Cx, Cy, iteration, Zx, Zy, Zx2, Zy2) shared(CxMin, CyMin, PixelHeight, PixelWidth, ER2, picture, itAmount)
    {
        int threadId = omp_get_thread_num();
        unsigned char colorThreshold = (255 / omp_get_num_threads()) * threadId;
        Color colorOutside = {colorThreshold, colorThreshold, colorThreshold};
        Color colorInside = {0, 128, (unsigned char) (255 - colorThreshold)};

#pragma omp for schedule(dynamic, 100)
        for (int iY = 0; iY < pictureHeight; iY++) {
            Cy = CyMin + iY * PixelHeight;
            if (fabs(Cy) < PixelHeight / 2) Cy = 0.0;
            for (int iX = 0; iX < pictureWidth; iX++) {
                Cx = CxMin + iX * PixelWidth;

                Zx = 0.0;
                Zy = 0.0;
                Zx2 = Zx * Zx;
                Zy2 = Zy * Zy;
                for (iteration = 0; iteration < IterationMax && ((Zx2 + Zy2) < ER2); iteration++) {
                    Zy = 2 * Zx * Zy + Cy;
                    Zx = Zx2 - Zy2 + Cx;
                    Zx2 = Zx * Zx;
                    Zy2 = Zy * Zy;
                }
                itAmount[threadId] += iteration;

                if (iteration == IterationMax) {
                    picture[iY][iX] = colorInside;
                } else {
                    picture[iY][iX] = colorOutside;
                }
            }
        }
    }

    const char *comment = "# ";
    const int MaxColorComponentValue = 255;

    FILE *fp = fopen(filename, "wb");
    fprintf(fp, "P6\n %s\n %d\n %d\n %d\n", comment, pictureWidth, pictureHeight, MaxColorComponentValue);
    fwrite(picture, sizeof(Color), pictureHeight * pictureWidth, fp);
    fclose(fp);

    printf("Iterations per thread: %d,%d,%d,%d\n", itAmount[0], itAmount[1], itAmount[2], itAmount[3]);
}

int main() {
    double startSequential = omp_get_wtime();
    mandelbrot("sequential.ppm");
    double stopSequential = omp_get_wtime();

    double startParallel = omp_get_wtime();
    mandelbrotParallel("parallel.ppm");
    double stopParallel = omp_get_wtime();

    printf("Sequential: %f\n", stopSequential - startSequential);
    printf("  Parallel: %f\n", stopParallel - startParallel);
}
