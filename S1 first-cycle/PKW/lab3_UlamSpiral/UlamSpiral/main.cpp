#include <iostream>
#include <cmath>
#include <omp.h>

struct Color {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} colorBlack = {0, 0, 0};

int ulamGetMap(int col, int row, int dimension) {
    int x = col - (dimension - 1) / 2;
    int y = row - dimension / 2;

    int l = 2 * std::max(abs(x), abs(y));
    int d;
    if (y >= x) {
        d = l * 3 + x + y;
    } else {
        d = l - x - y;
    }

    return (l - 1) * (l - 1) + d;
}

bool isPrime(int number) {
    for (int divisor = 2; divisor * divisor <= number; divisor++) {
        if (number % divisor == 0) {
            return false;
        }
    }
    return number > 2;
}

int main() {
    const int dimension = 4000;
    auto **spiral = new Color *[dimension];
    for (int i = 0; i < dimension; i++) {
        spiral[i] = new Color[dimension];
    }

    const int threads = 4;
    auto **times = new double *[threads];
    for (int i = 0; i < threads; i++) {
        times[i] = new double [threads];
        for (int j = 0; j < threads; j++) {
            times[i][j] = 0.0;
        }
    }

    omp_set_nested(1);
#pragma omp parallel num_threads(threads) default(none) shared(dimension, spiral, colorBlack, times, threads)
    {
        int idx = omp_get_thread_num();
        unsigned char color = (255 / omp_get_num_threads()) * (idx + 1);

#pragma omp for
        for (int row = 0; row < dimension; row++) {

#pragma omp parallel num_threads(threads) default(none) shared(dimension, spiral, row, color, colorBlack, times)
            {
                int idy = omp_get_thread_num();
                unsigned char clr = (255 / omp_get_num_threads()) * (idy + 1);
                Color threadBackgroundColor = {color, clr, clr};

                int ancId = omp_get_ancestor_thread_num(omp_get_level() - 1);

                double start = omp_get_wtime();
#pragma omp for
                for (int col = 0; col < dimension; col++) {
                    int val = ulamGetMap(col, row, dimension);
                    if (isPrime(val)) {
                        spiral[row][col] = colorBlack;
                    } else {
                        spiral[row][col] = threadBackgroundColor;
                    }
                }
                double stop = omp_get_wtime();

                times[ancId][idy] += stop - start;
            }
        }
    }

    for (int i = 0; i < threads; i++) {
        for (int j = 0; j < threads; j++) {
            std::cout << "Time " << i << "-" << j << " : " << times[i][j] << std::endl;
        }
    }

    const char *comment = "# ";
    FILE *fp = fopen("spiral", "wb");
    fprintf(fp, "P6\n %s\n %d\n %d\n %d\n", comment, dimension, dimension, 255);
    for (int i = 0; i < dimension; i++) {
        fwrite(spiral[i], sizeof(Color), dimension, fp);
    }
    fclose(fp);

    for (int i = 0; i < dimension; i++) {
        delete[] spiral[i];
    }
    delete[] spiral;

    for (int i = 0; i < threads; i++) {
        delete[] times[i];
    }
    delete[] times;

    return 0;
}
