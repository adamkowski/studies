#include <iostream>
#include <omp.h>
#include <cassert>
#include <vector>
#include <random>

const int SIZE = 99;

const int W = -1;
const int H = 0;

struct Color {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} black = {0, 0, 0};

omp_lock_t counterLock;
int taskCounter = 1;

omp_lock_t mazeLock[SIZE][SIZE] = {};
int maze[SIZE][SIZE] = {
        {W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
        {W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W},
        {W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W},
        {W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, W},
        {W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W},
        {W, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, W},
        {W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W},
        {W, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, W, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, W, H, W},
        {W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W},
        {W, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W},
        {W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W},
        {W, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, W},
        {W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W},
        {W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, H, H, W},
        {W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W},
        {W, H, W, H, W, H, W, H, H, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, W},
        {W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W},
        {W, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, W},
        {W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W},
        {W, H, W, H, H, H, H, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, W},
        {W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W},
        {W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, H, H, W, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, W},
        {W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W},
        {W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, W},
        {W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W},
        {W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W},
        {W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, W, W, H, W},
        {W, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W},
        {W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W},
        {W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, W},
        {W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W},
        {W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, W, H, W, H, W, H, H, H, H, H, H},
        {W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W},
        {W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, W},
        {W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W},
        {W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, H, H, H, H, W, H, W, H, H, H, W},
        {W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, H, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W},
        {W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, W, H, W, H, W, H, H, H, H, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, W},
        {W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, W, W, H, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, W, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W},
        {W, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, W},
        {W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, H, W},
        {W, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W},
        {W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, H, W, W, W},
        {W, H, H, H, H, H, W, H, W, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, H, H, H, H, H, H, W, H, H, H, W},
        {W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W},
        {W, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, W},
        {W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W},
        {W, H, H, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, W},
        {W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W},
        {W, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, H, H, H, H, W},
        {W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W},
        {W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, W},
        {W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W},
        {W, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W},
        {W, W, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W},
        {W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, H, H, W},
        {W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, W, W},
        {W, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, W},
        {W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W},
        {W, H, W, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, W},
        {W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, H, W},
        {H, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, W},
        {W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, H, W},
        {W, H, H, H, H, H, W, H, W, H, W, H, W, H, W, H, W, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, W},
        {W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W},
        {W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, W},
        {W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W},
        {W, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, W},
        {W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W},
        {W, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, H, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, W},
        {W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W},
        {W, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W},
        {W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W},
        {W, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, H, H, H, H, H, H, W, H, H, H, W, H, W},
        {W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W},
        {W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W},
        {W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W},
        {W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W},
        {W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W},
        {W, H, W, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, W},
        {W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W},
        {W, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W},
        {W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, W, W},
        {W, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, W},
        {W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, W, W, H, W},
        {W, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, W},
        {W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, H, W, H, W, H, W, H, W, W, W},
        {W, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, W, H, H, H, H, H, W},
        {W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W},
        {W, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, H, H, W, H, W, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, W, H, H, H, H, H, H, H, H, H, W},
        {W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, H, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W},
        {W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, W},
        {W, H, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W},
        {W, H, W, H, W, H, H, H, W, H, W, H, W, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, H, H, H, H, H, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, W},
        {W, H, W, H, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, H, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, H, W, W, W, H, W, W, W, W, W, W, W, W, W, H, W, W, W, W, W, W, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W},
        {W, H, W, H, H, H, W, H, W, H, W, H, H, H, H, H, W, H, W, H, W, H, H, H, W, H, H, H, H, H, W, H, W, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, W, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, W, H, H, H, H, H, W},
        {W, H, W, H, W, H, W, H, W, H, W, W, W, W, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, H, W, H, W, W, W, W, W, H, W, W, W, W, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W, H, W, W, W, H, W, W, W, H, W, H, W, H, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, H, W},
        {W, H, H, H, W, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, W, H, H, H, H, H, H, H, W, H, W, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, H, W},
        {W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W, W}
};

void solveMaze(int, int, int);

int main() {
    omp_init_lock(&counterLock);
    for (auto &x : mazeLock) {
        for (auto &y : x) {
            omp_init_lock(&y);
        }
    }

#pragma omp parallel default(none) shared(taskCounter)
    {
#pragma omp master
        {
            solveMaze(1, 1, taskCounter);
        }
    }

    omp_destroy_lock(&counterLock);
    for (auto &x : mazeLock) {
        for (auto &y : x) {
            omp_destroy_lock(&y);
        }
    }

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<unsigned char> dist(0, 255);
    std::vector<unsigned char> randomColors;
    randomColors.reserve(taskCounter);
    for (int i = 0; i < taskCounter; i++) {
        randomColors.push_back(dist(mt));
    }

    auto **picture = new Color *[SIZE];
    for (int i = 0; i < SIZE; i++) {
        picture[i] = new Color[SIZE];
    }

    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            int mazeCell = maze[i][j];
            if (mazeCell == W) {
                picture[i][j] = black;
            } else {
                auto color = (unsigned char) (255.0 / mazeCell);
                if (mazeCell % 3 == 0) {
                    picture[i][j] = {255, randomColors[mazeCell], color};
                } else if (mazeCell % 3 == 1) {
                    picture[i][j] = {color, 255, randomColors[mazeCell]};
                } else {
                    picture[i][j] = {randomColors[mazeCell], color, 255};
                }
            }
        }
    }

    const char *comment = "# ";
    FILE *fp = fopen("maze.ppm", "wb");
    fprintf(fp, "P6\n %s\n %d\n %d\n %d\n", comment, SIZE, SIZE, 255);
    for (int i = 0; i < SIZE; i++) {
        fwrite(picture[i], sizeof(Color), SIZE, fp);
    }
    fclose(fp);

    for (int i = 0; i < SIZE; i++) {
        delete[] picture[i];
    }
    delete[] picture;

    return 0;
}

void solveMaze(int row, int col, int taskId) {
    assert(maze[row][col] == H);

    omp_set_lock(&(mazeLock[row][col]));
    maze[row][col] = taskId;
    omp_unset_lock(&(mazeLock[row][col]));

    bool top = false;
    if (row > 0) {
        omp_set_lock(&(mazeLock[row - 1][col]));
        if (maze[row - 1][col] == H) {
            top = true;
        }
        omp_unset_lock(&(mazeLock[row - 1][col]));
    }

    bool bottom = false;
    if (row < SIZE - 1) {
        omp_set_lock(&(mazeLock[row + 1][col]));
        if (maze[row + 1][col] == H) {
            bottom = true;
        }
        omp_unset_lock(&(mazeLock[row + 1][col]));
    }

    bool left = false;
    if (col > 0) {
        omp_set_lock(&(mazeLock[row][col - 1]));
        if (maze[row][col - 1] == H) {
            left = true;
        }
        omp_unset_lock(&(mazeLock[row][col - 1]));
    }

    bool right = false;
    if (col < SIZE - 1) {
        omp_set_lock(&(mazeLock[row][col + 1]));
        if (maze[row][col + 1] == H) {
            right = true;
        }
        omp_unset_lock(&(mazeLock[row][col + 1]));
    }

    int sum = top + bottom + left + right;
    if (sum == 0) {
        return;
    } else if (sum == 1) {
        if (top) {
            solveMaze(row - 1, col, taskId);
        } else if (bottom) {
            solveMaze(row + 1, col, taskId);
        } else if (left) {
            solveMaze(row, col - 1, taskId);
        } else {
            assert(right == true);
            solveMaze(row, col + 1, taskId);
        }
    } else {
        assert(sum >= 0 && sum <= 4);

        if (top) {
            omp_set_lock(&counterLock);
            taskCounter += 1;
            int newId = taskCounter;
            omp_unset_lock(&counterLock);

#pragma omp task default(none) shared(row, col, newId)
            {
                solveMaze(row - 1, col, newId);
            }
        }

        if (bottom) {
            omp_set_lock(&counterLock);
            taskCounter += 1;
            int newId = taskCounter;
            omp_unset_lock(&counterLock);

#pragma omp task default(none) shared(row, col, newId)
            {
                solveMaze(row + 1, col, newId);
            }
        }

        if (left) {
            omp_set_lock(&counterLock);
            taskCounter += 1;
            int newId = taskCounter;
            omp_unset_lock(&counterLock);

#pragma omp task default(none) shared(row, col, newId)
            {
                solveMaze(row, col - 1, newId);
            }
        }

        if (right) {
            omp_set_lock(&counterLock);
            taskCounter += 1;
            int newId = taskCounter;
            omp_unset_lock(&counterLock);

#pragma omp task default(none) shared(row, col, newId)
            {
                solveMaze(row, col + 1, newId);
            }
        }

#pragma omptask wait

    }
}
