#include <iostream>
#include <omp.h>
#include <cmath>

const int SIZE = 2048;
const int MAX_NESTING = 8;

struct Color {
    unsigned char r;
    unsigned char g;
    unsigned char b;
};

void drawTriangle(int y, int x, int nestingLevel, Color** picture) {
    if (nestingLevel > MAX_NESTING) {
        return;
    }

    unsigned char col = (255 / MAX_NESTING) * nestingLevel;
    Color levelColor = {col, col, col};

    int triangleHeight = SIZE / (int) pow(2, nestingLevel);

    for (int i = y; i < y + triangleHeight; i++) {
        for (int j = x; j < x + i - y; j++) {
            picture[i][j] = levelColor;
        }
    }

#pragma omp task default(none) shared(x, y, nestingLevel, picture)
    drawTriangle(y, x, nestingLevel + 1, picture);

#pragma omp task default(none) shared(x, y, triangleHeight, nestingLevel, picture)
    drawTriangle(y + triangleHeight / 2, x, nestingLevel + 1, picture);

#pragma omp task default(none) shared(x, y, triangleHeight, nestingLevel, picture)
    drawTriangle(y + triangleHeight / 2, x + triangleHeight / 2, nestingLevel + 1, picture);

#pragma omp taskwait
}

int main() {
    auto **picture = new Color *[SIZE];
    for (int i = 0; i < SIZE; i++) {
        picture[i] = new Color[SIZE];
    }

    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            picture[i][j] = {128, 0, 255};
        }
    }

#pragma omp parallel default(none) shared(picture)
    {
#pragma omp single
        drawTriangle(0, 0, 0, picture);
    }

    const char *comment = "# ";
    FILE *fp = fopen("sierpinski.ppm", "wb");
    fprintf(fp, "P6\n %s\n %d\n %d\n %d\n", comment, SIZE, SIZE, 255);
    for (int i = 0; i < SIZE; i++) {
        fwrite(picture[i], sizeof(Color), SIZE, fp);
    }
    fclose(fp);

    for (int i = 0; i < SIZE; i++) {
        delete[] picture[i];
    }
    delete[] picture;

    return 0;
}
