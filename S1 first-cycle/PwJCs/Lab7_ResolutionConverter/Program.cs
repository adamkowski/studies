﻿using System.Drawing;
using System;
using Lab7_ResolutionConverter.ImageUtils;
using System.IO;
using System.Text.RegularExpressions;

namespace Lab7_ResolutionConverter
{
    class Program
    {
        public const string DEFAULT_OUTPUT_DIRECTORY = "\\output";

        private static Size targetResolution;
        private static string inputDirectory = null;
        private static string outputDirectory = null;

        static void Main(string[] args)
        {
            LoadArgs(args);
            ConvertFiles();
        }

        private static void LoadArgs(string[] args)
        {
            if (args.Length > 0 && args.Length <= 3)
            {
                try
                {
                    ReadArguments(args);
                }
                catch (ArgumentException exception)
                {
                    Console.WriteLine(exception.Message);
                    Environment.Exit(1);
                }
            }
            else
            {
                Console.WriteLine("Invalid number of arguments." + Environment.NewLine + 
                    " [ REQUIRED ] -res=WIDTHxHEIGHT" + Environment.NewLine + 
                    " [ OPTIONAL ] -input=PATH -output=PATH");
                Environment.Exit(1);
            }
        }

        private static void ReadArguments(string[] arguments)
        {
            foreach (var arg in arguments)
            {
                if (arg.Contains("="))
                {
                    var begin = arg.Substring(0, arg.IndexOf('='));
                    switch (begin)
                    {
                        case "-res":
                            if (!targetResolution.IsEmpty) 
                                throw new ArgumentException("Duplicated argument: " + begin);
                            targetResolution = GetResolution(arg);
                            break;
                        case "-output":
                            if (outputDirectory != null) 
                                throw new ArgumentException("Duplicated argument: " + begin);
                            outputDirectory = arg.Substring(arg.IndexOf('=') + 1);
                            break;
                        case "-input":
                            if (inputDirectory != null) 
                                throw new ArgumentException("Duplicated argument: " + begin);
                            inputDirectory = arg.Substring(arg.IndexOf('=') + 1);
                            break;
                        default:
                            throw new ArgumentException("Unsuported argument: " + begin);
                    }
                }
                else
                {
                    throw new ArgumentException("Unsuported argument: " + arg);
                }
            }

            if (targetResolution.IsEmpty)
                throw new ArgumentException("Parameter '-res' is obligatory");

            SetDefaultIfArgumentNotSpecified();
        }

        private static void SetDefaultIfArgumentNotSpecified()
        {
            if (inputDirectory == null)
                inputDirectory = Directory.GetCurrentDirectory();

            if (outputDirectory == null)
            {
                outputDirectory = Directory.GetCurrentDirectory() + DEFAULT_OUTPUT_DIRECTORY;
                Directory.CreateDirectory(outputDirectory);
            }
        }

        private static Size GetResolution(string arg)
        {
            var res = arg.Substring(arg.IndexOf('=') + 1);

            var regex = new Regex(@"^([1-9][0-9]*)x([1-9][0-9]*)$");
            var match = regex.Match(res);
            if (!match.Success)
                throw new ArgumentException("Invalid resolution: " + res);

            var imageWidth = int.Parse(match.Groups[1].Value);
            var imageHeight = int.Parse(match.Groups[2].Value);
            return new Size(imageWidth, imageHeight);
        }

        private static void ConvertFiles()
        {
            try
            {
                var filesToConvert = Files.LoadImages(inputDirectory);
                var convertedFiles = new ImageConverter(filesToConvert).Convert(targetResolution);
                Files.SaveImages(convertedFiles, outputDirectory);
            }
            catch (DirectoryNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
