﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Lab7_ResolutionConverter.ImageUtils
{
    static class Files
    {
        public static readonly List<string> IMAGE_FILE_EXTENSIONS = 
            new List<string>{ ".png", ".jpg", ".jpeg" };

        public static List<FileImage> LoadImages(string directoryPath)
        {
            if (!Directory.Exists(directoryPath))
            {
                throw new DirectoryNotFoundException(
                    "Input directory '" + directoryPath + "' does not exist.");
            }

            var images = new List<FileImage>();
            var directoryContent = Directory.GetFiles(directoryPath);
            foreach (string file in directoryContent)
            {
                var extension = Path.GetExtension(file);
                if (IMAGE_FILE_EXTENSIONS.Contains(extension))
                {
                    images.Add(new FileImage(file));
                }
            }
            return images;
        }

        public static void SaveImages(List<FileImage> filesToSave, string outputPath)
        {
            if (!Directory.Exists(outputPath))
            {
                throw new DirectoryNotFoundException(
                    "Output directory '" + outputPath + "' does not exist.");
            }

            var alreadyExistedFiles = GetExistedFileNames(outputPath);

            Console.WriteLine("Converted files:");
            foreach (FileImage file in filesToSave)
            {
                var newFilePath = outputPath + "\\" + 
                    GenerateName(file.GetFileName(), alreadyExistedFiles) + 
                    Path.GetExtension(file.GetFileName());

                var image = file.GetImage();
                image.Save(newFilePath);
                Console.WriteLine("\t" + newFilePath);
            }
        }

        private static List<string> GetExistedFileNames(string directory)
        {
            var alreadyExistedFiles = Directory.GetFiles(directory).ToList();
            var counter = 0;
            while (counter < alreadyExistedFiles.Count)
            {
                alreadyExistedFiles[counter] =
                    Path.GetFileNameWithoutExtension(alreadyExistedFiles[counter]);
                counter++;
            }
            return alreadyExistedFiles;
        }

        private static string GenerateName(string previousName, List<string> existedFiles)
        {
            var nameToSave = Path.GetFileNameWithoutExtension(previousName);
            if (existedFiles.Contains(nameToSave))
            {
                var newName = nameToSave;
                var counter = 1;
                while (existedFiles.Contains(newName))
                {
                    newName = nameToSave + "_" + counter;
                    counter++;
                }
                nameToSave = newName;
            }
            return nameToSave;
        }
    }
}
