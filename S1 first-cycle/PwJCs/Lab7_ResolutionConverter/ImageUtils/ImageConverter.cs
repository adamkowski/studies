﻿using System.Collections.Generic;
using System.Drawing;

namespace Lab7_ResolutionConverter
{
    class ImageConverter
    {
        private readonly List<FileImage> imagesToConvert;

        public ImageConverter(List<FileImage> images) 
        {
            imagesToConvert = images;
        }

        public List<FileImage> Convert(Size targetResolution)
        {
            var convertedImages = new List<FileImage>();
            foreach (FileImage toConvert in imagesToConvert)
            {
                var converted = new Bitmap(toConvert.GetImage(), targetResolution);
                convertedImages.Add(new FileImage(converted, toConvert.GetFileName()));
            }
            return convertedImages;
        }
    }
}
