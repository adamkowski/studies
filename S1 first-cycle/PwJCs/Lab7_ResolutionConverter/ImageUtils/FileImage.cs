﻿using System.Drawing;
using System.IO;

namespace Lab7_ResolutionConverter
{
    class FileImage
    {
        private readonly Image image;
        private readonly string fileName;

        public FileImage(string filePath)
        {
            fileName = Path.GetFileName(filePath);
            image = Image.FromFile(filePath);
        }

        public FileImage(Image image, string fileName)
        {
            this.image = image;
            this.fileName = fileName;
        }

        public Image GetImage()
        {
            return image;
        }

        public string GetFileName()
        {
            return fileName;
        }
    }
}
