﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lab4_BadPaint
{
    class RectangleShape : Shape, IShape
    {
        private Point firstCorner;
        private Point diagonalCorner;

        public RectangleShape(Panel panel, Pen defaultPen) : base(panel, defaultPen) { }

        public void Begin(Point startingPoint)
        {
            SetDrawing(true);

            firstCorner = startingPoint;
            diagonalCorner = startingPoint;
        }

        public void Move(Point currentMouseCoordinates)
        {
            if (IsDrawing())
            {
                diagonalCorner = currentMouseCoordinates;
                GetPanel().Refresh();
            }
        }

        public void Paint(Graphics graphics)
        {
            graphics.DrawRectangle(GetPen(), GetRectangle());
        }

        private Rectangle GetRectangle()
        {
            var rect = new Rectangle
            {
                X = Math.Min(firstCorner.X, diagonalCorner.X),
                Y = Math.Min(firstCorner.Y, diagonalCorner.Y),
                Width = Math.Abs(firstCorner.X - diagonalCorner.X),
                Height = Math.Abs(firstCorner.Y - diagonalCorner.Y)
            };
            return rect;
        }
    }
}
