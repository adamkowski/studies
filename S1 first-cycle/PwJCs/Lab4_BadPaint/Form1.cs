﻿using System;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace Lab4_BadPaint
{
    public partial class Form1 : Form
    {
        private IShape shape = null;

        public Form1()
        {
            InitializeComponent();

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
            | BindingFlags.Instance | BindingFlags.NonPublic, null,
            panel1, new object[] { true });
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            var pen = GetPen();
            try
            {
                shape = GetShape(pen);
                panel2.BackColor = DefaultBackColor;

                shape.Begin(e.Location);
            }
            catch (ArgumentNullException)
            {
                panel2.BackColor = Color.Red;
            }
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (shape != null)
            {
                shape.Move(e.Location);
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            if (shape != null)
            {
                shape.Finish();
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (shape != null)
            {
                shape.Paint(e.Graphics);
            }
        }

        private Pen GetPen()
        {
            var checkedColor = panel3.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            return new Pen(checkedColor.BackColor, 2);
        }

        private IShape GetShape(Pen pen)
        {
            var checkedShape = panel2.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            if (checkedShape == radioButton1)
            {
                return new RectangleShape(panel1, pen);
            }
            else if (checkedShape == radioButton2)
            {
                return new LineShape(panel1, pen);
            }
            else if (checkedShape == radioButton3)
            {
                return new FreeShape(panel1, pen);
            }

            throw new ArgumentNullException();
        }

        private void ClearPanelButton_Click(object sender, EventArgs e)
        {
            shape = null;
            this.panel1.BackgroundImage = null;
        }
    }
}
