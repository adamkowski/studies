﻿using System.Drawing;

namespace Lab4_BadPaint
{
    interface IShape
    {
        void Begin(Point startingPoint);
        void Move(Point currentMouseCoordinates);
        void Paint(Graphics graphics);
        void Finish();
    }
}
