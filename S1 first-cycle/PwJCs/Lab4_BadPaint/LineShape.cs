﻿using System.Drawing;
using System.Windows.Forms;

namespace Lab4_BadPaint
{
    class LineShape : Shape, IShape
    {
        private Point lineEndFirst;
        private Point lineEndSecond;

        public LineShape(Panel panel, Pen defaultPen) : base(panel, defaultPen) { }

        public void Begin(Point startingPoint)
        {
            SetDrawing(true);

            lineEndFirst = startingPoint;
            lineEndSecond = startingPoint;
        }

        public void Move(Point currentMouseCoordinates)
        {
            if (IsDrawing())
            {
                lineEndSecond = currentMouseCoordinates;
                GetPanel().Refresh();
            }
        }

        public void Paint(Graphics graphics)
        {
            graphics.DrawLine(GetPen(), lineEndFirst, lineEndSecond);
        }
    }
}
