﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab3_vsSourcesCopier
{
    public partial class Form1 : Form
    {
        private FolderBrowserDialog folderBrowserDialog;
        private Directory directory;
        private string path;

        public Form1()
        {
            InitializeComponent();

            folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.ShowNewFolderButton = false;

            directory = null;
            path = "";
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {
            ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                directory = null;
                path = "";

                label1.Text = folderBrowserDialog.SelectedPath;
                label2.Text = "";
                label3.Text = "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!label1.Text.Equals(""))
            {
                directory = new Directory(label1.Text);
                path = Directory.CopySources(directory);
                label2.Text = path;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (directory != null && !path.Equals(""))
            {
                string zip = Directory.CreateZip(path);
                label3.Text = zip;
            }
        }
    }
}
