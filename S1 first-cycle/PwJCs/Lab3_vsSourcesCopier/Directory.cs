﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Lab3_vsSourcesCopier
{
    class Directory
    {
        private static readonly string COPY_DIR_NAME = "kopia";
        private static readonly List<string> SOURCES_TO_COPY = new List<string> { "Compile", "EmbeddedResource", "None" };
        private static readonly string SOURCE_FILE_ATTRIBUTE = "Include";

        private readonly string projectPath;
        private readonly string sourcesCopyPath;
        private readonly string slnFilePath;
        private readonly List<string> csprojFiles;
        private readonly List<string> projectFiles;

        public Directory(string path)
        {
            this.projectPath = path;
            this.sourcesCopyPath = path + "\\" + COPY_DIR_NAME;

            this.slnFilePath = FindSlnFile();
            if (this.slnFilePath != null)
            {
                this.csprojFiles = FindCsprojFiles();
            }

            if (this.csprojFiles != null && this.csprojFiles.Any())
            {
                this.projectFiles = FindSourceFiles();
            }
        }

        private string FindSlnFile()
        {
            string[] content = System.IO.Directory.GetFiles(this.projectPath);
            string sln = null;
            foreach (string file in content)
            {
                if ((file.Substring(file.Length - 4)).Equals(".sln"))
                {
                    sln = file.Substring(file.LastIndexOf('\\'));
                    break;
                }
            }
            return sln;
        }

        private List<string> FindCsprojFiles()
        {
            List<string> csproj = new List<string>();

            StreamReader file = new StreamReader(this.projectPath + "\\" + this.slnFilePath);
            string line;
            while ((line = file.ReadLine()) != null)
            {
                string[] lineStrings = line.Split(' ');
                foreach (string word in lineStrings)
                {
                    if (word.Contains(".csproj"))
                    {
                        csproj.Add(word.Substring(1, word.Length - 3));
                    }
                }
            }
            file.Close();

            return csproj;
        }

        private List<string> FindSourceFiles()
        {
            List<string> sources = new List<string>();
            foreach (string file in this.csprojFiles)
            {
                string filePath = this.projectPath + "\\" + file;
                string dir = file.Substring(0, file.LastIndexOf("\\"));

                XmlDocument xml = new XmlDocument();
                xml.LoadXml(File.ReadAllText(filePath));

                foreach (string toInclude in SOURCES_TO_COPY)
                {
                    XmlNodeList nodes = xml.GetElementsByTagName(toInclude);
                    foreach (XmlNode node in nodes)
                    {
                        string name = node.Attributes[SOURCE_FILE_ATTRIBUTE].Value;
                        sources.Add(dir + "\\" + name);
                    }
                }
            }
            return sources;
        }

        public static string CopySources(Directory dir)
        {
            System.IO.Directory.CreateDirectory(dir.sourcesCopyPath);

            if (dir.slnFilePath != null)
                CopyFile(dir.slnFilePath, dir.projectPath);

            if (dir.csprojFiles != null)
                CopyAllFiles(dir.csprojFiles, dir.projectPath);

            if (dir.projectFiles != null)
                CopyAllFiles(dir.projectFiles, dir.projectPath);

            return dir.sourcesCopyPath;
        }

        private static void CopyFile(string filePath, string projectPath)
        {
            if (filePath != null)
            {
                string copySubDirectories = filePath.Substring(0, filePath.LastIndexOf('\\'));
                string copyDestinationPath = projectPath + "\\" + COPY_DIR_NAME + "\\" + copySubDirectories;
                string fileName = filePath.Substring(filePath.LastIndexOf("\\"));
                System.IO.Directory.CreateDirectory(copyDestinationPath);
                File.Copy(projectPath + "\\" + filePath, copyDestinationPath + fileName, true);
            }

        }

        private static void CopyAllFiles(List<string> files, string destination)
        {
            if (files.Any())
            {
                foreach (string file in files)
                {
                    CopyFile(file, destination);
                }
            }
        }

        public static string CreateZip(string directory)
        {
            string copyZipPath = directory + ".zip";
            if (!File.Exists(copyZipPath))
            {
                if (System.IO.Directory.Exists(directory))
                {
                    ZipFile.CreateFromDirectory(directory, copyZipPath);
                }
            }
            return copyZipPath;
        }
    }
}
