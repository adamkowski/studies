﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Lab6_TypingHelper
{
    public partial class Form1 : Form
    {
        private const int HINTS = 10;

        private int minLength = 1;
        private int maxLength = 5;
        
        private readonly List<NameBeginnings> nameBeginnings = new List<NameBeginnings>();

        public Form1()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (updMinLength.Value < updMaxLength.Value)
            {
                minLength = (int) updMinLength.Value;
                maxLength = (int) updMaxLength.Value;

                try
                {
                    var fileDialog = new OpenFileDialog();
                    if (fileDialog.ShowDialog() == DialogResult.OK)
                    {
                        var filePath = fileDialog.FileName;
                        var names = LoadNamesFromFile(filePath);

                        lblSummary.Text += "\nBeginnings:";
                        for (int length = minLength; length <= maxLength; length++)
                        {
                            var nb = new NameBeginnings(names, length);
                            nameBeginnings.Add(nb);
                            lblSummary.Text += string.Format("\n - {0}: {1} ms", length.ToString(), nb.GetElapsedTime());
                        }
                        ChangeElementsVisability();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("The file is incorrect.");
                }
            }
            else
            {
                MessageBox.Show("Min must be lower than max.");
            }
        }

        private List<string> LoadNamesFromFile(string filePath)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var fileLines = File.ReadAllLines(filePath, Encoding.Default);
            var fileContent = new List<string>();
            foreach (string line in fileLines)
            {
                fileContent.Add(line.Substring(line.LastIndexOf(' ') + 1));
            }

            stopWatch.Stop();
            lblSummary.Text = string.Format("File loading: {0} ms", stopWatch.ElapsedMilliseconds);

            return fileContent;
        }

        private void ChangeElementsVisability()
        {
            cmbInput.Visible = lbxNames.Visible = true;
            btnLoad.Visible = false;
            updMinLength.Visible = updMaxLength.Visible = false;
            lblMinLength.Visible = lblMaxLength.Visible = false;
        }

        private void cmbInput_TextUpdate(object sender, EventArgs e)
        {
            cmbInput.DroppedDown = true;
            if (cmbInput.Text.Length <= maxLength)
            {
                cmbInput.Items.Clear();
            }
            cmbInput.SelectionStart = cmbInput.Text.Length;

            string input = cmbInput.Text;
            if (input.Length >= minLength && input.Length - minLength < nameBeginnings.Count)
            {
                var nb = nameBeginnings[input.Length - minLength];
                var beginnings = nb.GetBeginnings();
                if (beginnings.ContainsKey(input))
                {
                    AddItems(beginnings[input]);
                }
            }
        }

        private void AddItems(HashSet<string> names)
        {
            int counter = 0;
            foreach (var n in names)
            {
                cmbInput.Items.Add(n);
                counter++;
                if (counter >= HINTS)
                {
                    break;
                }
            }
        }

        private void cmbInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                lbxNames.Items.Add(cmbInput.Text);
                cmbInput.Text = null;
            }
        }
    }
}
