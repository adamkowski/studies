﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Lab6_TypingHelper
{
    class NameBeginnings
    {
        private readonly Dictionary<string, HashSet<string>> beginnings = new Dictionary<string, HashSet<string>>();
        private readonly long elapsedTime;

        public NameBeginnings(List<string> names, int length)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            foreach (var name in names)
            {
                if (name.Length >= length)
                {
                    string nameBeginning = name.Substring(0, length);
                    if (!beginnings.ContainsKey(nameBeginning))
                    {
                        beginnings[nameBeginning] = new HashSet<string>();
                    }
                    beginnings[nameBeginning].Add(name);
                }
            }

            stopWatch.Stop();
            this.elapsedTime = stopWatch.ElapsedMilliseconds;
        }

        public Dictionary<string, HashSet<string>> GetBeginnings()
        {
            return beginnings;
        }

        public long GetElapsedTime()
        {
            return elapsedTime;
        }
    }
}
