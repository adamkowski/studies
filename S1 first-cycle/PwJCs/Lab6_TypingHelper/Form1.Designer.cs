﻿namespace Lab6_TypingHelper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLoad = new System.Windows.Forms.Button();
            this.lblSummary = new System.Windows.Forms.Label();
            this.cmbInput = new System.Windows.Forms.ComboBox();
            this.lbxNames = new System.Windows.Forms.ListBox();
            this.updMinLength = new System.Windows.Forms.NumericUpDown();
            this.updMaxLength = new System.Windows.Forms.NumericUpDown();
            this.lblMinLength = new System.Windows.Forms.Label();
            this.lblMaxLength = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.updMinLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updMaxLength)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoad
            // 
            this.btnLoad.AutoSize = true;
            this.btnLoad.Location = new System.Drawing.Point(12, 51);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(151, 23);
            this.btnLoad.TabIndex = 0;
            this.btnLoad.Text = "Load names";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // lblSummary
            // 
            this.lblSummary.AutoSize = true;
            this.lblSummary.Location = new System.Drawing.Point(9, 9);
            this.lblSummary.Name = "lblSummary";
            this.lblSummary.Size = new System.Drawing.Size(27, 13);
            this.lblSummary.TabIndex = 1;
            this.lblSummary.Text = "asdf";
            // 
            // cmbInput
            // 
            this.cmbInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbInput.FormattingEnabled = true;
            this.cmbInput.Location = new System.Drawing.Point(233, 12);
            this.cmbInput.Name = "cmbInput";
            this.cmbInput.Size = new System.Drawing.Size(220, 21);
            this.cmbInput.TabIndex = 2;
            this.cmbInput.Visible = false;
            this.cmbInput.TextUpdate += new System.EventHandler(this.cmbInput_TextUpdate);
            this.cmbInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbInput_KeyPress);
            // 
            // lbxNames
            // 
            this.lbxNames.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbxNames.FormattingEnabled = true;
            this.lbxNames.Location = new System.Drawing.Point(233, 39);
            this.lbxNames.Name = "lbxNames";
            this.lbxNames.Size = new System.Drawing.Size(220, 394);
            this.lbxNames.TabIndex = 4;
            this.lbxNames.Visible = false;
            // 
            // updMinLength
            // 
            this.updMinLength.Location = new System.Drawing.Point(12, 25);
            this.updMinLength.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.updMinLength.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.updMinLength.Name = "updMinLength";
            this.updMinLength.Size = new System.Drawing.Size(56, 20);
            this.updMinLength.TabIndex = 5;
            this.updMinLength.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // updMaxLength
            // 
            this.updMaxLength.Location = new System.Drawing.Point(107, 25);
            this.updMaxLength.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.updMaxLength.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.updMaxLength.Name = "updMaxLength";
            this.updMaxLength.Size = new System.Drawing.Size(56, 20);
            this.updMaxLength.TabIndex = 6;
            this.updMaxLength.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblMinLength
            // 
            this.lblMinLength.AutoSize = true;
            this.lblMinLength.Location = new System.Drawing.Point(12, 9);
            this.lblMinLength.Name = "lblMinLength";
            this.lblMinLength.Size = new System.Drawing.Size(56, 13);
            this.lblMinLength.TabIndex = 7;
            this.lblMinLength.Text = "Min length";
            // 
            // lblMaxLength
            // 
            this.lblMaxLength.AutoSize = true;
            this.lblMaxLength.Location = new System.Drawing.Point(104, 9);
            this.lblMaxLength.Name = "lblMaxLength";
            this.lblMaxLength.Size = new System.Drawing.Size(59, 13);
            this.lblMaxLength.TabIndex = 8;
            this.lblMaxLength.Text = "Max length";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 450);
            this.Controls.Add(this.lblMaxLength);
            this.Controls.Add(this.lblMinLength);
            this.Controls.Add(this.updMaxLength);
            this.Controls.Add(this.updMinLength);
            this.Controls.Add(this.lbxNames);
            this.Controls.Add(this.cmbInput);
            this.Controls.Add(this.lblSummary);
            this.Controls.Add(this.btnLoad);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.updMinLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updMaxLength)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label lblSummary;
        private System.Windows.Forms.ComboBox cmbInput;
        private System.Windows.Forms.ListBox lbxNames;
        private System.Windows.Forms.NumericUpDown updMinLength;
        private System.Windows.Forms.NumericUpDown updMaxLength;
        private System.Windows.Forms.Label lblMinLength;
        private System.Windows.Forms.Label lblMaxLength;
    }
}

