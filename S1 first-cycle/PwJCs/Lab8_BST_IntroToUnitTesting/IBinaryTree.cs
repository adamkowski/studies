﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8_BST_IntroToUnitTesting
{
    interface IBinaryTree
    {
        void Insert(int key);
        void Delete(int key);
        Node Find(int key);
    }
}
