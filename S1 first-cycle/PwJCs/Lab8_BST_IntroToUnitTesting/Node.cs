﻿namespace Lab8_BST_IntroToUnitTesting
{
    public class Node
    {
        private int key;
        private int value;
        private Node leftNode;
        private Node rightNode;

        public Node(int key)
        {
            this.key = key;
            leftNode = null;
            rightNode = null;
        }

        public int GetKey()
        {
            return key;
        }

        public Node GetLeftNode()
        {
            return leftNode;
        }

        public Node GetRightNode()
        {
            return rightNode;
        }

        public void SetLeftNode(Node node)
        {
            leftNode = node;
        }

        public void SetRightNode(Node node)
        {
            rightNode = node;
        }
    }
}
