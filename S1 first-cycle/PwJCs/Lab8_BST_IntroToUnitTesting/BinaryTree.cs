﻿using System;

namespace Lab8_BST_IntroToUnitTesting
{
    public class BinaryTree : IBinaryTree
    {
        private Node rootNode = null;
        private int size = 0;

        public BinaryTree() { }

        public int Size()
        {
            return size;
        }

        public void Insert(int key)
        {
            Node newNode = new Node(key);
            Node parent = null;
            Node currentNode = rootNode;

            while (currentNode != null)
            {
                if (currentNode.GetKey() == key)
                    throw new ArgumentException("Duplicated key");

                parent = currentNode;
                if (currentNode.GetKey() > key)
                    currentNode = currentNode.GetLeftNode();
                else
                    currentNode = currentNode.GetRightNode();
            }

            if (rootNode == null)
                rootNode = newNode;
            else if (parent.GetKey() > key)
                parent.SetLeftNode(newNode);
            else
                parent.SetRightNode(newNode);

            this.size += 1;
        }

        public void Delete(int key)
        {
            Node parent = null;
            Node currentNode = rootNode;
            while (currentNode != null && key != currentNode.GetKey())
            {
                parent = currentNode;
                if (currentNode.GetKey() < key)
                    currentNode = currentNode.GetRightNode();
                else
                    currentNode = currentNode.GetLeftNode();
            }

            if (currentNode == null)
                throw new ArgumentException("No such key in the tree");

            if (currentNode.GetRightNode() == null && currentNode.GetLeftNode() == null)
            {
                DeleteLeaf(currentNode, parent);
                return;
            }

            if (currentNode.GetRightNode() == null)
            {
                DeleteNodeWithOnlyLeftSubtree(currentNode, parent);
                return;
            }

            if (currentNode.GetLeftNode() == null)
            {
                DeleteNodeWithOnlyRightSubtree(currentNode, parent);
                return;
            }

            DeleteNodeWithBothSubtrees(currentNode, parent);
        }

        private void DeleteLeaf(Node currentNode, Node parent)
        {
            if (currentNode == rootNode)
            {
                rootNode = null;
                return;
            }

            if (parent.GetRightNode() == currentNode)
                parent.SetRightNode(null);
            else
                parent.SetLeftNode(null);
        }

        private void DeleteNodeWithOnlyLeftSubtree(Node currentNode, Node parent)
        {
            if (currentNode == rootNode)
                rootNode = currentNode.GetLeftNode();
            else if (parent.GetRightNode() == currentNode)
                parent.SetRightNode(currentNode.GetLeftNode());
            else
                parent.SetLeftNode(currentNode.GetLeftNode());
        }

        private void DeleteNodeWithOnlyRightSubtree(Node currentNode, Node parent)
        {
            if (currentNode == rootNode)
                rootNode = currentNode.GetRightNode();
            else if (parent.GetRightNode() == currentNode)
                parent.SetRightNode(currentNode.GetRightNode());
            else
                parent.SetLeftNode(currentNode.GetRightNode());
        }

        private void DeleteNodeWithBothSubtrees(Node currentNode, Node parent)
        {
            Node preparent = currentNode;
            Node child = currentNode.GetLeftNode();
            while (child.GetRightNode() != null)
            {
                preparent = child;
                child = child.GetRightNode();
            }

            if (child == currentNode.GetLeftNode())
            {
                if (currentNode == rootNode)
                {
                    rootNode = currentNode.GetLeftNode();
                    rootNode.SetRightNode(currentNode.GetRightNode());
                }
                else if (parent.GetRightNode() == currentNode)
                    parent.SetRightNode(child);
                else
                    parent.SetLeftNode(child);

                return;
            }

            Node grandchild = child.GetLeftNode();
            if (preparent.GetRightNode() == child)
                preparent.SetRightNode(grandchild);
            else
                preparent.SetLeftNode(grandchild);

            child.SetLeftNode(currentNode.GetLeftNode());

            if (currentNode == rootNode)
            {
                rootNode = child;
                rootNode.SetRightNode(currentNode.GetRightNode());
            }
            else if (parent.GetRightNode() == currentNode)
                parent.SetRightNode(child);
            else
                parent.SetLeftNode(child);
        }

        public Node Find(int key)
        {
            Node currentNode = rootNode;
            while (currentNode != null && key != currentNode.GetKey())
            {
                if (currentNode.GetKey() < key)
                    currentNode = currentNode.GetRightNode();
                else
                    currentNode = currentNode.GetLeftNode();
            }
            return currentNode;
        }
    }
}
