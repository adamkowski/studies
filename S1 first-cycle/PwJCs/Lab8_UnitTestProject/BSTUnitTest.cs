﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab8_BST_IntroToUnitTesting;
using System;

namespace Lab8_UnitTestProject
{
    [TestClass]
    public class BSTUnitTest
    {
        [TestMethod]
        public void AfterCreateTreeHasZeroNodes()
        {
            BinaryTree bst = new BinaryTree();
            Assert.AreEqual(0, bst.Size());
        }

        [TestMethod]
        public void AfterInsertOneElementTreeHasOneNode()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(10);
            Assert.AreEqual(1, bst.Size());
        }

        [TestMethod]
        public void AfterInsertTwoElementsTreeHasTwoNodes()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(10);
            bst.Insert(5);
            Assert.AreEqual(2, bst.Size());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Duplicated key")]
        public void AfterInsertSameKeysExceptionIsThrown()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(10);
            bst.Insert(10);
        }

        [TestMethod]
        public void AfterInsertElementAddedElementIsFound()
        {
            int key = 10;
            BinaryTree bst = new BinaryTree();
            bst.Insert(key);

            Node node = bst.Find(10);
            Assert.IsTrue(node.GetKey() == key);
        }

        [TestMethod]
        public void AfterInsertThreeNodesChildsAreCorrect()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(3);
            bst.Insert(2);
            bst.Insert(1);

            int k1 = bst.Find(3).GetLeftNode().GetKey();
            int k2 = bst.Find(2).GetLeftNode().GetKey();
            Assert.IsTrue(k1 == 2 && k2 == 1);
        }

        [TestMethod]
        public void SearchingNotExistingNodeReturnsNull()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(10);

            Assert.IsTrue(bst.Find(5) == null);
        }

        [TestMethod]
        public void AfterDeleteElementElementIsNotFound()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(10);
            bst.Insert(5);
            bst.Delete(5);
            Assert.IsTrue(bst.Find(5) == null);
        }

        [TestMethod]
        public void AfterDeleteRootNewRootIsSet()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(10);
            bst.Insert(5);
            bst.Delete(10);
            Assert.IsTrue(bst.Find(5) != null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No such key in the tree")]
        public void DeletingNonExistingKeyThrowsException()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(10);
            bst.Delete(5);
        }

        [TestMethod]
        public void AfterDeleteMiddleNodeTreeHasNoGap()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(3);
            bst.Insert(2);
            bst.Insert(1);

            bst.Delete(2);
            Assert.IsTrue(bst.Find(3).GetLeftNode().GetKey() == 1);
        }

        [TestMethod]
        public void DeleteLeaf()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(2);
            bst.Insert(3);
            bst.Insert(1);

            bst.Delete(1);
            bst.Delete(3);
            Assert.IsTrue(bst.Find(1) == null && bst.Find(3) == null);
        }

        [TestMethod]
        public void DeleteNodeWithJustOneSubTree()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(10);
            bst.Insert(5);
            bst.Insert(4);
            bst.Insert(3);

            bst.Delete(5);
            Assert.IsTrue(bst.Find(10).GetLeftNode().GetKey() == 4);
        }

        [TestMethod]
        public void DeleteNodeWithTwoSubTreesAndPredecessorIsAChild()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(9);
            bst.Insert(15);
            bst.Insert(5);

            bst.Delete(9);
            Assert.IsTrue(bst.Find(5).GetRightNode().GetKey() == 15);
        }

        [TestMethod]
        public void DeleteNodeWithTwoSubTreesAndPredecessorIsAGrandchild()
        {
            BinaryTree bst = new BinaryTree();
            bst.Insert(9);
            bst.Insert(15);
            bst.Insert(5);
            bst.Insert(3);
            bst.Insert(7);

            bst.Delete(9);
            Node newRoot = bst.Find(7);
            int leftChildKey = newRoot.GetLeftNode().GetKey();
            int rightChildKey = newRoot.GetRightNode().GetKey();

            Assert.IsTrue(leftChildKey == 5 && rightChildKey == 15);
        }
    }
}
