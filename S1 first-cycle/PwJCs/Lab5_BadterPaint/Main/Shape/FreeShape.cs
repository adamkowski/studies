﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Lab5_BadterPaint
{
    class FreeShape : Shape, IShape
    {
        private readonly List<Point> points = new List<Point>();

        public FreeShape(Panel panel, Pen defaultPen) : base(panel, defaultPen) { }

        public void Begin(Point startingPoint)
        {
            SetDrawing(true);
            points.Add(startingPoint);
        }

        public void Move(Point currentMouseCoordinates)
        {
            if (IsDrawing())
            {
                points.Add(currentMouseCoordinates);
                GetPanel().Refresh();
            }
        }

        public void Paint(Graphics graphics)
        {
            if (points.Count > 1)
            {
                graphics.DrawLines(GetPen(), points.ToArray());
            }
        }
    }
}
