﻿using System.Drawing;
using System.Windows.Forms;

namespace Lab5_BadterPaint
{
    abstract class Shape
    {
        private readonly Panel panel;
        private readonly Pen pen;

        private bool drawing = false;

        public Shape(Panel panel, Pen pen)
        {
            this.panel = panel;
            this.pen = pen;
        }

        public void Finish() 
        {
            drawing = false;

            var bmp = new Bitmap(panel.Width, panel.Height);
            panel.DrawToBitmap(bmp, new Rectangle(0, 0, panel.Width, panel.Height));
            Image image = panel.BackgroundImage;
            panel.BackgroundImage = bmp;
            if (image != null)
            {
                image.Dispose();
            }
        }

        public Panel GetPanel()
        {
            return panel;
        }

        public Pen GetPen()
        {
            return pen;
        }

        public bool IsDrawing()
        {
            return drawing;
        }

        public void SetDrawing(bool drawing)
        {
            this.drawing = drawing;
        }
    }
}
