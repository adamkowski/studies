﻿using System.Drawing;

namespace Lab5_BadterPaint
{
    interface IShape
    {
        void Begin(Point startingPoint);
        void Move(Point currentMouseCoordinates);
        void Paint(Graphics graphics);
        void Finish();
    }
}
