﻿namespace Lab5_BadterPaint
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.radioButton13 = new System.Windows.Forms.RadioButton();
            this.radioButton14 = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButton15 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.DrawingTabs = new System.Windows.Forms.TabControl();
            this.panel4 = new System.Windows.Forms.Panel();
            this.PenSizeDown = new System.Windows.Forms.Button();
            this.PenSizeUp = new System.Windows.Forms.Button();
            this.PenSizeLabel = new System.Windows.Forms.Label();
            this.LoadTabButton = new System.Windows.Forms.Button();
            this.SaveTabButton = new System.Windows.Forms.Button();
            this.AddTabButton = new System.Windows.Forms.Button();
            this.DeleteTabButton = new System.Windows.Forms.Button();
            this.MinimizeAppButton = new System.Windows.Forms.Button();
            this.MaximizeAppButton = new System.Windows.Forms.Button();
            this.CloseAppButton = new System.Windows.Forms.Button();
            this.ClearPanelButton = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioButton10
            // 
            this.radioButton10.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton10.BackColor = System.Drawing.Color.Silver;
            this.radioButton10.FlatAppearance.BorderSize = 0;
            this.radioButton10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton10.Location = new System.Drawing.Point(35, 0);
            this.radioButton10.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(35, 35);
            this.radioButton10.TabIndex = 9;
            this.radioButton10.UseVisualStyleBackColor = false;
            // 
            // radioButton12
            // 
            this.radioButton12.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton12.BackColor = System.Drawing.Color.Black;
            this.radioButton12.Checked = true;
            this.radioButton12.FlatAppearance.BorderSize = 0;
            this.radioButton12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton12.Location = new System.Drawing.Point(315, 0);
            this.radioButton12.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(35, 35);
            this.radioButton12.TabIndex = 13;
            this.radioButton12.TabStop = true;
            this.radioButton12.UseVisualStyleBackColor = false;
            // 
            // radioButton13
            // 
            this.radioButton13.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton13.BackColor = System.Drawing.Color.Blue;
            this.radioButton13.FlatAppearance.BorderSize = 0;
            this.radioButton13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton13.Location = new System.Drawing.Point(245, 0);
            this.radioButton13.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton13.Name = "radioButton13";
            this.radioButton13.Size = new System.Drawing.Size(35, 35);
            this.radioButton13.TabIndex = 12;
            this.radioButton13.UseVisualStyleBackColor = false;
            // 
            // radioButton14
            // 
            this.radioButton14.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton14.BackColor = System.Drawing.Color.Lime;
            this.radioButton14.FlatAppearance.BorderSize = 0;
            this.radioButton14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton14.Location = new System.Drawing.Point(175, 0);
            this.radioButton14.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton14.Name = "radioButton14";
            this.radioButton14.Size = new System.Drawing.Size(35, 35);
            this.radioButton14.TabIndex = 11;
            this.radioButton14.UseVisualStyleBackColor = false;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.radioButton10);
            this.panel3.Controls.Add(this.radioButton12);
            this.panel3.Controls.Add(this.radioButton13);
            this.panel3.Controls.Add(this.radioButton14);
            this.panel3.Controls.Add(this.radioButton15);
            this.panel3.Controls.Add(this.radioButton8);
            this.panel3.Controls.Add(this.radioButton5);
            this.panel3.Controls.Add(this.radioButton4);
            this.panel3.Controls.Add(this.radioButton7);
            this.panel3.Controls.Add(this.radioButton9);
            this.panel3.Location = new System.Drawing.Point(636, 432);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(350, 35);
            this.panel3.TabIndex = 12;
            // 
            // radioButton15
            // 
            this.radioButton15.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.radioButton15.FlatAppearance.BorderSize = 0;
            this.radioButton15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton15.Location = new System.Drawing.Point(105, 0);
            this.radioButton15.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton15.Name = "radioButton15";
            this.radioButton15.Size = new System.Drawing.Size(35, 35);
            this.radioButton15.TabIndex = 10;
            this.radioButton15.UseVisualStyleBackColor = false;
            // 
            // radioButton8
            // 
            this.radioButton8.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton8.BackColor = System.Drawing.Color.White;
            this.radioButton8.FlatAppearance.BorderSize = 0;
            this.radioButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton8.Location = new System.Drawing.Point(0, 0);
            this.radioButton8.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(35, 35);
            this.radioButton8.TabIndex = 3;
            this.radioButton8.UseVisualStyleBackColor = false;
            // 
            // radioButton5
            // 
            this.radioButton5.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton5.BackColor = System.Drawing.Color.Fuchsia;
            this.radioButton5.FlatAppearance.BorderSize = 0;
            this.radioButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton5.Location = new System.Drawing.Point(280, 0);
            this.radioButton5.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(35, 35);
            this.radioButton5.TabIndex = 7;
            this.radioButton5.UseVisualStyleBackColor = false;
            // 
            // radioButton4
            // 
            this.radioButton4.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton4.BackColor = System.Drawing.Color.Aqua;
            this.radioButton4.FlatAppearance.BorderSize = 0;
            this.radioButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton4.Location = new System.Drawing.Point(210, 0);
            this.radioButton4.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(35, 35);
            this.radioButton4.TabIndex = 6;
            this.radioButton4.UseVisualStyleBackColor = false;
            // 
            // radioButton7
            // 
            this.radioButton7.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton7.BackColor = System.Drawing.Color.Yellow;
            this.radioButton7.FlatAppearance.BorderSize = 0;
            this.radioButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton7.Location = new System.Drawing.Point(70, 0);
            this.radioButton7.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(35, 35);
            this.radioButton7.TabIndex = 5;
            this.radioButton7.UseVisualStyleBackColor = false;
            // 
            // radioButton9
            // 
            this.radioButton9.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton9.BackColor = System.Drawing.Color.Red;
            this.radioButton9.FlatAppearance.BorderSize = 0;
            this.radioButton9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton9.Location = new System.Drawing.Point(140, 0);
            this.radioButton9.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(35, 35);
            this.radioButton9.TabIndex = 4;
            this.radioButton9.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Location = new System.Drawing.Point(0, 35);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(986, 397);
            this.panel1.TabIndex = 10;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.Controls.Add(this.radioButton3);
            this.panel2.Controls.Add(this.radioButton2);
            this.panel2.Controls.Add(this.radioButton1);
            this.panel2.Location = new System.Drawing.Point(-3, 432);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 35);
            this.panel2.TabIndex = 11;
            // 
            // radioButton3
            // 
            this.radioButton3.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton3.BackgroundImage = global::Lab5_BadterPaint.Properties.Resources.baseline_create_black_18dp;
            this.radioButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.radioButton3.FlatAppearance.BorderSize = 0;
            this.radioButton3.FlatAppearance.CheckedBackColor = System.Drawing.Color.PaleGreen;
            this.radioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton3.Location = new System.Drawing.Point(0, 0);
            this.radioButton3.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(70, 35);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.Click += new System.EventHandler(this.RestorePanelDefaultColor);
            // 
            // radioButton2
            // 
            this.radioButton2.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton2.BackgroundImage = global::Lab5_BadterPaint.Properties.Resources.baseline_show_chart_black_18dp;
            this.radioButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.radioButton2.FlatAppearance.BorderSize = 0;
            this.radioButton2.FlatAppearance.CheckedBackColor = System.Drawing.Color.PaleGreen;
            this.radioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton2.Location = new System.Drawing.Point(140, 0);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(70, 35);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.Click += new System.EventHandler(this.RestorePanelDefaultColor);
            // 
            // radioButton1
            // 
            this.radioButton1.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton1.BackgroundImage = global::Lab5_BadterPaint.Properties.Resources.baseline_crop_16_9_black_18dp;
            this.radioButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.radioButton1.FlatAppearance.BorderSize = 0;
            this.radioButton1.FlatAppearance.CheckedBackColor = System.Drawing.Color.PaleGreen;
            this.radioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton1.Location = new System.Drawing.Point(70, 0);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(0);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(70, 35);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.Click += new System.EventHandler(this.RestorePanelDefaultColor);
            // 
            // DrawingTabs
            // 
            this.DrawingTabs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DrawingTabs.ItemSize = new System.Drawing.Size(0, 31);
            this.DrawingTabs.Location = new System.Drawing.Point(185, 0);
            this.DrawingTabs.Margin = new System.Windows.Forms.Padding(0);
            this.DrawingTabs.Name = "DrawingTabs";
            this.DrawingTabs.Padding = new System.Drawing.Point(3, 3);
            this.DrawingTabs.SelectedIndex = 0;
            this.DrawingTabs.Size = new System.Drawing.Size(661, 45);
            this.DrawingTabs.TabIndex = 14;
            this.DrawingTabs.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.DrawingTabs_Selecting);
            this.DrawingTabs.Deselecting += new System.Windows.Forms.TabControlCancelEventHandler(this.DrawingTabs_Deselecting);
            // 
            // panel4
            // 
            this.panel4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panel4.Controls.Add(this.PenSizeDown);
            this.panel4.Controls.Add(this.PenSizeUp);
            this.panel4.Controls.Add(this.PenSizeLabel);
            this.panel4.Location = new System.Drawing.Point(280, 432);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(150, 35);
            this.panel4.TabIndex = 25;
            // 
            // PenSizeDown
            // 
            this.PenSizeDown.BackgroundImage = global::Lab5_BadterPaint.Properties.Resources.baseline_expand_more_black_18dp;
            this.PenSizeDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PenSizeDown.FlatAppearance.BorderSize = 0;
            this.PenSizeDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PenSizeDown.Location = new System.Drawing.Point(0, 0);
            this.PenSizeDown.Margin = new System.Windows.Forms.Padding(0);
            this.PenSizeDown.Name = "PenSizeDown";
            this.PenSizeDown.Size = new System.Drawing.Size(50, 35);
            this.PenSizeDown.TabIndex = 27;
            this.PenSizeDown.UseVisualStyleBackColor = true;
            this.PenSizeDown.Click += new System.EventHandler(this.PenSizeDown_Click);
            // 
            // PenSizeUp
            // 
            this.PenSizeUp.BackgroundImage = global::Lab5_BadterPaint.Properties.Resources.baseline_expand_less_black_18dp;
            this.PenSizeUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PenSizeUp.FlatAppearance.BorderSize = 0;
            this.PenSizeUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PenSizeUp.Location = new System.Drawing.Point(100, 0);
            this.PenSizeUp.Margin = new System.Windows.Forms.Padding(0);
            this.PenSizeUp.Name = "PenSizeUp";
            this.PenSizeUp.Size = new System.Drawing.Size(50, 35);
            this.PenSizeUp.TabIndex = 26;
            this.PenSizeUp.UseVisualStyleBackColor = true;
            this.PenSizeUp.Click += new System.EventHandler(this.PenSizeUp_Click);
            // 
            // PenSizeLabel
            // 
            this.PenSizeLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PenSizeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PenSizeLabel.Location = new System.Drawing.Point(50, 0);
            this.PenSizeLabel.Margin = new System.Windows.Forms.Padding(0);
            this.PenSizeLabel.Name = "PenSizeLabel";
            this.PenSizeLabel.Size = new System.Drawing.Size(50, 35);
            this.PenSizeLabel.TabIndex = 24;
            this.PenSizeLabel.Text = "1";
            this.PenSizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LoadTabButton
            // 
            this.LoadTabButton.BackgroundImage = global::Lab5_BadterPaint.Properties.Resources.baseline_get_app_black_18dp;
            this.LoadTabButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.LoadTabButton.FlatAppearance.BorderSize = 0;
            this.LoadTabButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoadTabButton.Location = new System.Drawing.Point(90, 0);
            this.LoadTabButton.Margin = new System.Windows.Forms.Padding(0);
            this.LoadTabButton.Name = "LoadTabButton";
            this.LoadTabButton.Size = new System.Drawing.Size(45, 35);
            this.LoadTabButton.TabIndex = 27;
            this.LoadTabButton.UseVisualStyleBackColor = true;
            this.LoadTabButton.Click += new System.EventHandler(this.LoadTabButton_Click);
            // 
            // SaveTabButton
            // 
            this.SaveTabButton.BackgroundImage = global::Lab5_BadterPaint.Properties.Resources.baseline_save_black_18dp;
            this.SaveTabButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.SaveTabButton.FlatAppearance.BorderSize = 0;
            this.SaveTabButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveTabButton.Location = new System.Drawing.Point(45, 0);
            this.SaveTabButton.Margin = new System.Windows.Forms.Padding(0);
            this.SaveTabButton.Name = "SaveTabButton";
            this.SaveTabButton.Size = new System.Drawing.Size(45, 35);
            this.SaveTabButton.TabIndex = 26;
            this.SaveTabButton.UseVisualStyleBackColor = true;
            this.SaveTabButton.Click += new System.EventHandler(this.SaveTabButton_Click);
            // 
            // AddTabButton
            // 
            this.AddTabButton.BackgroundImage = global::Lab5_BadterPaint.Properties.Resources.baseline_add_black_18dp;
            this.AddTabButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AddTabButton.FlatAppearance.BorderSize = 0;
            this.AddTabButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddTabButton.Location = new System.Drawing.Point(140, 0);
            this.AddTabButton.Margin = new System.Windows.Forms.Padding(0);
            this.AddTabButton.Name = "AddTabButton";
            this.AddTabButton.Size = new System.Drawing.Size(45, 35);
            this.AddTabButton.TabIndex = 21;
            this.AddTabButton.UseVisualStyleBackColor = true;
            this.AddTabButton.Click += new System.EventHandler(this.AddTabButton_Click);
            // 
            // DeleteTabButton
            // 
            this.DeleteTabButton.BackgroundImage = global::Lab5_BadterPaint.Properties.Resources.baseline_delete_forever_black_18dp;
            this.DeleteTabButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.DeleteTabButton.FlatAppearance.BorderSize = 0;
            this.DeleteTabButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteTabButton.Location = new System.Drawing.Point(0, 0);
            this.DeleteTabButton.Margin = new System.Windows.Forms.Padding(0);
            this.DeleteTabButton.Name = "DeleteTabButton";
            this.DeleteTabButton.Size = new System.Drawing.Size(45, 35);
            this.DeleteTabButton.TabIndex = 20;
            this.DeleteTabButton.UseVisualStyleBackColor = true;
            this.DeleteTabButton.Click += new System.EventHandler(this.CloseTabButton_Click);
            // 
            // MinimizeAppButton
            // 
            this.MinimizeAppButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MinimizeAppButton.BackgroundImage = global::Lab5_BadterPaint.Properties.Resources.baseline_minimize_black_18dp;
            this.MinimizeAppButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.MinimizeAppButton.FlatAppearance.BorderSize = 0;
            this.MinimizeAppButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeAppButton.Location = new System.Drawing.Point(846, 0);
            this.MinimizeAppButton.Margin = new System.Windows.Forms.Padding(0);
            this.MinimizeAppButton.Name = "MinimizeAppButton";
            this.MinimizeAppButton.Size = new System.Drawing.Size(45, 35);
            this.MinimizeAppButton.TabIndex = 19;
            this.MinimizeAppButton.UseVisualStyleBackColor = true;
            this.MinimizeAppButton.Click += new System.EventHandler(this.MinimizeAppButton_Click);
            // 
            // MaximizeAppButton
            // 
            this.MaximizeAppButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MaximizeAppButton.BackgroundImage = global::Lab5_BadterPaint.Properties.Resources.baseline_maximize_black_18dp;
            this.MaximizeAppButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.MaximizeAppButton.FlatAppearance.BorderSize = 0;
            this.MaximizeAppButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MaximizeAppButton.Location = new System.Drawing.Point(896, 0);
            this.MaximizeAppButton.Margin = new System.Windows.Forms.Padding(0);
            this.MaximizeAppButton.Name = "MaximizeAppButton";
            this.MaximizeAppButton.Size = new System.Drawing.Size(45, 35);
            this.MaximizeAppButton.TabIndex = 18;
            this.MaximizeAppButton.UseVisualStyleBackColor = true;
            this.MaximizeAppButton.Click += new System.EventHandler(this.MaximizeAppButton_Click);
            // 
            // CloseAppButton
            // 
            this.CloseAppButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseAppButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CloseAppButton.BackgroundImage")));
            this.CloseAppButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CloseAppButton.FlatAppearance.BorderSize = 0;
            this.CloseAppButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.CloseAppButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseAppButton.Location = new System.Drawing.Point(941, 0);
            this.CloseAppButton.Margin = new System.Windows.Forms.Padding(0);
            this.CloseAppButton.Name = "CloseAppButton";
            this.CloseAppButton.Size = new System.Drawing.Size(45, 35);
            this.CloseAppButton.TabIndex = 17;
            this.CloseAppButton.UseVisualStyleBackColor = true;
            this.CloseAppButton.Click += new System.EventHandler(this.CloseAppButton_Click);
            // 
            // ClearPanelButton
            // 
            this.ClearPanelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearPanelButton.BackgroundImage = global::Lab5_BadterPaint.Properties.Resources.baseline_format_color_reset_black_18dp;
            this.ClearPanelButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClearPanelButton.FlatAppearance.BorderSize = 0;
            this.ClearPanelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearPanelButton.Location = new System.Drawing.Point(563, 432);
            this.ClearPanelButton.Name = "ClearPanelButton";
            this.ClearPanelButton.Size = new System.Drawing.Size(70, 35);
            this.ClearPanelButton.TabIndex = 13;
            this.ClearPanelButton.UseVisualStyleBackColor = true;
            this.ClearPanelButton.Click += new System.EventHandler(this.ClearPanelButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 467);
            this.Controls.Add(this.MinimizeAppButton);
            this.Controls.Add(this.LoadTabButton);
            this.Controls.Add(this.SaveTabButton);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.AddTabButton);
            this.Controls.Add(this.DeleteTabButton);
            this.Controls.Add(this.MaximizeAppButton);
            this.Controls.Add(this.CloseAppButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.DrawingTabs);
            this.Controls.Add(this.ClearPanelButton);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.Move += new System.EventHandler(this.Form1_Move);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ClearPanelButton;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.RadioButton radioButton13;
        private System.Windows.Forms.RadioButton radioButton14;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioButton15;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl DrawingTabs;
        private System.Windows.Forms.Button CloseAppButton;
        private System.Windows.Forms.Button MaximizeAppButton;
        private System.Windows.Forms.Button MinimizeAppButton;
        private System.Windows.Forms.Button DeleteTabButton;
        private System.Windows.Forms.Button AddTabButton;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label PenSizeLabel;
        private System.Windows.Forms.Button PenSizeUp;
        private System.Windows.Forms.Button PenSizeDown;
        private System.Windows.Forms.Button SaveTabButton;
        private System.Windows.Forms.Button LoadTabButton;
    }
}

