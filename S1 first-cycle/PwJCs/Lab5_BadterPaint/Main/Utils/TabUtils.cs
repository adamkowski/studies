﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace Lab5_BadterPaint.Main.Util
{
    class TabUtils
    {
        public const string TABS_NAME = "Picture ";
        private static int tabsCounter = 0;

        private TabUtils() { }

        public static int AddTab(TabControl drawingTabs)
        {
            string title = TABS_NAME + (++tabsCounter);
            return AddTab(drawingTabs, title);
        }

        public static int AddTab(TabControl drawingTabs, string title)
        {
            var tabPage = new TabPage(title);
            drawingTabs.TabPages.Add(tabPage);
            return PictureUtils.AddEmptyImage();
        }

        public static void Load(TabControl drawingTabs)
        {
            var fileDialog = new OpenFileDialog();
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                var fileName = CutFileName(fileDialog.FileName);
                var newTabIndex = AddTab(drawingTabs, fileName);

                var drawing = Image.FromFile(fileDialog.FileName);
                PictureUtils.AddImage(drawing);

                drawingTabs.SelectTab(newTabIndex);
            }
        }

        private static string CutFileName(string path)
        {
            var beginName = path.LastIndexOf('\\') + 1;
            var endName = path.LastIndexOf('.') - beginName;
            return path.Substring(beginName, endName);
        }

        public static void Save(Image image)
        {
            var fileDialog = new SaveFileDialog();
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                var fileName = fileDialog.FileName + ".png";
                image.Save(fileName, ImageFormat.Png);
            }
        }
    }
}
