﻿using System.Collections.Generic;
using System.Drawing;

namespace Lab5_BadterPaint
{
    class PictureUtils
    {
        private static readonly List<Image> tabsImages = new List<Image>();

        private PictureUtils() { }

        public static Image GetImage(int index)
        {
            return tabsImages[index];
        }

        public static int AddImage(Image image)
        {
            tabsImages[tabsImages.Count - 1] = image;
            return tabsImages.Count - 1;
        }

        public static int AddEmptyImage()
        {
            tabsImages.Add(null);
            return tabsImages.Count - 1;
        }

        public static void RemoveImage(int index)
        {
            tabsImages.RemoveAt(index);
        }

        public static void SaveImage(int index, Image image)
        {
            tabsImages[index] = image;
        }
    }
}
