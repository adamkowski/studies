﻿using Lab5_BadterPaint.Main.Util;
using System;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Lab5_BadterPaint
{
    public partial class Form1 : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HTCAPTION = 0x2;

        private IShape shape = null;

        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // width of ellipse
            int nHeightEllipse // height of ellipse
        );

        public Form1()
        {
            InitializeComponent();
            Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 5, 5));

            TabUtils.AddTab(DrawingTabs);

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
            | BindingFlags.Instance | BindingFlags.NonPublic, null,
            panel1, new object[] { true });
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            var pen = GetPen();
            try
            {
                shape = GetShape(pen);
                panel2.BackColor = DefaultBackColor;

                shape.Begin(e.Location);
            }
            catch (ArgumentNullException)
            {
                panel2.BackColor = Color.IndianRed;
            }
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (shape != null)
            {
                shape.Move(e.Location);
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            if (shape != null)
            {
                shape.Finish();
                shape = null;
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (shape != null)
            {
                shape.Paint(e.Graphics);
            }
        }

        private Pen GetPen()
        {
            var checkedColor = panel3.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            var penSize = (float) Convert.ToDouble(this.PenSizeLabel.Text);
            return new Pen(checkedColor.BackColor, penSize);
        }

        private IShape GetShape(Pen pen)
        {
            var checkedShape = panel2.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            if (checkedShape == radioButton1)
            {
                return new RectangleShape(panel1, pen);
            }
            else if (checkedShape == radioButton2)
            {
                return new LineShape(panel1, pen);
            }
            else if (checkedShape == radioButton3)
            {
                return new FreeShape(panel1, pen);
            }

            throw new ArgumentNullException();
        }

        private void ClearPanelButton_Click(object sender, EventArgs e)
        {
            this.panel1.BackgroundImage = null;
        }

        public void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HTCAPTION, 0);
            }
        }

        private void AddTabButton_Click(object sender, EventArgs e)
        {
            int newTabIndex = TabUtils.AddTab(DrawingTabs);
            DrawingTabs.SelectTab(newTabIndex);
        }

        private void CloseTabButton_Click(object sender, EventArgs e)
        {
            var tabIndex = DrawingTabs.SelectedIndex;
            PictureUtils.RemoveImage(tabIndex);
            DrawingTabs.TabPages.Remove(DrawingTabs.TabPages[tabIndex]);
                
            if (DrawingTabs.SelectedIndex < 0)
            {
                Application.Exit();
            }
        }

        private void DrawingTabs_Deselecting(object sender, TabControlCancelEventArgs e)
        {
            if (e.TabPageIndex >= 0)
            {
                PictureUtils.SaveImage(e.TabPageIndex, panel1.BackgroundImage);
            }
        }

        private void DrawingTabs_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (e.TabPageIndex >= 0)
            {
                panel1.BackgroundImage = PictureUtils.GetImage(DrawingTabs.SelectedIndex);
            }
        }

        private void CloseAppButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MaximizeAppButton_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void MinimizeAppButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void RestorePanelDefaultColor(object sender, EventArgs e)
        {
            panel2.BackColor = DefaultBackColor;
        }

        private void PenSizeUp_Click(object sender, EventArgs e)
        {
            var size = Convert.ToDouble(this.PenSizeLabel.Text);
            if (size < 5)
            {
                size++;
            }
            this.PenSizeLabel.Text = size.ToString();
        }

        private void PenSizeDown_Click(object sender, EventArgs e)
        {
            var size = Convert.ToDouble(this.PenSizeLabel.Text);
            if (size > 1)
            {
                size--;
            }
            this.PenSizeLabel.Text = size.ToString();
        }

        private void SaveTabButton_Click(object sender, EventArgs e)
        {
            if (panel1.BackgroundImage != null)
            {
                TabUtils.Save(panel1.BackgroundImage);
            }
            else
            {
                MessageBox.Show("The drawing is empty!");
            }
        }

        private void LoadTabButton_Click(object sender, EventArgs e)
        {
            TabUtils.Load(DrawingTabs);
        }

        private void Form1_Move(object sender, EventArgs e)
        {
            Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 5, 5));
        }
    }
}
