﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Table
    {
        private List<List<String>> header;
        private List<List<String>> footer;
        private List<List<String>> rows;

        public Table()
        {
            header = new List<List<String>>();
            footer = new List<List<String>>();
            rows = new List<List<String>>();
        }

        public Table(List<String> header, List<String> footer, List<String> row) : base()
        {
            InsertRow(row);
            InsertHeader(header);
            InsertFooter(footer);
        }

        public void InsertRow(List<String> row)
        {
            this.rows.Add(row);
        }

        public void InsertHeader(List<String> header)
        {
            this.header.Add(header);
        }

        public void InsertFooter(List<String> footer)
        {
            this.footer.Add(footer);
        }

        public String GenerateTable()
        {
            StringBuilder html = new StringBuilder();
            html.Append("<table border='1px'>\n");

            if (this.header.Any())
            {
                html.Append("<thead>\n")
                    .Append(GetRow(this.header, "th"))
                    .Append("</thead>\n");
            }

            html.Append("<tbody>\n")
                .Append(GetRow(this.rows, "td"))
                .Append("</tbody>\n");

            if (this.footer.Any())
            {
                html.Append("<tfoot>\n")
                    .Append(GetRow(this.footer, "td"))
                    .Append("</tfoot>\n");
            }

            html.Append("</table>\n");
            return html.ToString();
        }

        private String GetRow(List<List<String>> tableSection, String tag)
        {
            StringBuilder html = new StringBuilder();
            foreach (List<String> row in tableSection)
            {
                html.Append("<tr>\n");
                foreach (String element in row)
                {
                    html.Append("<").Append(tag).Append(">")
                        .Append(element)
                        .Append("</").Append(tag).Append(">\n");
                }
                html.Append("</tr>\n");
            }
            return html.ToString();
        }
    }
}
