﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class HtmlCode
    {
        private static readonly string INPUT_FILE_NAME = "input.csv";

        private HtmlCode() { }

        public static string GenerateRandomDemo(int number)
        {
            string table = "";
            switch (number)
            {
                case 1:
                    table = Demo1(); break;
                case 2:
                    table = Demo2(); break;
                case 3:
                    table = Demo3(); break;
                case 4:
                    table = Demo4(); break;
            }

            string html = "<!DOCTYPE html>\n<html>\n<head>\n<title>Demo #" + number + "</title>\n</head>\n<body>\n" + table + "</body>\n</html>\n";
            return FormatAsHtml(html);
        }

        private static string FormatAsHtml(string toFormat)
        {
            StringBuilder formatted = new StringBuilder();

            int tabs = 0;

            var scanner = new StringReader(toFormat);
            string line;
            while ((line = scanner.ReadLine()) != null)
            {
                int extraTab = 0;

                if (line.Contains("</") && !((line.Substring(0, 2)).Equals("</")))
                {
                    extraTab++;
                }
                else if (line.Contains("</"))
                {
                    extraTab++;
                    tabs--;
                }
                else if (!line.Contains("<!"))
                {
                    tabs++;
                }

                for (int i = 0; i < tabs - 1 + extraTab; i++)
                {
                    formatted.Append("\t");
                }

                formatted.Append(line).Append("\n");
            }

            return formatted.ToString();
        }

        private static string Demo1()
        {
            var table = new Table();
            table.InsertRow(new List<string> { "3.14", "2.71" });
            table.InsertRow(new List<string> { "0", "1" });
            return table.GenerateTable();
        }

        private static string Demo2()
        {
            var loremIpsum = new List<string>
            {
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam placerat est id mollis imperdiet. Vivamus id risus sed massa fermentum malesuada at nec quam. Phasellus fringilla dignissim lorem vitae dignissim. In facilisis vitae magna quis aliquet. Integer vel pretium leo. Mauris non risus tempor, pharetra libero quis, sodales nibh. Etiam vitae nisl iaculis, efficitur magna ac, fermentum ante. Duis volutpat, dolor eget hendrerit lacinia, risus leo sollicitudin odio, at tempor tortor mauris eu dui. Morbi scelerisque diam ut arcu facilisis pulvinar.",
                "Vestibulum fringilla quam ac suscipit feugiat. Quisque tincidunt vehicula ex, at venenatis ipsum vehicula non. Vivamus molestie magna eu porta dapibus. Mauris at augue ullamcorper, auctor erat vel, sagittis metus. Vestibulum blandit sagittis nisl, ac aliquam lacus suscipit ac. Curabitur eu sapien at elit semper ultrices vel nec nisl. Curabitur nec dignissim est, a hendrerit nulla. Ut ullamcorper velit lorem, et scelerisque lacus dictum id. Aliquam eu ullamcorper ligula. Cras tincidunt odio in erat ullamcorper ullamcorper. Aliquam vel odio ac dolor pulvinar egestas id et nulla. Nulla ac massa ipsum. Curabitur in lacinia ex. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent varius orci magna, in dignissim velit fermentum et.",
                "Pellentesque a lectus fringilla, mollis felis sit amet, tincidunt magna. Sed vel convallis sem. In vitae egestas velit. Fusce nec lectus eu augue venenatis lobortis et nec felis. Etiam maximus eget metus nec commodo. Sed vulputate nulla vel tortor dictum, eu pretium diam iaculis. Duis metus tortor, imperdiet et blandit sit amet, gravida id elit. Quisque ac pellentesque nunc.",
                "Praesent vel ullamcorper massa, id sagittis diam. Proin vitae commodo risus, sit amet dapibus justo. Suspendisse feugiat odio leo, in feugiat magna commodo sit amet. Fusce nec tortor lectus. Nunc lobortis nibh sed enim eleifend, dapibus vestibulum nulla dignissim. Nunc vulputate ligula vel mi vehicula varius. Proin lacinia sem sed massa convallis, quis vehicula nisi finibus. Quisque interdum tempor diam vel sagittis. Phasellus pretium facilisis libero, in blandit magna sollicitudin quis. Ut fringilla mi vel urna faucibus, et ultrices tellus interdum.",
                "Integer vitae nisi efficitur, tincidunt enim eget, sollicitudin massa. Integer erat felis, consectetur nec nisi vitae, laoreet vestibulum velit. Integer blandit metus non risus ultrices mattis. Curabitur eu neque bibendum mauris dignissim dapibus. Vivamus consequat, felis blandit laoreet condimentum, tellus nulla sodales ex, vel hendrerit leo nunc vitae felis. Nunc in ex felis. Nullam ut rhoncus metus. Nam magna justo, condimentum sit amet porta eu, scelerisque a diam. Proin non dapibus nisi. Vivamus imperdiet nec tellus ac eleifend. Nam dapibus justo vel sem aliquet, sed tristique ex convallis. Proin ullamcorper fringilla orci quis sodales. Etiam posuere lorem ut arcu ultrices feugiat. Integer efficitur leo ac purus tristique, et venenatis ipsum varius. Donec at eros tempor, finibus lorem eu, lacinia nulla. Vivamus feugiat hendrerit purus ut auctor.",
                "Cras erat augue, venenatis eget nulla a, sagittis maximus nunc. Ut tempor pharetra enim ac cursus. Pellentesque luctus venenatis dignissim. Quisque non massa dapibus, rhoncus nisi vitae, pretium lectus. Ut blandit egestas mi, sit amet tristique neque malesuada non. Etiam viverra eleifend arcu, sed sagittis sem pellentesque non. Duis aliquet magna a dolor varius vestibulum. In eu commodo neque, sit amet tincidunt urna. Maecenas nec maximus nulla. Praesent et ultrices erat, non fermentum risus. Mauris vehicula id lectus nec porta. Vivamus in congue felis, quis pharetra massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent dapibus, sem ut suscipit mollis, mi nulla finibus nunc, non convallis ante risus non quam. Curabitur interdum nulla sed semper finibus."
            };

            var table = new Table();
            table.InsertRow(new List<string> { loremIpsum.ElementAt(0), loremIpsum.ElementAt(1) });
            table.InsertRow(new List<string> { loremIpsum.ElementAt(2), loremIpsum.ElementAt(3) });
            table.InsertRow(new List<string> { loremIpsum.ElementAt(4), loremIpsum.ElementAt(5) });

            return table.GenerateTable();
        }

        private static string Demo3()
        {
            var fileContent = new List<List<string>>();
            foreach (string line in File.ReadAllLines(INPUT_FILE_NAME).ToList())
            {
                fileContent.Add(line.Remove(line.Length - 1).Split(',').ToList());
            }

            var table = new Table();
            foreach (List<string> row in fileContent)
            {
                table.InsertRow(row);
            }

            return table.GenerateTable();
        }

        private static string Demo4()
        {
            var table = new Table();
            table.InsertHeader(new List<string> { "Autor", "Tytuł" });
            table.InsertRow(new List<string> { "Raymond Chandler", "Gleboki sen" });
            table.InsertRow(new List<string> { "Thomas Harris", "Milczenie owiec" });
            table.InsertRow(new List<string> { "Mario Puzo", "Ojciec chrzestny" });
            table.InsertRow(new List<string> { "Robert Ludlum", "Tozsamosc Bournea" });

            return table.GenerateTable();
        }
    }
}
