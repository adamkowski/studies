﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Server
    {
        private readonly string address = "127.0.0.1";
        private readonly int port;
        private readonly string path;

        private TcpListener server = null;
        private bool running = false;

        public Server(int port, string path)
        {
            this.port = port;
            this.path = path;
        }

        public void Run()
        {
            try
            {
                server = new TcpListener(IPAddress.Parse(address), port);
                server.Start();
                running = true;

                while (running)
                {
                    TcpClient client = server.AcceptTcpClient();
                    NetworkStream stream = client.GetStream();

                    byte[] data = new byte[client.ReceiveBufferSize];
                    int bytesRead = stream.Read(data, 0, Convert.ToInt32(client.ReceiveBufferSize));
                    string[] request = (Encoding.ASCII.GetString(data, 0, bytesRead)).Split(' ');

                    string filePath = "";
                    if (request[0].Equals("GET"))
                    {
                        filePath = request[1].Remove(0, 1);
                    }

                    var message = CreateMessage(filePath);
                    byte[] msg = Encoding.ASCII.GetBytes(message);
                    stream.Write(msg, 0, msg.Length);

                    client.Close();
                }
            }
            catch (SocketException ex)
            {
                Console.WriteLine("SocketException: {0}", ex);
            }
            finally
            {
                server.Stop();
                running = false;
            }
        }

        public void Kill()
        {
            if (server != null)
            {
                server.Stop();
            }
            running = false;
        }

        private string CreateMessage(string filePath)
        {
            var message = new StringBuilder("HTTP/1.1 ");

            StreamReader file = null;
            try
            {
                if (filePath.Equals(""))
                    file = new StreamReader(path + "/index.html");
                else
                    file = new StreamReader(path + "/" + filePath);

                message.Append("200\n\n");

                string line;
                while ((line = file.ReadLine()) != null)
                {
                    message.Append(line);
                }
            }
            catch (FileNotFoundException e)
            {
                message.Append("404\n\n");
                message.Append("<html><body>Error 404: File not found</body></html>");
            }
            catch (Exception e)
            {
                message.Append("500\n\n");
                message.Append("<html><body>Error 500: Exception</body></html>");
            }

            return message.ToString();
        }

    }
}
