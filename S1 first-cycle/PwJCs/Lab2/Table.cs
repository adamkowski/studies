﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Table
    {
        private List<List<string>> header;
        private List<List<string>> footer;
        private List<List<string>> rows;

        public Table()
        {
            header = new List<List<string>>();
            footer = new List<List<string>>();
            rows = new List<List<string>>();
        }

        public Table(List<string> header, List<string> footer, List<string> row) : base()
        {
            InsertRow(row);
            InsertHeader(header);
            InsertFooter(footer);
        }

        public void InsertRow(List<string> row)
        {
            this.rows.Add(row);
        }

        public void InsertHeader(List<string> header)
        {
            this.header.Add(header);
        }

        public void InsertFooter(List<string> footer)
        {
            this.footer.Add(footer);
        }

        public string GenerateTable()
        {
            StringBuilder html = new StringBuilder();
            html.Append("<table border='1px'>\n");

            if (this.header.Any())
            {
                html.Append("<thead>\n")
                    .Append(GetRow(this.header, "th"))
                    .Append("</thead>\n");
            }

            html.Append("<tbody>\n")
                .Append(GetRow(this.rows, "td"))
                .Append("</tbody>\n");

            if (this.footer.Any())
            {
                html.Append("<tfoot>\n")
                    .Append(GetRow(this.footer, "td"))
                    .Append("</tfoot>\n");
            }

            html.Append("</table>\n");
            return html.ToString();
        }

        private string GetRow(List<List<string>> tableSection, string tag)
        {
            StringBuilder html = new StringBuilder();
            foreach (List<string> row in tableSection)
            {
                html.Append("<tr>\n");
                foreach (string element in row)
                {
                    html.Append("<").Append(tag).Append(">")
                        .Append(element)
                        .Append("</").Append(tag).Append(">\n");
                }
                html.Append("</tr>\n");
            }
            return html.ToString();
        }
    }
}
