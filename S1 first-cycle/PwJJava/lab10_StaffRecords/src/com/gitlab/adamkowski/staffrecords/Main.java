package com.gitlab.adamkowski.staffrecords;

import com.gitlab.adamkowski.staffrecords.controller.Controller;

public class Main {

    public static void main(String[] args) {
        new Controller();
    }
}
