package com.gitlab.adamkowski.staffrecords.employees;

import java.io.Serializable;

public class Trader extends Employee implements Serializable {
    public static final String POSITION_TRADER = "Handlowiec";

    private int commission;
    private int commissionLimit;

    public Trader() {
        super(POSITION_TRADER);
    }

    public void setCommission(int commission) {
        if (commission < 0)
            throw new IllegalArgumentException("Commission cannot be negative");

        this.commission = commission;
    }

    public void setCommissionLimit(int commissionLimit) {
        if (commission < 0)
            throw new IllegalArgumentException("Commission monthly limit cannot be negative");

        this.commissionLimit = commissionLimit;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Prowizja\t\t\t\t\t:\t" + commission + System.lineSeparator() +
                "Limit prowizji / miesiąc\t:\t" + commissionLimit + System.lineSeparator();
    }
}
