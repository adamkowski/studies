package com.gitlab.adamkowski.staffrecords.employees;

import java.io.Serializable;

public class Director extends Employee implements Serializable {
    public static final String POSITION_DIRECTOR = "Dyrektor";

    private int salarySupplement;
    private String bankCard;
    private int monthlyCostLimit;

    public Director() {
        super(POSITION_DIRECTOR);
    }

    public void setSalarySupplement(int salarySupplement) {
        if (salarySupplement < 0)
            throw new IllegalArgumentException("Supplement cannot be negative");

        this.salarySupplement = salarySupplement;
    }

    public void setBankCard(String bankCard) {
        this.bankCard = bankCard;
    }

    public void setMonthlyCostLimit(int monthlyCostLimit) {
        if (salarySupplement < 0)
            throw new IllegalArgumentException("Monthly limit cannot be negative");

        this.monthlyCostLimit = monthlyCostLimit;
    }

    @Override
    public String toString() {
        return super.toString() +
               "Dodatek służbowy\t\t\t:\t" + salarySupplement + System.lineSeparator() +
               "Karta służbowa numer\t\t:\t" + bankCard + System.lineSeparator() +
               "Limit kosztów / miesiąc\t\t:\t" + monthlyCostLimit + System.lineSeparator();
    }
}
