package com.gitlab.adamkowski.staffrecords.controller;

import com.gitlab.adamkowski.staffrecords.employees.Employee;
import com.gitlab.adamkowski.staffrecords.menu.Menu;

import java.io.*;
import java.util.HashMap;
import java.util.zip.*;

public class FileUtils {
    public static final String EXT_GZIP = ".gz";
    public static final String EXT_ZIP = ".zip";

    public static void saveToFile(HashMap<String, Employee> employees, String comp, String name) {
        if (comp.equals("G"))
            saveToGzip(employees, name + EXT_GZIP);
        else if (comp.equals("Z"))
            saveToZip(employees, name + EXT_ZIP);
    }

    private static void saveToGzip(HashMap<String, Employee> employees, String name) {
        try (GZIPOutputStream gzip = new GZIPOutputStream(new FileOutputStream(name))) {
            serializeEmployees(employees, gzip);
        }
        catch (IOException e) {
            System.out.println("Writing to file failed");
        }
    }

    private static void saveToZip(HashMap<String, Employee> employees, String name) {
        try (FileOutputStream fos = new FileOutputStream(name);
             ZipOutputStream zip = new ZipOutputStream(fos)
        ) {
            zip.putNextEntry(new ZipEntry("employees"));
            serializeEmployees(employees, zip);
        }
        catch (IOException e) {
            System.out.println("Writing to file failed");
        }
    }

    private static void serializeEmployees(HashMap<String, Employee> employees,
                                           DeflaterOutputStream archiveStream) throws IOException {

        try (ObjectOutputStream output = new ObjectOutputStream(archiveStream)) {
            for (String user : employees.keySet())
                output.writeObject(employees.get(user));
        }
    }

    public static HashMap<String, Employee> readFromFile(String fileName) {
        final String compression = fileName.substring(fileName.lastIndexOf("."));

        HashMap<String, Employee> employees = null;
        if (compression.equals(EXT_GZIP))
            employees = loadGzip(fileName);
        else if (compression.equals(EXT_ZIP))
            employees = loadZip(fileName);

        return employees;
    }

    private static HashMap<String, Employee> loadGzip(String fileName) {
        HashMap<String, Employee> employees = new HashMap<>();

        try (GZIPInputStream gzip = new GZIPInputStream(new FileInputStream(fileName));
             ObjectInputStream inputStream = new ObjectInputStream(gzip)
        ) {
            employees = deserializeEmployees(inputStream);
        }
        catch (IOException ignore) {
        }

        return employees;
    }

    private static HashMap<String, Employee> loadZip(String fileName) {
        HashMap<String, Employee> employees = new HashMap<>();

        try (FileInputStream fis = new FileInputStream(fileName);
             ZipInputStream zis = new ZipInputStream(fis)
        ) {
            zis.getNextEntry();
            try (ObjectInputStream inputStream = new ObjectInputStream(zis)) {
                employees = deserializeEmployees(inputStream);
            }
        }
        catch (IOException ignore) {
        }

        return employees;
    }

    private static HashMap<String, Employee> deserializeEmployees(ObjectInputStream inputStream) {
        HashMap<String, Employee> employees = new HashMap<>();

        try {
            Object readObject = inputStream.readObject();
            while (readObject != null) {
                Employee e = (Employee) readObject;
                employees.put(e.getPesel(), e);
                readObject = inputStream.readObject();
            }
        }
        catch (IOException | ClassNotFoundException ignored) {
        }

        return employees;
    }
}
