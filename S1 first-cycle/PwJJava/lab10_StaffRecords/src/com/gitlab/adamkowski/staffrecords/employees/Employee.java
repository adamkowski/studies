package com.gitlab.adamkowski.staffrecords.employees;

import java.io.Serializable;

public abstract class Employee implements Serializable {
    private String pesel;
    private String firstName;
    private String lastName;
    private final String position;
    private int salary;
    private String phone;

    public Employee(String position) {
        this.position = position;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        if (pesel.length() != 11)
            throw new IllegalArgumentException("Improper length of pesel number");

        if (!pesel.matches("[0-9]{11}"))
            throw new IllegalArgumentException("Pesel has to contain only numbers");

        this.pesel = pesel;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setSalary(int salary) {
        if (salary < 0)
            throw new IllegalArgumentException("Salary cannot be negative");

        this.salary = salary;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Identyfikator PESEL\t\t\t:\t" + pesel + System.lineSeparator() +
               "Imię\t\t\t\t\t\t:\t" + firstName + System.lineSeparator() +
               "Nazwisko\t\t\t\t\t:\t" + lastName + System.lineSeparator() +
               "Stanowisko\t\t\t\t\t:\t" + position + System.lineSeparator() +
               "Wynagrodzenie\t\t\t\t:\t" + salary + System.lineSeparator() +
               "Telefon służbowy numer\t\t:\t" + phone + System.lineSeparator();
    }
}
