package com.gitlab.adamkowski.staffrecords.controller;

import com.gitlab.adamkowski.staffrecords.employees.Director;
import com.gitlab.adamkowski.staffrecords.employees.Employee;
import com.gitlab.adamkowski.staffrecords.employees.Trader;
import com.gitlab.adamkowski.staffrecords.menu.Menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Controller {
    public static final String QUIT_SYMBOL = "Q";

    private final Menu menu = new Menu();
    private HashMap<String, Employee> employees = new HashMap<>();

    private final Scanner input = new Scanner(System.in);

    public Controller() {
        run();
    }

    private void run() {
        String choice = "";
        while (!choice.equals(QUIT_SYMBOL)) {
            menu.show();

            choice = input.nextLine().toUpperCase();
            switch (choice) {
                case "1":
                    listEmployees();
                    break;
                case "2":
                    addEmployee();
                    break;
                case "3":
                    deleteEmployee();
                    break;
                case "4":
                    String operation = Menu.askWithResponse("[Z]achowaj / [O]dtwórz").toUpperCase();
                    if (operation.equals("Z")) {
                        saveData();
                    }
                    else if (operation.equals("O")) {
                        loadData();
                    }
                    break;
            }
        }
    }

    private void listEmployees() {
        ArrayList<String> pesels = new ArrayList<>(employees.keySet());
        if (pesels.size() > 0)
            showEmployees(pesels);
    }

    private void showEmployees(ArrayList<String> pesels) {
        String next = "";
        int index = 0;
        while (!next.equals(QUIT_SYMBOL)) {
            if (index > pesels.size() - 1) {
                index = 0;
            }
            menu.printEmployee(employees.get(pesels.get(index)), ++index, employees.size());
            next = input.nextLine().toUpperCase();
        }
    }

    private void addEmployee() {
        try {
            Employee e = insertEmployee();
            if (!employees.containsKey(e.getPesel())) {
                employees.put(e.getPesel(), e);
            }
        }
        catch (ClassNotFoundException | IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    private Employee insertEmployee() throws ClassNotFoundException, IllegalArgumentException {
        String choice = Menu.askWithResponse("[D]yrektor / [H]andlowiec").toUpperCase();

        Employee employee;
        if (choice.equals("D")) {
            employee = new Director();
        }
        else if (choice.equals("H")) {
            employee = new Trader();
        }
        else {
            throw new ClassNotFoundException("Invalid employee type");
        }

        String pesel = Menu.askWithResponse("Identyfikator PESEL");
        if (!employees.containsKey(pesel)) {
            employee.setPesel(pesel);
        }
        else {
            throw new IllegalArgumentException("This pesel already exists");
        }

        String value;

        value = Menu.askWithResponse("Imię");
        employee.setFirstName(value);

        value = Menu.askWithResponse("Nazwisko");
        employee.setLastName(value);

        value = Menu.askWithResponse("Wynagrodzenie");
        employee.setSalary(Integer.parseInt(value));

        value = Menu.askWithResponse("Telefon służbowy numer");
        employee.setPhone(value);

        if (choice.equals("D")) {
            value = Menu.askWithResponse("Dodatek służbowy");
            ((Director) employee).setSalarySupplement(Integer.parseInt(value));

            value = Menu.askWithResponse("Karta służbowa numer");
            ((Director) employee).setBankCard(value);

            value = Menu.askWithResponse("Limit kosztów / miesiąc");
            ((Director) employee).setMonthlyCostLimit(Integer.parseInt(value));
        }
        else if (choice.equals("H")) {
            value = Menu.askWithResponse("Prowizja");
            ((Trader) employee).setCommission(Integer.parseInt(value));

            value = Menu.askWithResponse("Limit prowizji / miesiąc");
            ((Trader) employee).setCommissionLimit(Integer.parseInt(value));
        }

        return employee;
    }

    private void deleteEmployee() {
        String pesel = Menu.askWithResponse("Podaj identyfiktor PESEL");
        if (!employees.containsKey(pesel)) {
            Menu.showMessage("Nie znaleziono");
        }
        else {
            menu.showDeleteView(employees.get(pesel));
        }

        String response = input.nextLine().toUpperCase();
        if (!response.equals(QUIT_SYMBOL)) {
            employees.remove(pesel);
        }
    }

    private void saveData() {
        String compression = Menu.askWithResponse("Kompresja [G]zip / [Z]ip").toUpperCase();
        String fileName = Menu.askWithResponse("Nawa pliku");
        FileUtils.saveToFile(employees, compression, fileName);
    }

    private void loadData() {
        String fileName = Menu.askWithResponse("Nawa pliku");
        employees = FileUtils.readFromFile(fileName);
    }
}
