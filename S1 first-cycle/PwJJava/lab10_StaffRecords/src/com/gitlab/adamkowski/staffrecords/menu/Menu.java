package com.gitlab.adamkowski.staffrecords.menu;

import com.gitlab.adamkowski.staffrecords.employees.Employee;

import java.util.Scanner;

public class Menu {
    public Menu() { }

    public void show() {
        System.out.print(
                System.lineSeparator() + "MENU" + System.lineSeparator() +
                "\t1. Lista pracowników" + System.lineSeparator() +
                "\t2. Dodaj pracownika" + System.lineSeparator() +
                "\t3. Usuń pracownika" + System.lineSeparator() +
                "\t4. Kopia zapasowa" + System.lineSeparator() +
                System.lineSeparator() + "Wybór> ");
    }

    public void printEmployee(Employee employee, int index, int total) {
        System.out.print("1. Lista pracowników" + System.lineSeparator() + System.lineSeparator() +
                employee + System.lineSeparator() +
                "\t\t\t\t\t\t\t\t[Pozycja: " + index + "/" + total + "]" + System.lineSeparator() +
                "[Enter] - następny" + System.lineSeparator() +
                "[Q] - powrót" + System.lineSeparator());
    }

    public void showDeleteView(Employee employee) {
        System.out.print("3. Usuń pracownika" + System.lineSeparator() + System.lineSeparator() +
                "Podaj identyfiktor PESEL\t:\t" + employee.getPesel() + System.lineSeparator() +
                "------------------------------------------------------" + System.lineSeparator() +
                employee +
                "------------------------------------------------------" + System.lineSeparator() +
                "[Enter] - potwierdź" + System.lineSeparator() +
                "[Q] - porzuć" + System.lineSeparator());
    }

    public static void showMessage(String content) {
        System.out.println(content);
    }

    public static String askWithResponse(String message) {
        StringBuilder builder = new StringBuilder(message);
        while (builder.length() < 30) {
            builder.append(" ");
        }
        builder.append(":   ");
        System.out.print(builder.toString());

        return new Scanner(System.in).nextLine();
    }
}
