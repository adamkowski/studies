package com.gitlab.adamkowski.staffrecords.controller;

import com.gitlab.adamkowski.staffrecords.employees.Director;
import com.gitlab.adamkowski.staffrecords.employees.Employee;
import com.gitlab.adamkowski.staffrecords.employees.Trader;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FileUtilsTest {
    @Test
    void afterSaveToFileFileExists() {
        HashMap<String, Employee> employees = new HashMap<>();
        Employee e = new Director();
        employees.put("12345678910", e);
        FileUtils.saveToFile(employees, "G", "test_file");
        File f = new File("test_file.gz");
        assertTrue(f.exists());
        f.delete();
    }

    @Test
    void afterReadFileProperContentIsDeserialize() {
        Employee trader = new Trader();
        trader.setPesel("00000000000");
        HashMap<String, Employee> employees = new HashMap<>();
        employees.put("00000000000", trader);
        FileUtils.saveToFile(employees, "G", "tmp_file");

        HashMap<String, Employee> loaded = FileUtils.readFromFile("tmp_file.gz");
        String pesel = loaded.get("00000000000").getPesel();
        new File("tmp_file.gz").delete();

        assertEquals(pesel, trader.getPesel());
    }
}
