package com.gitlab.adamkowski.staffrecords.employees;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    @Test
    void afterAddEmployeeProperPositionIsSet() {
        Employee boss = new Director();
        Employee trader = new Trader();
        assertEquals(boss.getPosition(), "Dyrektor");
        assertEquals(trader.getPosition(), "Handlowiec");
    }

    @Test
    void improperLengthPeselThrowsException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee e = new Director();
            e.setPesel("1234567890");
        });

        assertEquals(exception.getMessage(), "Improper length of pesel number");
    }

    @Test
    void invalidCharInPeselThrowsException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee e = new Director();
            e.setPesel("1234567890x");
        });

        assertEquals(exception.getMessage(), "Pesel has to contain only numbers");
    }

    @Test
    void negativeSalaryValueThrowsException() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            Employee e = new Trader();
            e.setSalary(-1200);
        });

        assertEquals(exception.getMessage(), "Salary cannot be negative");
    }
}
