import logging
import os
import random
import urllib.request
from collections import Counter
from os.path import exists, isdir

import matplotlib.pyplot as plt
import networkx as nx


def main():
    logging.basicConfig(level=logging.INFO)

    data_set_path = find_data_set()
    net = load_network(data_set_path)
    graph = remove_loops_and_duplicated_edges(net)
    graph = find_largest_component(graph)
    approx_avg_path_length(graph)
    find_high_ranked_cores(graph)


def find_data_set():
    student_index = 323573
    network_id = student_index % 6
    logging.info(f'Wybrana sieć: {network_id}')
    path = f'./data/{network_id}.txt'
    if not exists(path):
        download_data_set(path)
    return path


def download_data_set(path: str):
    if not isdir('./data'):
        os.makedirs('./data')
    urllib.request.urlretrieve('https://www.ia.pw.edu.pl/~mkamola/dataset-big/5.txt', path)
    logging.debug(f'Downloaded file: {path}')


def load_network(data_set_path: str) -> nx.Graph:
    net: nx.Graph = nx.read_edgelist(data_set_path, create_using=nx.MultiGraph)
    logging.info(f'Sieć pierwotna - rząd: {len(net.nodes)}')
    logging.info(f'Sieć pierwotna - rozmiar: {len(net.edges)}')
    return net


def remove_loops_and_duplicated_edges(net: nx.Graph) -> nx.Graph:
    graph = nx.Graph(net)
    graph.remove_edges_from(nx.selfloop_edges(graph))
    logging.info(f'Sieć przetworzona - rząd: {len(graph.nodes)}')
    logging.info(f'Sieć przetworzona - rozmiar: {len(graph.edges)}')
    return graph


def find_largest_component(graph: nx.Graph):
    connected_components = sorted(nx.connected_components(graph), key=len)
    largest_component = graph.subgraph(connected_components[-1])
    logging.info(f'Największa składowa spójna - rząd: {len(largest_component.nodes)}')
    logging.info(f'Największa składowa spójna - rozmiar: {len(largest_component.edges)}')
    return largest_component


def approx_avg_path_length(graph: nx.Graph):
    rank = len(graph.nodes)
    nodes_list = list(graph.nodes)
    for limit in (100, 1_000, 10_000):
        avg = 0
        for number in range(limit):
            random_src = nodes_list[random.randint(0, rank - 1)]
            random_tgt = nodes_list[random.randint(0, rank - 1)]
            avg += nx.shortest_path_length(graph, source=random_src, target=random_tgt)
        length = avg / limit
        logging.info(f'Aproksymacja średniej długości ścieżki ({limit} wierzchołków): {length}')


def find_high_ranked_cores(graph: nx.Graph):
    core_numbers = nx.core_number(graph)
    sorted_items = {k: v for k, v in sorted(core_numbers.items(), key=lambda item: item[1], reverse=True)}
    counted = Counter(sorted_items.values())
    logging.info(f'Liczba rdzeni: 1. {counted[1]}, 2. {counted[2]}, 3. {counted[3]}')

    plt.bar(counted.keys(), counted.values())
    plt.title('Rozkład wierzchołków')
    plt.show()


if __name__ == '__main__':
    main()
