import sys

from matplotlib import pyplot as plt

from QLearning import q_learning


def main():
    input_maze = load_maze('data/maze.txt')

    print('Loaded maze:')
    show_maze(input_maze)

    path, epochs, steps = q_learning(input_maze)

    print('Solved maze:')
    mark_path(input_maze, path)
    show_maze(input_maze)

    plt.plot(epochs, steps)
    plt.show()


def load_maze(path):
    try:
        with open(path, mode='r', encoding='UTF-8') as file:
            lines = []
            for file_line in file:
                line = []
                for file_char in file_line.rstrip():
                    line.append(file_char)
                lines.append(line)
            return lines
    except FileNotFoundError:
        print(f"File '{path}' not found.")
        sys.exit(1)


def mark_path(maze, path):
    for i in range(1, len(path) - 1):
        prev = path[i - 1]
        curr = path[i]
        assert maze[curr[0]][curr[1]] != '#'

        if curr[0] == prev[0]:
            if curr[1] > prev[1]:
                maze[curr[0]][curr[1]] = '\u2192'
            else:
                maze[curr[0]][curr[1]] = '\u2190'
        else:
            if curr[0] > prev[0]:
                maze[curr[0]][curr[1]] = '\u2193'
            else:
                maze[curr[0]][curr[1]] = '\u2191'


def show_maze(maze):
    for row in maze:
        for col in row:
            print(col, end='')
        print()
    print()


if __name__ == '__main__':
    main()
