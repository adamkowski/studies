from random import choice, uniform

import numpy as np


def q_learning(input_maze, learning_epochs=500, step_limit=500, learning_rate=0.5):
    q_table = np.zeros((len(input_maze), len(input_maze[0]), 4))
    reward_table = np.zeros((len(input_maze), len(input_maze[0])))

    begin_point = find_cell_in_maze(input_maze, 'S')
    end_point = find_cell_in_maze(input_maze, 'F')
    reward_table[end_point[0]][end_point[1]] = 1

    moves = {
        0: (0, -1),
        1: (1, 0),
        2: (0, 1),
        3: (-1, 0)
    }

    gamma = 0.9
    epsilon = 1.0
    epochs = []
    steps = []
    path = [begin_point]
    for epoch in range(learning_epochs):
        position = begin_point
        for step in range(step_limit):
            maze_walls = np.array([list(int(cell == '#') for cell in line) for line in input_maze])
            available_moves = find_available_moves(position, maze_walls, moves)
            move = find_move(available_moves, epsilon, q_table, position)
            next_position = np.array(moves[move]) + np.array(position)
            if epoch == learning_epochs - 1:
                path.append(next_position)

            update_q_table(q_table, reward_table, position, next_position, move, gamma, learning_rate)

            if next_position[0] == end_point[0] and next_position[1] == end_point[1]:
                epochs.append(epoch)
                steps.append(step)
                break

            position = next_position

        if epsilon > 0.01:
            epsilon -= 0.01

    return path, epochs, steps


def find_move(moves, epsilon, q_table, current_pos):
    if uniform(0, 1) > epsilon:
        return max(moves, key=lambda direction: q_table[current_pos[0]][current_pos[1]][direction])
    return choice(moves)


def find_cell_in_maze(maze, cell):
    for row in range(len(maze)):
        for col in range(len(maze[0])):
            if maze[row][col] == cell:
                return row, col
    raise IndexError


def find_available_moves(current_pos, maze, moves):
    results = []
    for move in moves.keys():
        next_move = np.array(moves[move]) + np.array(current_pos)
        if is_move_correct(maze, next_move):
            results.append(move)
    return results


def is_move_correct(maze, move):
    return is_move_in_maze_range(maze, move) and not is_move_in_maze_corridor(maze, move)


def is_move_in_maze_range(maze, move):
    return 0 <= move[0] < maze.shape[0] and 0 <= move[1] < maze.shape[1]


def is_move_in_maze_corridor(maze, move):
    return maze[move[0]][move[1]] == 1


def update_q_table(q_table, reward_table, current_pos, next_pos, move, gamma, learning_rate):
    reward = reward_table[next_pos[0]][next_pos[1]]
    update = (reward + gamma * max(q_table[next_pos[0]][next_pos[1]]) - q_table[current_pos[0]][current_pos[1]][move])
    q_table[current_pos[0]][current_pos[1]][move] += learning_rate * update
