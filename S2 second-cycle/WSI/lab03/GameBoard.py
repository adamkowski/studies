import copy

from GameState import GameState


class GameBoard(object):

    def __init__(self, game_board=None):
        if game_board is None:
            self.__board = [["_", "_", "_"], ["_", "_", "_"], ["_", "_", "_"]]
        else:
            self.__board = game_board

    def get_board(self):
        deepcopy = copy.deepcopy(self.__board)
        return deepcopy

    def display(self):
        print("┌───────┐")
        for row in self.__board:
            print("│ ", end="")
            for cell in row:
                if cell == "_":
                    print(" ", end="")
                else:
                    print(cell, end="")
                print(" ", end="")
            print("│")
        print("└───────┘")

    def put_token(self, token: str, row: int, col: int):
        if self.__board[row][col] != "_" and token != "_":
            raise ValueError("This cell is occupied!")
        self.__board[row][col] = token

    def get_winning_token(self):
        if self.__is_diagonal_winning():
            return self.__board[1][1]
        elif self.__get_winning_column() != -1:
            return self.__board[0][self.__get_winning_column()]
        elif self.__get_winning_row() != -1:
            return self.__board[self.__get_winning_row()][0]
        return ""

    def get_game_state(self):
        win_token = self.get_winning_token()
        if win_token == "X":
            return GameState.X_WIN
        elif win_token == "O":
            return GameState.O_WIN
        elif self.__is_move_possible():
            return GameState.NOT_FINISHED
        return GameState.DRAW

    def __is_diagonal_winning(self) -> bool:
        token = self.__board[1][1]
        if token == "_":
            return False
        return token == self.__board[0][0] and token == self.__board[2][2] or token == self.__board[0][2] and token == self.__board[2][0]

    def __get_winning_column(self):
        for col in range(0, 3):
            token = self.__board[0][col]
            if token == "_":
                continue
            if token == self.__board[1][col] == self.__board[2][col]:
                return col
        return -1

    def __get_winning_row(self):
        for row in range(0, 3):
            token = self.__board[row][0]
            if token == "_":
                continue
            if token == self.__board[row][1] == self.__board[row][2]:
                return row
        return -1

    def __is_move_possible(self):
        for row in self.__board:
            for cell in row:
                if cell == "_":
                    return True
        return False
