from Game import Game
from PlayerMinimax import PlayerMinimax


def main():
    player_1 = PlayerMinimax("X", 9, True)
    player_2 = PlayerMinimax("O", 9, True)
    game = Game(player_1, player_2)
    game.play()
    print(f"Player 1: {player_1.get_states_counter()}")
    print(f"Player 2: {player_2.get_states_counter()}")


if __name__ == '__main__':
    main()
