from enum import Enum


class GameState(Enum):
    NOT_FINISHED = 0,
    DRAW = 1,
    X_WIN = 2,
    O_WIN = 3
