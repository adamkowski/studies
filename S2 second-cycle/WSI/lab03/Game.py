from GameBoard import GameBoard
from GameState import GameState


class Game(object):

    def __init__(self, player_x, player_o):
        self.__game_board = GameBoard()
        self.__player_x = player_x
        self.__player_o = player_o
        self.__current_player = None

    def play(self):
        self.__game_board.display()
        while self.__game_board.get_game_state() == GameState.NOT_FINISHED:
            self.__current_player = self.__next_player()
            self.__current_player.move(self.__game_board)
            self.__game_board.display()
        print(self.__game_board.get_game_state().name)

    def __next_player(self):
        if self.__current_player == self.__player_x:
            return self.__player_o
        return self.__player_x
