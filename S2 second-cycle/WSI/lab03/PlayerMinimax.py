from GameBoard import GameBoard
from GameState import GameState


class PlayerMinimax(object):

    def __init__(self, token: str, depth: int, alpha_beta: bool):
        self.__player_token = token
        if token == "X":
            self.__opponent_token = "O"
        else:
            self.__opponent_token = "X"
        self.__depth = depth
        self.__alpha_beta = alpha_beta
        self.__states_counter = 0

    def move(self, game_board: GameBoard):
        best_move = self.__minimax(game_board.get_board(), self.__player_token)
        spot = best_move['spot']
        game_board.put_token(self.__player_token, spot[0], spot[1])

    def __minimax(self, new_board, token, current_depth=1):
        game_board = GameBoard(new_board)
        if game_board.get_game_state() != GameState.NOT_FINISHED or current_depth > self.__depth:
            move = {}
            winning_token = game_board.get_winning_token()
            if winning_token == self.__player_token:
                move['score'] = 10
            elif winning_token == self.__opponent_token:
                move['score'] = -10
            else:
                move['score'] = 0
            return move

        moves = []
        for spot in self.__find_empty_cells(new_board):
            move = {'spot': spot}
            game_board.put_token(token, spot[0], spot[1])
            if token == self.__player_token:
                move['score'] = self.__minimax(game_board.get_board(), self.__opponent_token, current_depth + 1)['score']
            else:
                move['score'] = self.__minimax(game_board.get_board(), self.__player_token, current_depth + 1)['score']
            game_board.put_token("_", spot[0], spot[1])
            moves.append(move)

        alpha = -1
        beta = 1

        best_move = 0
        if token == self.__player_token:
            best_score = -1000
            for i in range(0, len(moves)):
                self.__states_counter += 1
                if moves[i]['score'] > best_score:
                    best_score = moves[i]['score']
                    best_move = i
                    if self.__alpha_beta:
                        alpha = max(alpha, best_score)
                        if beta <= alpha:
                            return moves[best_move]
        else:
            best_score = 1000
            for i in range(0, len(moves)):
                self.__states_counter += 1
                if moves[i]['score'] < best_score:
                    best_score = moves[i]['score']
                    best_move = i
                    if self.__alpha_beta:
                        beta = min(beta, best_score)
                        if beta <= alpha:
                            return moves[best_move]

        return moves[best_move]

    @staticmethod
    def __find_empty_cells(board):
        available_spots = []
        for row in range(0, 3):
            for col in range(0, 3):
                if board[row][col] == "_":
                    available_spots.append((row, col))
        return available_spots

    def get_states_counter(self):
        return self.__states_counter
