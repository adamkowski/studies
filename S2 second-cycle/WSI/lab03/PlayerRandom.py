import random


class PlayerRandom(object):

    def __init__(self, token: str):
        self.__token = token

    def move(self, game_board):
        while True:
            row = random.randint(0, 2)
            col = random.randint(0, 2)
            try:
                game_board.put_token(self.__token, row, col)
            except ValueError:
                continue
            break
