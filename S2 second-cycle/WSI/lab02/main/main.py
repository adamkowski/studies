import random
from timeit import default_timer as timer
from datetime import timedelta
from enum import Enum

import numpy as np
from matplotlib import pyplot as plt
from numpy import sin, exp, cos


class PopulationType(Enum):
    RANDOMIZED = 1
    CLONES = 2


POINT_RANGE = (-2 * np.pi, 2 * np.pi)
RECOMBINATION_ALPHA = 0.1


def main():
    iterations_limit = 100
    population_init_size = 10
    tournament_size = 2
    elite_size = 1
    mutation_strength_sigma = 0.5
    mut_probability = 0.5

    find_minimum(iterations_limit, population_init_size, PopulationType.RANDOMIZED, tournament_size, elite_size,
                 mutation_strength_sigma, mut_probability, "Random")
    find_minimum(iterations_limit, population_init_size, PopulationType.CLONES, tournament_size, elite_size,
                 mutation_strength_sigma, mut_probability, "Clones")

    plt.title("Minimum found")
    plt.xlabel("Iteration")
    plt.ylabel("Value")
    plt.legend()
    plt.show()


def find_minimum(iterations_limit, population_init_size, population_init_type, tournament_size, elite_size,
                 mutation_strength_sigma, mut_probability, label):
    start = timer()

    actual_min = float("inf")
    min_values = []
    elite = np.zeros((0, 2))
    for i in range(iterations_limit):
        population = generate_population(population_init_type, population_init_size - len(elite))
        population = np.concatenate([elite, population])

        population = select_tournament(population, tournament_size)
        population = cross_population(population)
        population = mutate_population(population, mutation_strength_sigma, mut_probability)

        elite = take_best(population, elite_size)
        actual_min = min(actual_min, function_bird(elite[0]))
        min_values.append(actual_min)
    x = list(range(1, iterations_limit + 1))
    plt.plot(x, min_values, label=label)

    elapsed = timedelta(seconds=timer() - start)
    print(label + ": " + str(elapsed) + ", " + str(actual_min))


def generate_population(population_type: PopulationType, population_size: int):
    if population_type == PopulationType.RANDOMIZED:
        return generate_randomized(population_size)

    return generate_clones(population_size)


def generate_randomized(population_size):
    population = np.zeros((population_size, 2))

    for i in range(population_size):
        population[i, :] = [
            np.random.uniform(POINT_RANGE[0], POINT_RANGE[1]),
            np.random.uniform(POINT_RANGE[0], POINT_RANGE[1])
        ]

    return population


def generate_clones(population_size: int):
    population = np.zeros((population_size, 2))

    rand_x = np.random.uniform(POINT_RANGE[0], POINT_RANGE[1])
    rand_y = np.random.uniform(POINT_RANGE[0], POINT_RANGE[1])
    for i in range(population_size):
        population[i, :] = [rand_x, rand_y]

    return population


def select_tournament(population, tournament_size):
    new_population = np.full_like(population, 0)
    for i in range(len(population)):
        tournament = np.zeros((tournament_size, 2))
        values = []
        for j in range(tournament_size):
            rand = random.choice(population)
            tournament[j, :] = rand
            values.append(function_bird(rand))
        min_index = values.index(min(values))
        new_population[i, :] = tournament[min_index]
    return new_population


def function_bird(x):
    """ Reference: https://www.indusmic.com/post/bird-function """
    addition = (
        sin(x[0]) * (exp(1 - cos(x[1])) ** 2),
        cos(x[1]) * (exp(1 - sin(x[0])) ** 2),
        (x[0] - x[1]) ** 2
    )
    return sum(addition)


def take_best(population, elite_size):
    sorted_population = np.array(sorted(population, key=function_bird))
    return sorted_population[0:elite_size]


def cross_population(population):
    crossed_population = np.full_like(population, 0)
    for i in range(len(population) // 2):
        (parent_1, parent_2) = get_random_parents(population)
        (child_1, child_2) = arithmetic_recombination(parent_1, parent_2)
        crossed_population[i, :] = child_1
        crossed_population[i + len(population) // 2, :] = child_2
    return crossed_population


def get_random_parents(population):
    return random.choice(population), random.choice(population)


def arithmetic_recombination(parent_1, parent_2):
    child_1 = (
        RECOMBINATION_ALPHA * parent_1[0] + (1 - RECOMBINATION_ALPHA) * parent_2[0],
        RECOMBINATION_ALPHA * parent_1[1] + (1 - RECOMBINATION_ALPHA) * parent_2[1]
    )
    child_2 = (
        RECOMBINATION_ALPHA * parent_2[0] + (1 - RECOMBINATION_ALPHA) * parent_1[0],
        RECOMBINATION_ALPHA * parent_2[1] + (1 - RECOMBINATION_ALPHA) * parent_1[1]
    )
    return child_1, child_2


def mutate_population(population, sigma, mut_probability):
    """ Population mutation using the Gaussian distribution: x += sigma * N(0, 1) """
    mutated_population = np.full_like(population, 0)
    index = 0
    for point in population:
        if np.random.uniform(0.0, 1.0) <= mut_probability:
            mutated_population[index] = mutate_point(point, sigma)
        else:
            mutated_population[index] = point
        index += 1
    return mutated_population


def mutate_point(point, sigma):
    distribution_normal = np.random.normal()
    return np.array([
        point[0] + sigma * distribution_normal,
        point[1] + sigma * distribution_normal
    ])


def draw_plot(population):
    for point in population:
        plt.plot(point[0], point[1], 'b.')


if __name__ == '__main__':
    main()
