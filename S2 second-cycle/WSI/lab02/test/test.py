import unittest

from main.main import function_bird


class MyTestCase(unittest.TestCase):
    def test_function_bird_should_return_correct_value(self):
        result = function_bird([4.70104, 3.15294])
        expected = -106.764537
        self.assertAlmostEqual(result, expected, 1)


if __name__ == '__main__':
    unittest.main()
