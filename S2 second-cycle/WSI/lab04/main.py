import csv
import math
import sys
from collections import defaultdict

import numpy as np

from WinesData import WinesData


def main():
    if len(sys.argv) < 2:
        print("You must specify the path to the file.")
        sys.exit(1)

    wines_data = WinesData(read_file(sys.argv[1]))
    train_data, test_data = wines_data.split(0.6)

    categorized_groups = group_data_by_wine_quality(train_data)
    probabilities = get_probabilities(categorized_groups, len(train_data))
    wine_properties = calculate_wine_group_properties(categorized_groups)

    verify_test_set(test_data, wine_properties, probabilities)

    data_parts = wines_data.part(5)
    verify_cross_validation(data_parts)


def read_file(path: str):
    content = []
    try:
        with open(path, mode='r') as csv_file:
            reader = csv.reader(csv_file, delimiter=';')
            for row in reader:
                content.append(row)
    except FileNotFoundError:
        print(f"File '{path}' not found.")
        sys.exit(1)
    return content


def group_data_by_wine_quality(data):
    groups = defaultdict(list)
    for line in data:
        groups[line[11]].append(line[:11])
    return groups


def get_probabilities(categorized_groups, train_data_size):
    prob = {}
    for i in categorized_groups.keys():
        prob[i] = len(categorized_groups[i]) / train_data_size
    return prob


def calculate_wine_group_properties(categorized_groups):
    group_properties = {}
    for quality_cat in categorized_groups:
        quality_group = categorized_groups[quality_cat]
        property_values = [[], [], [], [], [], [], [], [], [], [], []]
        for csv_wine_record in quality_group:
            for wine_property in range(len(csv_wine_record)):
                property_values[wine_property].append(float(csv_wine_record[wine_property]))

        group_properties[quality_cat] = {
            "fixed_acidity": {"mean": np.mean(property_values[0]), "var": np.var(property_values[0])},
            "volatile_acidity": {"mean": np.mean(property_values[1]), "var": np.var(property_values[1])},
            "citric_acid": {"mean": np.mean(property_values[2]), "var": np.var(property_values[2])},
            "residual_sugar": {"mean": np.mean(property_values[3]), "var": np.var(property_values[3])},
            "chlorides": {"mean": np.mean(property_values[4]), "var": np.var(property_values[4])},
            "free_sulfur_dioxide": {"mean": np.mean(property_values[5]), "var": np.var(property_values[5])},
            "total_sulfur_dioxide": {"mean": np.mean(property_values[6]), "var": np.var(property_values[6])},
            "density": {"mean": np.mean(property_values[7]), "var": np.var(property_values[7])},
            "pH": {"mean": np.mean(property_values[8]), "var": np.var(property_values[8])},
            "sulphates": {"mean": np.mean(property_values[9]), "var": np.var(property_values[9])},
            "alcohol": {"mean": np.mean(property_values[10]), "var": np.var(property_values[10])},
        }
    return group_properties


def evaluate_probe(probe, wine_properties, probabilities):
    evaluations = {}
    for quality_category in wine_properties:
        properties = wine_properties[quality_category]
        property_eval = []
        for idx, prop in enumerate(properties):
            next_property = wine_properties[quality_category][prop]
            property_eval.append(gaussian_function(float(probe[idx]), next_property["var"], next_property["mean"]))
        evaluations[quality_category] = math.prod(property_eval) * probabilities[quality_category]
    return max(evaluations, key=evaluations.get)


def gaussian_function(x: float, sigma2: float, my: float) -> float:
    """
    Gaussian probability density function. If sigma is zero then it is converted to a very small value
    (according to https://stats.stackexchange.com/a/233841)
    :param x: function attribute
    :param sigma2: (σ²) variance
    :param my: (μ) mean
    :return: the function value for the specified parameter
    """
    if sigma2 < 0.001:
        sigma2 = 0.001

    base = 1 / (math.sqrt(2 * math.pi * sigma2))
    exponent = -((x - my) ** 2) / (2 * sigma2)
    return base * math.exp(exponent)


def verify_test_set(test_data, wine_properties, probabilities):
    correct_eval = 0
    for test in test_data:
        real_cat = test[11]
        evaluation = evaluate_probe(test[:11], wine_properties, probabilities)
        if evaluation == real_cat:
            correct_eval += 1
    print(f"Verification using a test set: {correct_eval}/{len(test_data)}", end=" ")
    print(f"({int(correct_eval / len(test_data) * 100)}%) correct")


def verify_cross_validation(data_parts):
    correct = []
    for part_idx in range(len(data_parts)):
        test_data = data_parts[part_idx]
        train_data = exclude_data(data_parts, part_idx)

        categorized_groups = group_data_by_wine_quality(train_data)
        probabilities = get_probabilities(categorized_groups, len(train_data))
        wine_properties = calculate_wine_group_properties(categorized_groups)

        correct_eval = 0
        for test in test_data:
            real_cat = test[11]
            evaluation = evaluate_probe(test[:11], wine_properties, probabilities)
            if evaluation == real_cat:
                correct_eval += 1
        correct.append(correct_eval)
    print(f"Verification using cross validation: {sum(correct)}/{len(data_parts) * len(data_parts[0])}", end=" ")
    print(f"({int(sum(correct) / (len(data_parts) * len(data_parts[0])) * 100)}%) correct")


def exclude_data(data_parts, part_idx):
    data = []
    for i in range(len(data_parts)):
        if i != part_idx:
            data = [*data, *data_parts[i]]
    return data


if __name__ == '__main__':
    main()
