class WinesData(object):
    def __init__(self, csv_lines):
        self.__header = csv_lines[0]
        data = csv_lines[1:]
        self.__categories_quality = []
        for line in data:
            self.__categories_quality.append(line[11])
        self.__data = data

    @property
    def categories_quality(self):
        return self.__categories_quality

    def split(self, ratio: float):
        split_point = int(ratio * len(self.__data))
        return self.__data[:split_point], self.__data[split_point:]

    def part(self, k: int):
        parts = []
        for i in range(k):
            parts.append(self.__data[i * int(len(self.__data) / k):(i + 1) * int(len(self.__data) / k)])
        return parts
