import matplotlib.pyplot as plt
import numpy as np


def main():
    fig, (ax1, ax2) = plt.subplots(ncols=2, figsize=(15, 7))

    draw(lambda x: x ** 2 + 3 * x + 8, lambda x: 2 * x + 3, ax1, 10)
    draw(lambda x: x ** 4 - 5 * x ** 2 - 3 * x, lambda x: 4 * x ** 3 - 10 * x - 3, ax2, 4)

    plt.show()


def draw(func, grad, ax, limit):
    x = np.linspace(-limit, limit, 100)
    y = func(x)
    ax.plot(x, y)

    (min_x, min_y) = find_min(func, grad, -limit, 0.5)
    ax.plot(min_x, min_y, '+')


def find_min(fun_x, grad_x, x_start: int, lam: float, stop_threshold=0.5, max_iter=50):
    args = [x_start]
    values = [fun_x(x_start)]

    next_point = x_start
    grad = grad_x(x_start)
    count = 0
    while np.sqrt(grad * grad) > stop_threshold and count < max_iter:
        alpha = lam / np.sqrt(grad * grad)
        next_point = next_point - alpha * grad

        args.append(next_point)
        values.append(fun_x(next_point))

        grad = grad_x(next_point)
        count += 1

    return args, values


if __name__ == '__main__':
    main()
