# Subjects

* [WSI](#introduction-to-artificial-intelligence)
* [SAD](#statistics-in-data-analysis)

## Introduction to Artificial Intelligence

Implementation of AI algorithms using the Python language.

* **Lab 1:** Searching for the minimum using the gradient descent method.

## Statistics in Data Analysis

Using the R language for operations on statistical data

* **Project 1:** Analysis of aviation catastrophes over the years. Data modeling with the Poisson distribution.
  Assessing the level and increase of safety in aviation.
* **Project 2:** Statistical tests, checking the correlation between real data sets.
